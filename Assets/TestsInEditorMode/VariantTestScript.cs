using System;
using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class VariantTestScript
{
    [Test]
    public void VariantsAreEquals()
    {
        Variant v1 = new Variant(36);
        Variant v2 = new Variant(36);
        Variant v3 = new Variant("str");

        Assert.IsTrue(v1 == v2);
        Assert.IsFalse(v1 == v3);

        //LogAssert.Expect(UnityEngine.LogType.Assert, "Test Assert");
        //LogAssert.Expect(UnityEngine.LogType.Log, "Test log");
    }

    [Test]
    public void ColorSaveToString()
    {
        Color v1 = new Color(0.1234f, 0.3f, 0.5f, 1);
        string str = v1.ToStringExt();
        LogContext.WriteToLog(ELogLevel.IMPORTANT_INFO, str);
        StringExt.TryParse(str, out Color v2);
        Assert.IsTrue(v1 == v2);

        v1 = new Color(0.1f, 0.3f, 0.5f, 0.4f);
        str = v1.ToStringExt();
        LogContext.WriteToLog(ELogLevel.IMPORTANT_INFO, str);
        StringExt.TryParse(str, out v2);
        Assert.IsTrue(v1 == v2);
    }

    [Test]
    public void VariantSaveToString()
    {
        Variant v1 = new Variant(Color.white);
        string str = v1.SaveToString();

        LogContext.WriteToLog(ELogLevel.IMPORTANT_INFO, str);

        Variant v2 = Variant.CreateFromString(str);

        Assert.IsTrue(v1 == v2);
    }

    [Test]
    public void VariantArraySaveToString()
    {
        double[] arr = { 0.5, 0.7, 1 };

        Variant v1 = new Variant(arr);

        string str = v1.SaveToString();

        LogContext.WriteToLog(ELogLevel.IMPORTANT_INFO, str);

        Variant v2 = Variant.CreateFromString(str);

        double[] arr2 = v2.ToArrayFloat();

        bool eq = arr.Length == arr2.Length;
        for (int i = 0; eq && i < arr.Length; i++)
            eq = GameMath.Approximately(arr[i], arr2[i], 1e-5);

        Assert.IsTrue(eq);
    }

    [Test]
    public void VariantStringArraySaveToString()
    {
        string[] arr = { "1", "two", "apple" };

        Variant v1 = new Variant(arr);

        string str = v1.SaveToString();

        LogContext.WriteToLog(ELogLevel.IMPORTANT_INFO, str);

        Variant v2 = Variant.CreateFromString(str);

        string[] arr2 = v2.ToArrayStrings();

        bool eq = arr.Length == arr2.Length;
        for (int i = 0; eq && i < arr.Length; i++)
            eq = string.Equals(arr[i], arr2[i]);

        Assert.IsTrue(eq);
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    //[UnityTest]
    //public IEnumerator VariantTestScriptWithEnumeratorPasses()
    //{
    //    // Use the Assert class to test conditions.
    //    // Use yield to skip a frame.
    //    yield return null;
    //}
}
