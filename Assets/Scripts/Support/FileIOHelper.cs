﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public enum EFilePathes { Saves }

static class FileIOHelper
{
    public static string GetDebugPathes()
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendFormat("GetAbsoluteURL: {0}", GetAbsoluteURL());
        sb.AppendFormat("GetPersistentDataPath: {0}", GetPersistentDataPath());
        sb.AppendFormat("GetDataPath: {0}", GetDataPath());
        sb.AppendFormat("GetStreamingAssetsPath: {0}", GetStreamingAssetsPath());
        sb.AppendFormat("GetApplicationDataPath: {0}", GetApplicationDataPath());

        return sb.ToString();
    }

    public static string GetAbsoluteURL()
    {
        return Application.absoluteURL;
    }

    public static string GetPersistentDataPath()
    {
        return Application.persistentDataPath;
    }

    public static string GetDataPath()
    {
        return Application.dataPath;
    }

    public static string GetStreamingAssetsPath()
    {
        return Application.streamingAssetsPath;
    }

    public static string GetApplicationDataPath()
    {
        return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        //return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Razar/Frontier Pilot Simulator");
    }

    //public static string GetSystemDescriptionsPath()
    //{
    //    return string.Format("{0}/{1}", Application.streamingAssetsPath, "Descriptions/System");
    //}

    //private static string GetCustomDescriptionsPath()
    //{
    //    return string.Format("{0}/{1}", Application.streamingAssetsPath, "Descriptions/Custom");
    //}

    private static string GetSavesPath()
    {
        return Path.Combine(GetApplicationDataPath(), "Saves");
    }

    public static string GetFilePath(EFilePathes inPath, string inUniquePathPart)
    {
        string path = GetApplicationDataPath();

        switch (inPath)
        {
            case EFilePathes.Saves:
                path = string.Format("{0}/{1}", GetSavesPath(), inUniquePathPart);
                break;
        }

        return path;
    }

    public static string WriteTextToFile(EFilePathes inPath, string inUniquePathPart, string inText)
    {
        string file_path = GetFilePath(inPath, inUniquePathPart);
        string path = Path.GetDirectoryName(file_path);
        if (path != null && !Directory.Exists(path))
            Directory.CreateDirectory(path);
        File.WriteAllText(file_path, inText, Encoding.UTF8);
        return file_path;
    }

    public static bool IsFileExist(string path)
    {
        return File.Exists(path);
    }

    public static bool IsFileExist(EFilePathes inPath, string inUniquePathPart)
    {
        string file_path = GetFilePath(inPath, inUniquePathPart);
        return File.Exists(file_path);
    }

    public static string[] GetFindFilePathes(EFilePathes inPath, string inUniquePathPart)
    {
        var pathes = new List<string>();
        switch (inPath)
        {
            case EFilePathes.Saves:
                pathes.Add(string.Format("{0}/{1}", GetSavesPath(), inUniquePathPart));
                break;
        }

        return pathes.ToArray();
    }

    public static string LoadTextFromFile(EFilePathes inPath, string inUniquePathPart, bool inShowWarning)
    {
        string path = FindFilePath(inPath, inUniquePathPart, inShowWarning);
        if (string.IsNullOrEmpty(path))
            return string.Empty;

        string text = File.ReadAllText(path, Encoding.UTF8);
        return text;
    }

    private static string FindFilePath(EFilePathes inPath, string inUniquePathPart, bool inShowWarning)
    {
        string[] pathes = GetFindFilePathes(inPath, inUniquePathPart);

        for (int i = 0; i < pathes.Length; i++)
        {
            if (File.Exists(pathes[i]))
                return pathes[i];
        }

        if (inShowWarning)
        {
            string log_text = string.Format("Can't find file in: {0}", string.Join("; ", pathes));
            LogContext.WriteToLog(ELogLevel.WARNING, log_text);
        }

        return string.Empty;
    }
}
