﻿using System;
using UnityEngine;

public readonly struct RVector : IEquatable<RVector>
{
    private readonly Vector3 _position;

    private readonly Quaternion _rotation;

    public Vector3 Position => _position;
    public Quaternion Rotation => _rotation;

    public RVector(Vector3 inPosition, Quaternion inRotation)
    {
        _position = inPosition;
        _rotation = inRotation;
        if (_rotation.IsZero())
            _rotation = Quaternion.identity;
    }

    public RVector(Vector3 inPosition) : this(inPosition, Quaternion.identity) { }

    public RVector(int x, int y, int z) : this(new Vector3(x, y, z), Quaternion.identity) { }

    public RVector(Transform inTransform, bool inLocal) :
        this(inLocal ? inTransform.localPosition : inTransform.position, inLocal ? inTransform.localRotation : inTransform.rotation)
    { }

    public RVector(Transform inTransform, Vector3 inUnityShift) :
        this(inTransform.position - inUnityShift, inTransform.rotation)
    { }

    public override string ToString()
    {
        if(Rotation.IsIdentity())
            return _position.ToStringExt();
        return $"{_position.ToStringExt()}, {_rotation.ToStringExt()}";
    }

    public static bool TryParse(string inStrValue, out RVector outResult)
    {
        outResult = RVector.zero;

        if (string.IsNullOrEmpty(inStrValue))
            return false;

        (float[] arr, int arr_len) = inStrValue.ParseToFloatArray();

        if (arr_len == 3)
        {
            Vector3 pos = new Vector3(arr[0], arr[1], arr[2]);
            outResult = new RVector(pos);
            return true;
        }

        if (arr_len == 6)
        {
            Vector3 pos = new Vector3(arr[0], arr[1], arr[2]);
            Vector3 eulers = new Vector3(arr[3], arr[4], arr[5]);
            outResult = new RVector(pos, Quaternion.Euler(eulers));
            return true;
        }

        return false;
    }

    public static RVector zero = new RVector(Vector3.zero, Quaternion.identity);

    public Vector3 forward { get { return Rotation * Vector3.forward; } }
    public Vector3 up { get { return Rotation * Vector3.up; } }
    public Vector3 right { get { return Rotation * Vector3.right; } }

    public bool IsEmpty() { return _position.IsEmpty() && Rotation.IsIdentity(); }

    public RVector GetInverse()
    {
        return new RVector(-1 * _position, Quaternion.Inverse(Rotation));
    }

    ///объект родитель для входной точки
    public Vector3 WorldToLocalDirection_Parent(Vector3 inWorldDir)
    {
        return GameMath.WorldToLocalDirection(inWorldDir, Rotation);
    }

    ///объект родитель для входной точки
    public Vector3 WorldToLocalPosition_Parent(Vector3 inWorldPos)
    {
        return GameMath.WorldToLocalPosition(inWorldPos, Position, Rotation);
    }

    ///объект родитель для входной точки
    public RVector WorldToLocalPosition_Parent(Vector3 inWorldPos, Quaternion inWorldRotation)
    {
        Vector3 pos = GameMath.WorldToLocalPosition(inWorldPos, Position, Rotation);
        Quaternion rot = GameMath.RotationToLocalSpace(Rotation, inWorldRotation);

        return new RVector(pos, rot);
    }

    ///объект родитель для входной точки
    public RVector WorldToLocalPosition_Parent(RVector inWorldPos)
    {
        Vector3 pos = GameMath.WorldToLocalPosition(inWorldPos.Position, Position, Rotation);
        Quaternion rot = GameMath.RotationToLocalSpace(Rotation, inWorldPos.Rotation);

        return new RVector(pos, rot);
    }

    ///объект дочерний для входного объекта
    public RVector WorldToLocalPosition_Child(Vector3 inBasePos, Quaternion inBaseRotation)
    {
        Vector3 pos = GameMath.WorldToLocalPosition(Position, inBasePos, inBaseRotation);
        Quaternion rot = GameMath.RotationToLocalSpace(inBaseRotation, Rotation);

        return new RVector(pos, rot);
    }

    ///объект дочерний для входного объекта
    public RVector WorldToLocalPosition_Child(RVector inBase)
    {
        Vector3 pos = GameMath.WorldToLocalPosition(Position, inBase.Position, inBase.Rotation);
        Quaternion rot = GameMath.RotationToLocalSpace(inBase.Rotation, Rotation);

        return new RVector(pos, rot);
    }

    ///объект родитель для входной точки
    public Vector3 LocalToWorldDirection_Parent(Vector3 inLocalDir)
    {
        return Rotation * inLocalDir;
    }

    ///объект родитель для входной точки
    public RVector LocalToWorldPosition_Parent(Vector3 inLocalPos)
    {
        Vector3 pos = GameMath.LocalToWorldPosition(inLocalPos, Position, Rotation);
        return new RVector(pos, Rotation);
    }

    public RVector LocalShift(Vector3 inLocalShift)
    {
        Vector3 pos = GameMath.LocalToWorldPosition(inLocalShift, Position, Rotation);
        return new RVector(pos, Rotation);
    }

    public RVector WorldShift(Vector3 inShift)
    {
        return new RVector(Position + inShift, Rotation);
    }

    ///объект родитель для входной точки
    public RVector LocalToWorldPosition_Parent(Vector3 inLocalPos, Quaternion inLocalRotation)
    {
        Vector3 pos = GameMath.LocalToWorldPosition(inLocalPos, Position, Rotation);
        Quaternion rot = Rotation * inLocalRotation;

        return new RVector(pos, rot);
    }

    public RVector LocalToWorldPosition_Parent(RVector inLocalPos)
    {
        Vector3 pos = GameMath.LocalToWorldPosition(inLocalPos.Position, Position, Rotation);
        Quaternion rot = Rotation * inLocalPos.Rotation;

        return new RVector(pos, rot);
    }

    public RVector LocalShift(RVector inLocalShift)
    {
        Vector3 pos = GameMath.LocalToWorldPosition(inLocalShift.Position, Position, Rotation);
        Quaternion rot = Rotation * inLocalShift.Rotation;

        return new RVector(pos, rot);
    }

    public RVector WorldShift(RVector inShift)
    {
        Vector3 pos = Position + inShift.Position;
        Quaternion rot = Rotation * inShift.Rotation;

        return new RVector(pos, rot);
    }

    ///объект дочерний для входного объекта
    public RVector LocalToWorldPosition_Child(Vector3 inBasePos, Quaternion inBaseRotation)
    {
        Vector3 pos = GameMath.LocalToWorldPosition(Position, inBasePos, inBaseRotation);
        Quaternion rot = inBaseRotation * Rotation;

        return new RVector(pos, rot);
    }

    ///объект дочерний для входного объекта
    public RVector LocalToWorldPosition_Child(RVector inBase)
    {
        Vector3 pos = GameMath.LocalToWorldPosition(Position, inBase.Position, inBase.Rotation);
        Quaternion rot = inBase.Rotation * Rotation;

        return new RVector(pos, rot);
    }

    ///объект дочерний для входного объекта
    public RVector LocalToWorldPosition_Child(Transform inTransform)
    {
        Vector3 pos = inTransform.TransformPoint(Position);
        Quaternion rot = inTransform.rotation * Rotation;

        return new RVector(pos, rot);
    }

    public RVector Lerp(float t)
    {
        Vector3 v = Vector3.Lerp(Vector3.zero, _position, t);

        Quaternion q;
        if (GameMath.Approximately(t, 1, 0.03f) || t > 1)
            q = Rotation;
        else
            q = Quaternion.Slerp(Quaternion.identity, Rotation, t);

        return new RVector(v, q);
    }

    public static RVector Lerp(RVector From, RVector To, float t)
    {
        Vector3 v = Vector3.Lerp(From.Position, To.Position, t);

        Quaternion q;
        if (GameMath.Approximately(t, 1, 0.03f) || t > 1)
            q = To.Rotation;
        else
            q = Quaternion.Slerp(From.Rotation, To.Rotation, t);

        return new RVector(v, q);
    }


    #region ShouldSerialize
    public bool ShouldSerialize_position()
    {
        return _position != Vector3.zero;
    }

    public bool ShouldSerialize_rotation()
    {
        return _rotation != Quaternion.identity;
    }

    #endregion

    public bool Equals(RVector other)
    {
        return _position.Equals(other._position) && Rotation.Equals(other.Rotation);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is RVector && Equals((RVector)obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (_position.GetHashCode() * 397) ^ Rotation.GetHashCode();
        }
    }

    public bool Equals(RVector other, float inEpsilonPos, float inEpsilonRot)
    {
        return _position.IsEqual(other._position, inEpsilonPos) && Rotation.IsEqual(other.Rotation, inEpsilonRot);
    }

    public static bool operator ==(RVector x, RVector y)
    {
        return x.Position == y.Position && x.Rotation == y.Rotation;
    }

    public static bool operator !=(RVector x, RVector y)
    {
        return x.Position != y.Position || x.Rotation != y.Rotation;
    }
}
