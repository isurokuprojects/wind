﻿using System;
using System.Collections.Generic;

[Serializable]
public readonly struct NamedId
{
    private readonly uint _id;
    public uint id { get { return _id; } }

    private readonly string _name;
    public string name { get { return _name; } }

    private NamedId(uint inId, string inNames)
    {
        _id = inId;
        _name = inNames;
    }

    public NamedId(string inName)
    {
        this = NamedId.GetNamedId(inName);
    }

    public override string ToString()
    {
        return name ?? string.Empty;
        //return string.Format("{0} [{1}]", name, id);
    }

    public bool IsEmpty { get { return id == 0; } }

    public override int GetHashCode()
    {
        return id.GetHashCode();
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((NamedId)obj);
    }

    public bool Equals(NamedId other)
    {
        return id == other.id;
    }

    public static bool operator ==(NamedId left, NamedId right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(NamedId left, NamedId right)
    {
        return !left.Equals(right);
    }

    private static Dictionary<string, NamedId> _named_ids = new Dictionary<string, NamedId>();
    private static Dictionary<uint, NamedId> _id_ids = new Dictionary<uint, NamedId>();
    private static uint _id_counter = uint.MinValue;

    public static NamedId GetNamedId(string inName)
    {
        if (string.IsNullOrEmpty(inName))
            return Empty;

        string uname = inName.ToUpper();

        NamedId id;

        if (_named_ids.TryGetValue(uname, out id))
            return id;

        id = new NamedId(++_id_counter, inName);
        _named_ids.Add(uname, id);
        _id_ids.Add(id.id, id);

        return id;
    }

    public static NamedId GetNamedId(uint inNameId)
    {
        NamedId id;
        if (_id_ids.TryGetValue(inNameId, out id))
            return id;
        return Empty;
    }

    public static NamedId Empty = new NamedId(0, string.Empty);
    public static NamedId Name = GetNamedId("Name");
    public static NamedId Undefined = GetNamedId("Undefined");

    public class CNamedIdComparer : IEqualityComparer<NamedId>
    {
        public bool Equals(NamedId x, NamedId y) { return x.id == y.id; }
        public int GetHashCode(NamedId obj) { return obj.id.GetHashCode(); }
    }

    public static CNamedIdComparer NamedIdComparer = new CNamedIdComparer();

    public static NamedId CreateNamedId(uint inId, string inNames)
    {
        return new NamedId(inId, inNames);
    }
}