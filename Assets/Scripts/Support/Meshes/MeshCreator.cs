﻿using Primitives3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CMeshCreator
{
    public enum EDirection { X, Y, Z }

    private static Quaternion CreateRotation(EDirection inDirection, int inPointCount)
    {
        float a = 360f / inPointCount;
        Quaternion rot = Quaternion.identity;

        switch(inDirection)
        {
            case EDirection.X: rot = Quaternion.Euler(a, 0, 0); break;
            case EDirection.Y: rot = Quaternion.Euler(0, a, 0); break;
            case EDirection.Z: rot = Quaternion.Euler(0, 0, a); break;
        }
        
        return rot;
    }

    public static Vector3 GetNeedleVector(EDirection inDirection, float inHeght)
    {
        switch (inDirection)
        {
            case EDirection.X: return Vector3.right * inHeght;
            case EDirection.Y: return Vector3.up * inHeght;
            case EDirection.Z: return Vector3.forward * inHeght;
        }

        return Vector3.zero;
    }

    private static Vector3 GetNormalVector(EDirection inDirection)
    {
        switch (inDirection)
        {
            case EDirection.X: return Vector3.up;
            case EDirection.Y: return Vector3.forward;
            case EDirection.Z: return Vector3.right;
        }

        return Vector3.zero;
    }

    public static void CreateConusWall(EDirection inDirection, float inHeght, float inRadius, int inPointCount, Vector3 inShift, List<Vector3> outVertexes, List<int> outIndices, List<Vector3> outNormals)
    {
        Quaternion rot = CreateRotation(inDirection, inPointCount);

        Vector3 needle = GetNeedleVector(inDirection, inHeght);

        Vector3 p = GetNormalVector(inDirection);
        outVertexes.Add(inRadius * p + inShift);
        outNormals.Add(p);

        for (int i = 0; i < inPointCount; i++)
        {
            int prev_index = outVertexes.Count - 1;

            p = rot * p;

            Vector3 rad2 = inRadius * p;

            outVertexes.Add(needle + inShift);
            outNormals.Add(p);
            int needle_index = prev_index + 1;

            outVertexes.Add(rad2 + inShift);
            outNormals.Add(p);
            int new_point_index = needle_index + 1;

            outIndices.Add(prev_index);
            outIndices.Add(new_point_index);
            outIndices.Add(needle_index);
        }
    }

    public static void CreateConusBottom(EDirection inDirection, float inRadius, int inPointCount, Vector3 inShift, List<Vector3> outVertexes, List<int> outIndices, List<Vector3> outNormals)
    {
        CreateCilinderBottom(false, inDirection, 0, inRadius, inPointCount, inShift, outVertexes, outIndices, outNormals);
    }

    public static void CreateCilinderBottom(bool inUp, EDirection inDirection, float inHeght, float inRadius, int inPointCount, Vector3 inShift, List<Vector3> outVertexes, List<int> outIndices, List<Vector3> outNormals)
    {
        Quaternion rot = CreateRotation(inDirection, inPointCount);

        int d = inUp ? 1 : -1;
        Vector3 revert_normal = d * GetNeedleVector(inDirection, 1);
        Vector3 base_point = inUp ? GetNeedleVector(inDirection, inHeght) : Vector3.zero;
        base_point += inShift;

        Vector3 p = GetNormalVector(inDirection);
        outVertexes.Add(inRadius * p + base_point);
        outNormals.Add(revert_normal);

        for (int i = 0; i < inPointCount; i++)
        {
            outIndices.Add(outVertexes.Count + d);
            outIndices.Add(outVertexes.Count);
            outIndices.Add(outVertexes.Count - d);

            p = rot * p;

            outVertexes.Add(base_point);
            outNormals.Add(revert_normal);

            outVertexes.Add(inRadius * p + base_point);
            outNormals.Add(revert_normal);
        }
    }

    public static void CreateCilinderWall(EDirection inDirection, float inHeght, float inRadius, int inPointCount, Vector3 inShift, List<Vector3> outVertexes, List<int> outIndices, List<Vector3> outNormals)
    {
        Quaternion rot = CreateRotation(inDirection, inPointCount);

        Vector3 needle = GetNeedleVector(inDirection, inHeght);

        Vector3 p = GetNormalVector(inDirection);
        outVertexes.Add(inRadius * p + inShift);
        outNormals.Add(p);

        for (int i = 0; i < inPointCount; i++)
        {
            int prev_index = outVertexes.Count - 1;

            Vector3 rad1 = inRadius * p;
            outVertexes.Add(needle + rad1 + inShift);
            outNormals.Add(p);
            int opp_prev_index = prev_index + 1;

            p = rot * p;
            
            Vector3 rad2 = inRadius * p;

            outVertexes.Add(needle + rad2 + inShift);
            outNormals.Add(p);
            int opp_new_point_index = opp_prev_index + 1;

            outVertexes.Add(rad2 + inShift);
            outNormals.Add(p);
            int new_point_index = opp_new_point_index + 1;

            outIndices.Add(prev_index);
            outIndices.Add(new_point_index);
            outIndices.Add(opp_prev_index);

            outIndices.Add(new_point_index);
            outIndices.Add(opp_new_point_index);
            outIndices.Add(opp_prev_index);
        }
    }

    private static void AddOBBPoint(int index, Vector3[] corners, Vector3 inNormal, Vector3 inShift, List<Vector3> outVertexes, List<int> outIndices, List<Vector3> outNormals)
    {
        outIndices.Add(outVertexes.Count);
        outVertexes.Add(corners[index] + inShift);
        outNormals.Add(inNormal);
    }

    public static void CreateOBB(Vector3 inHS, Vector3 inShift, List<Vector3> outVertexes, List<int> outIndices, List<Vector3> outNormals)
    {
        var bb = new SBoundBox3D(Vector3.zero, inHS);
        Vector3[] corners = bb.GetCorners();

        foreach(var ind in GetOBBCornerIndex())
        {
            AddOBBPoint(ind.Key, corners, ind.Value, inShift, outVertexes, outIndices, outNormals);
        }
    }

    private static IEnumerable<KeyValuePair<int, Vector3>> GetOBBCornerIndex()
    {
        yield return new KeyValuePair<int, Vector3>(0, Vector3.back);
        yield return new KeyValuePair<int, Vector3>(2, Vector3.back);
        yield return new KeyValuePair<int, Vector3>(1, Vector3.back);
        yield return new KeyValuePair<int, Vector3>(0, Vector3.back);
        yield return new KeyValuePair<int, Vector3>(3, Vector3.back);
        yield return new KeyValuePair<int, Vector3>(2, Vector3.back);

        yield return new KeyValuePair<int, Vector3>(4, Vector3.forward);
        yield return new KeyValuePair<int, Vector3>(5, Vector3.forward);
        yield return new KeyValuePair<int, Vector3>(6, Vector3.forward);
        yield return new KeyValuePair<int, Vector3>(4, Vector3.forward);
        yield return new KeyValuePair<int, Vector3>(6, Vector3.forward);
        yield return new KeyValuePair<int, Vector3>(7, Vector3.forward);

        yield return new KeyValuePair<int, Vector3>(1, Vector3.right);
        yield return new KeyValuePair<int, Vector3>(2, Vector3.right);
        yield return new KeyValuePair<int, Vector3>(6, Vector3.right);
        yield return new KeyValuePair<int, Vector3>(6, Vector3.right);
        yield return new KeyValuePair<int, Vector3>(5, Vector3.right);
        yield return new KeyValuePair<int, Vector3>(1, Vector3.right);

        yield return new KeyValuePair<int, Vector3>(0, Vector3.left);
        yield return new KeyValuePair<int, Vector3>(7, Vector3.left);
        yield return new KeyValuePair<int, Vector3>(3, Vector3.left);
        yield return new KeyValuePair<int, Vector3>(0, Vector3.left);
        yield return new KeyValuePair<int, Vector3>(4, Vector3.left);
        yield return new KeyValuePair<int, Vector3>(7, Vector3.left);

        yield return new KeyValuePair<int, Vector3>(3, Vector3.up);
        yield return new KeyValuePair<int, Vector3>(7, Vector3.up);
        yield return new KeyValuePair<int, Vector3>(6, Vector3.up);
        yield return new KeyValuePair<int, Vector3>(3, Vector3.up);
        yield return new KeyValuePair<int, Vector3>(6, Vector3.up);
        yield return new KeyValuePair<int, Vector3>(2, Vector3.up);

        yield return new KeyValuePair<int, Vector3>(0, Vector3.down);
        yield return new KeyValuePair<int, Vector3>(5, Vector3.down);
        yield return new KeyValuePair<int, Vector3>(4, Vector3.down);
        yield return new KeyValuePair<int, Vector3>(0, Vector3.down);
        yield return new KeyValuePair<int, Vector3>(1, Vector3.down);
        yield return new KeyValuePair<int, Vector3>(5, Vector3.down);
    }
}

