﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class OBBMesh : MonoBehaviour
{
    public Material material;

    [SerializeField]
    [OnChangedCall("OnChangeParams")]
    Vector3 HalfSize = Vector3.one;

    [SerializeField]
    [OnChangedCall("OnChangeParams")]
    bool OriginInCenter = true;

    [SerializeField]
    [OnChangedCall("OnChangeColorParams")]
    private Color Color;

    List<Vector3> _vertexes = new List<Vector3>();
    List<int> _indices = new List<int>();
    List<Vector3> _normals = new List<Vector3>();

    private MeshFilter _mesh_filter;
    private Mesh _mesh;
    private Renderer _renderer;
    private MaterialPropertyBlock _mat_block;

    public void CreateMesh()
    {
        //CLogContext.WriteToLog(ELogLevel.IMPORTANT_INFO, $"Call: {Radius} Radius");
        if (_mesh_filter == null)
            _mesh_filter = transform.GetComponent<MeshFilter>();

        if (_renderer == null)
            _renderer = transform.GetComponent<Renderer>();

        if (_mesh == null)
        {
            _mesh = new Mesh();
            _mesh_filter.mesh = _mesh;
            _mesh.name = "Cilinder";
        }

        if (_mat_block == null)
            _mat_block = new MaterialPropertyBlock();

        _vertexes.Clear();
        _indices.Clear();
        _normals.Clear();

        Vector3 shift = OriginInCenter ? Vector3.zero : HalfSize;
        CMeshCreator.CreateOBB(HalfSize, shift, _vertexes, _indices, _normals);

        _mesh.Clear();

        _mesh.SetVertices(_vertexes);
        _mesh.SetIndices(_indices, MeshTopology.Triangles, 0);
        _mesh.SetNormals(_normals);

        _mesh.Optimize();

        _renderer.material = material;

        ColoredMesh();
    }

    public void OnChangeParams()
    {
        StartCoroutine(RecreateMesh());
    }

    IEnumerator RecreateMesh()
    {
        yield return new WaitForSeconds(0.1f);
        CreateMesh();
    }

    public void OnChangeColorParams()
    {
        StartCoroutine(ColoredMeshCorutine());
    }

    IEnumerator ColoredMeshCorutine()
    {
        yield return new WaitForSeconds(0.1f);
        ColoredMesh();
    }

    void ColoredMesh()
    {
        _mat_block.SetColor("_Color", Color);
        _renderer.SetPropertyBlock(_mat_block);
    }

    private void OnValidate()
    {
        HalfSize.x = Mathf.Clamp(HalfSize.x, 0, 300);
        HalfSize.y = Mathf.Clamp(HalfSize.y, 0, 300);
        HalfSize.z = Mathf.Clamp(HalfSize.z, 0, 300);
    }

    private void OnEnable()
    {
        CreateMesh();
    }

    public void SetOBB(Vector3 origin, Vector3 hs)
    {
        HalfSize = hs;
        transform.position = origin;
        transform.rotation = Quaternion.identity;
        OriginInCenter = true;

        CreateMesh();
    }

    public void SetColor(Color inColor)
    {
        Color = inColor;
        ColoredMesh();
    }
}
