﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class CilinderMesh : MonoBehaviour
{
    public Material material;

    [SerializeField]
    [OnChangedCall("OnChangeParams")]
    float Heght = 1;

    [SerializeField]
    [OnChangedCall("OnChangeParams")]
    float Radius = 1;

    [SerializeField]
    [OnChangedCall("OnChangeParams")]
    public int PointCount = 20;

    [SerializeField]
    [OnChangedCall("OnChangeParams")]
    public CMeshCreator.EDirection Direction;

    [SerializeField]
    [OnChangedCall("OnChangeColorParams")]
    private Color Color;

    List<Vector3> _vertexes = new List<Vector3>();
    List<int> _indices = new List<int>();
    List<Vector3> _normals = new List<Vector3>();

    private MeshFilter _mesh_filter;
    private Mesh _mesh;
    private Renderer _renderer;
    private MaterialPropertyBlock _mat_block;

    public void CreateMesh()
    {
        //CLogContext.WriteToLog(ELogLevel.IMPORTANT_INFO, $"Call: {Radius} Radius");
        if (_mesh_filter == null)
            _mesh_filter = transform.GetComponent<MeshFilter>();

        if (_renderer == null)
            _renderer = transform.GetComponent<Renderer>();

        if (_mesh == null)
        {
            _mesh = new Mesh();
            _mesh_filter.mesh = _mesh;
            _mesh.name = "Cilinder";
        }

        if (_mat_block == null)
            _mat_block = new MaterialPropertyBlock();

        _vertexes.Clear();
        _indices.Clear();
        _normals.Clear();

        CMeshCreator.CreateCilinderWall(Direction, Heght, Radius, PointCount, Vector3.zero, _vertexes, _indices, _normals);
        CMeshCreator.CreateCilinderBottom(false, Direction, Heght, Radius, PointCount, Vector3.zero, _vertexes, _indices, _normals);
        CMeshCreator.CreateCilinderBottom(true, Direction, Heght, Radius, PointCount, Vector3.zero, _vertexes, _indices, _normals);

        _mesh.Clear();

        _mesh.SetVertices(_vertexes);
        _mesh.SetIndices(_indices, MeshTopology.Triangles, 0);
        _mesh.SetNormals(_normals);

        _mesh.Optimize();

        _renderer.material = material;

        ColoredMesh();
    }

    public void OnChangeParams()
    {
        StartCoroutine(RecreateMesh());
    }

    IEnumerator RecreateMesh()
    {
        yield return new WaitForSeconds(0.1f);
        CreateMesh();
    }

    public void OnChangeColorParams()
    {
        StartCoroutine(ColoredMeshCorutine());
    }

    IEnumerator ColoredMeshCorutine()
    {
        yield return new WaitForSeconds(0.1f);
        ColoredMesh();
    }

    void ColoredMesh()
    {
        _mat_block.SetColor("_Color", Color);
        _renderer.SetPropertyBlock(_mat_block);
    }

    private void OnValidate()
    {
        Heght = Mathf.Clamp(Heght, 0.01f, 300);
        Radius = Mathf.Clamp(Radius, 0.01f, 300);
        PointCount = Mathf.Clamp(PointCount, 3, 30);
    }

    private void OnEnable()
    {
        CreateMesh();
    }
}
