﻿using System.Linq;
using UnityEngine;
using System.Reflection;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif //UNITY_EDITOR

/// Feel free to us this. :)

public class OnChangedCallAttribute : PropertyAttribute
{
    public string methodName;
    public OnChangedCallAttribute(string methodNameNoArguments)
    {
        methodName = methodNameNoArguments;
    }
}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(OnChangedCallAttribute))]
public class OnChangedCallAttributePropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginChangeCheck();
        
        EditorGUI.PropertyField(position, property, new GUIContent(property.displayName));

        if (EditorGUI.EndChangeCheck())
        {
            OnChangedCallAttribute at = attribute as OnChangedCallAttribute;

            Type target_class = property.serializedObject.targetObject.GetType();

            MethodInfo method = target_class.GetMethods().Where(m => m.Name == at.methodName).FirstOrDefault();

            if (method != null && method.GetParameters().Count() == 0)// Only instantiate methods with 0 parameters
                method.Invoke(property.serializedObject.targetObject, null);
        }
    }
}

#endif