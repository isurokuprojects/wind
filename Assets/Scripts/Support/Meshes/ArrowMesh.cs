﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class ArrowMesh : MonoBehaviour
{
    public Material material;

    private MaterialPropertyBlock _mat_block;

    [SerializeField]
    [OnChangedCall("OnChangeMeshParams")]
    private float Length = 1;

    [SerializeField]
    [OnChangedCall("OnChangeMeshParams")]
    private float BaseRadius = 1;

    [SerializeField]
    [OnChangedCall("OnChangeMeshParams")]
    private int PointCount = 20;

    [SerializeField]
    [OnChangedCall("OnChangeMeshParams")]
    private float PointerLength = 1;

    [SerializeField]
    [OnChangedCall("OnChangeMeshParams")]
    private float PointerRadius = 1;

    [SerializeField]
    [OnChangedCall("OnChangeMeshParams")]
    private int PointerPointCount = 20;

    [SerializeField]
    [OnChangedCall("OnChangeParams")]
    private CMeshCreator.EDirection Direction;

    [SerializeField]
    [OnChangedCall("OnChangeColorParams")]
    private Color ArrayColor;

    List<Vector3> _vertexes = new List<Vector3>();
    List<int> _indices = new List<int>();
    List<Vector3> _normals = new List<Vector3>();

    MeshFilter _mesh_filter;
    Mesh _mesh;
    Renderer _renderer;

    public void CreateMesh()
    {
        //CLogContext.WriteToLog(ELogLevel.IMPORTANT_INFO, $"Call: {Radius} Radius");
        if (_mesh_filter == null)
            _mesh_filter = transform.GetComponent<MeshFilter>();

        if (_renderer == null)
            _renderer = transform.GetComponent<Renderer>();

        if (_mesh == null)
        {
            _mesh = new Mesh();
            _mesh_filter.mesh = _mesh;
            _mesh.name = "Arrow";
        }

        if (_mat_block == null)
            _mat_block = new MaterialPropertyBlock();

        _vertexes.Clear();
        _indices.Clear();
        _normals.Clear();

        Vector3 shift = CMeshCreator.GetNeedleVector(Direction, Length);
        CMeshCreator.CreateConusWall(Direction, PointerLength, PointerRadius, PointerPointCount, shift, _vertexes, _indices, _normals);
        CMeshCreator.CreateConusBottom(Direction, PointerRadius, PointerPointCount, shift, _vertexes, _indices, _normals);
        CMeshCreator.CreateCilinderWall(Direction, Length, BaseRadius, PointCount, Vector3.zero, _vertexes, _indices, _normals);
        CMeshCreator.CreateCilinderBottom(false, Direction, 0, BaseRadius, PointCount, Vector3.zero, _vertexes, _indices, _normals);

        _mesh.Clear();

        _mesh.SetVertices(_vertexes);
        _mesh.SetIndices(_indices, MeshTopology.Triangles, 0);
        _mesh.SetNormals(_normals);


        _mesh.Optimize();

        _renderer.material = material;
        ColoredMesh();
    }

    public void OnChangeMeshParams()
    {
        StartCoroutine(RecreateMesh());
    }

    IEnumerator RecreateMesh()
    {
        yield return new WaitForSeconds(0.1f);
        CreateMesh();
    }

    public void OnChangeColorParams()
    {
        StartCoroutine(ColoredMeshCorutine());
    }

    IEnumerator ColoredMeshCorutine()
    {
        yield return new WaitForSeconds(0.1f);
        ColoredMesh();
    }

    void ColoredMesh()
    {
        _mat_block.SetColor("_Color", ArrayColor);
        _renderer.SetPropertyBlock(_mat_block);
    }

    private void OnValidate()
    {
        Length = Mathf.Clamp(Length, 0.01f, 300);
        BaseRadius = Mathf.Clamp(BaseRadius, 0.01f, 300);
        PointCount = Mathf.Clamp(PointCount, 3, 30);
        PointerLength = Mathf.Clamp(PointerLength, 0.01f, Length);
        PointerRadius = Mathf.Clamp(PointerRadius, 0.01f, 300);
        PointerPointCount = Mathf.Clamp(PointerPointCount, 3, 30);
    }

    private void OnEnable()
    {
        CreateMesh();
    }

    void Start()
    {

    }
}
