﻿using System;
using System.Globalization;
using System.Text;
using UnityEngine;

public struct Variant
{
    System.Object _objref;
    VariantSmall _value1;//+16
    VariantSmall _value2;//+16 = 32
    ulong _ulong_value; //+8 = 40
    int _flags; // +4 = 44 

    const int TypeCodeBitMask = 0xff;
    const int ArrayBitMask = 0x100;

    public void Clear()
    {
        _flags = 0;
        _objref = null;
        _value1.Clear();
        _value2.Clear();
        _ulong_value = 0;
    }

    public EVariantType VariantType
    {
        get
        {
            byte t = (byte)(TypeCodeBitMask & _flags);
            return (EVariantType)t;
        }
    }

    public static Variant Undefined = new Variant();

    public bool IsUndefined { get { return _flags == 0; } }

    public bool IsArray { get { return (_flags & ArrayBitMask) == ArrayBitMask; } }

    public static bool Approximately(Variant A, Variant B, double inEpsilon = 0.0001)
    {
        if (A.IsFloatable() && B.IsFloatable())
        {
            return GameMath.Approximately(A.ToFloat(), B.ToFloat(), inEpsilon);
        }
        return A.Equals(B);
    }

    #region Equals
    public bool Equals(Variant other)
    {
        return
            _flags == other._flags
            && _objref == other._objref
            && _value1.Equals(other._value1)
            && _value2.Equals(other._value2)
            && _ulong_value.Equals(other._ulong_value);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is Variant && Equals((Variant)obj);
    }

    public static bool operator ==(Variant left, Variant right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(Variant left, Variant right)
    {
        return !left.Equals(right);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int r = _value1.GetHashCode();
            r = HashFunctions.HashCombine(r, _objref.GetHashCode());
            r = HashFunctions.HashCombine(r, _value2.GetHashCode());
            r = HashFunctions.HashCombine(r, _ulong_value.GetHashCode());
            r = HashFunctions.HashCombine(r, _flags);
            return r;
        }
    }
    #endregion //Equals

    bool CheckType(EVariantType inNeedType, int inArray = 0)
    {
        int abm = ArrayBitMask & _flags;
        int tbm = TypeCodeBitMask & _flags;

        int it = (int)inNeedType;

        return abm == inArray && it == tbm;
    }

    #region INT
    public Variant(long value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(long value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public int ToInt(int inDefault = 0)
    {
        return _value1.ToInt(inDefault);
    }

    public long ToLong(long inDefault = 0)
    {
        return _value1.ToLong(inDefault);
    }

    #endregion INT

    #region DateTime
    public Variant(DateTime value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(DateTime value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public DateTime ToDateTime(DateTime inDefault)
    {
        return _value1.ToDateTime(inDefault);
    }
    #endregion DateTime

    #region TimeSpan
    public Variant(TimeSpan value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(TimeSpan value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }
    public TimeSpan ToTimeSpan(TimeSpan inDefault)
    {
        return _value1.ToTimeSpan(inDefault);
    }
    #endregion TimeSpan

    #region UINT
    public Variant(ulong value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(ulong value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public ulong ToULong(ulong inDefault = 0)
    {
        return _value1.ToULong(inDefault);
    }

    public uint ToUInt(uint inDefault = 0)
    {
        return _value1.ToUInt(inDefault);
    }
    #endregion UINT

    #region Bool
    public Variant(bool value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(bool value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public bool ToBool(bool inDefault = false)
    {
        return _value1.ToBool(inDefault);
    }

    #endregion Bool

    #region FLOAT
    public Variant(double value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(double value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public bool IsFloatable()
    {
        return VariantSmall.IsFloatable(VariantType);
    }

    public float ToFloat(float inDefault = 0)
    {
        return _value1.ToFloat(inDefault);
    }

    public double ToDouble(double inDefault = 0)
    {
        return _value1.ToDouble(inDefault);
    }
    #endregion FLOAT

    #region FLOAT[]
    public Variant(double[] value)
    {
        _objref = value;
        _value1 = new VariantSmall();
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)EVariantType.Float | ArrayBitMask;
    }

    public void Set(double[] value)
    {
        _objref = value;
        _value1.Clear();
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)EVariantType.Float | ArrayBitMask;
    }

    public double[] ToArrayFloat()
    {
        if (CheckType(EVariantType.Float, ArrayBitMask))
            return (double[])_objref;

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return new double[0];
    }

    #endregion FLOAT[]

    #region Vector3
    public Variant(Vector3 value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(Vector3 value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public Vector3 ToVector3(Vector3 inDefault)
    {
        return _value1.ToVector3(inDefault);
    }
    public Vector3 ToVector3()
    {
        return _value1.ToVector3();
    }
    #endregion Vector3

    #region Vector2
    public Variant(Vector2 value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(Vector2 value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public Vector2 ToVector2()
    {
        return _value1.ToVector2();
    }

    public Vector2 ToVector2(Vector2 inDefault)
    {
        return _value1.ToVector2(inDefault);
    }

    #endregion Vector2

    #region string
    public Variant(string value)
    {
        _objref = value;
        _value1 = new VariantSmall();
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)EVariantType.String;
    }

    public void Set(string value)
    {
        _objref = value;
        _value1.Clear();
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)EVariantType.String;
    }

    public string ToStringValue()
    {
        return ToStringValue(string.Empty);
    }

    public string ToStringValue(string inDefault)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.String))
            return (string)_objref;

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return inDefault;
    }
    #endregion string

    #region string_array
    public Variant(string[] value)
    {
        _objref = value;
        _value1 = new VariantSmall();
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)EVariantType.String | ArrayBitMask;
    }

    public void Set(string[] value)
    {
        _objref = value;
        _value1.Clear();
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)EVariantType.String | ArrayBitMask;
    }

    public string[] ToArrayStrings()
    {
        if (CheckType(EVariantType.String, ArrayBitMask))
            return (string[])_objref;

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return new string[0];
    }
    #endregion string

    #region NamedId
    public Variant(NamedId value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(NamedId value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public NamedId ToNamedId()
    {
        return _value1.ToNamedId();
    }

    public NamedId ToNamedId(NamedId inDefault)
    {
        return _value1.ToNamedId(inDefault);
    }

    #endregion NamedId

    #region Quaternion
    public Variant(Quaternion value)
    {
        _objref = null;
        _value1 = new VariantSmall(value);
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(Quaternion value)
    {
        _objref = null;
        _value1.Set(value);
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public Quaternion ToQuaternion()
    {
        return _value1.ToQuaternion();
    }

    public Quaternion ToQuaternion(Quaternion inDefault)
    {
        return _value1.ToQuaternion(inDefault);
    }
    #endregion Quaternion

    #region RVector
    public Variant(RVector value)
    {
        _objref = null;
        _value1 = new VariantSmall(value.Position);
        _value2 = new VariantSmall(value.Rotation);
        _ulong_value = 0;
        _flags = (int)EVariantType.RVector;
    }

    public void Set(RVector value)
    {
        Set(value.Position, value.Rotation);
    }

    public void Set(Vector3 Position, Quaternion Rotation)
    {
        _objref = null;
        _value1 = new VariantSmall(Position);
        _value2 = new VariantSmall(Rotation);
        _ulong_value = 0;
        _flags = (int)EVariantType.RVector;
    }

    public RVector ToRVector()
    {
        return ToRVector(RVector.zero);
    }

    public RVector ToRVector(RVector inDefault)
    {
        if (IsUndefined)
            return inDefault;

        if (!CheckType(EVariantType.RVector))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
            return inDefault;
        }

        Vector3 pos = _value1.ToVector3();
        Quaternion rot = _value2.ToQuaternion();
        return new RVector(pos, rot);
    }

    #endregion RVector

    #region RVector[]
    public Variant(RVector[] value)
    {
        _objref = value;
        _value1 = new VariantSmall();
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)EVariantType.RVector | ArrayBitMask;
    }

    public void Set(RVector[] value)
    {
        _objref = value;
        _value1.Clear();
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)EVariantType.RVector | ArrayBitMask;
    }

    public RVector[] ToArrayRVector()
    {
        if (CheckType(EVariantType.RVector, ArrayBitMask))
            return (RVector[])_objref;

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return new RVector[0];
    }

    #endregion RVector[]

    #region Color
    public Variant(Color value)
    {
        _objref = null;
        _value1 = new VariantSmall(new Vector2(value.r, value.g));
        _value2 = new VariantSmall(new Vector2(value.b, value.a));
        _ulong_value = 0;
        _flags = (int)EVariantType.Color;
    }

    public void Set(Color value)
    {
        _objref = null;
        _value1.Set(new Vector2(value.r, value.g));
        _value2.Set(new Vector2(value.b, value.a));
        _ulong_value = 0;
        _flags = (int)EVariantType.Color;
    }

    public Color ToColor(Color inDefault)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Color))
        {
            Vector2 v1 = _value1.ToVector2();
            Vector2 v2 = _value2.ToVector2();
            return new Color(v1.x, v1.y, v2.x, v2.y);
        }

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return inDefault;
    }
    #endregion Color

    public Variant(VariantSmall value)
    {
        _objref = null;
        _value1 = value;
        _value2 = new VariantSmall();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public void Set(VariantSmall value)
    {
        _objref = null;
        _value1 = value;
        _value2.Clear();
        _ulong_value = 0;
        _flags = (int)_value1.Flags;
    }

    public VariantSmall ToVariantSmall()
    {
        if (IsUndefined)
            return VariantSmall.Undefined;

        if (IsArray)
        {
            LogContext.WriteToLog(ELogLevel.ERROR, "Can't cast array to VariantSmall");
            return VariantSmall.Undefined;
        }

        if (VariantType >= EVariantType.String)
        {
            LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
            return VariantSmall.Undefined;
        }

        return _value1;
    }

    private const string ArrayDelimeter = "; ";
    private const string ArrayStringDelimeter = "\n";

    public override string ToString()
    {
        if (IsArray)
        {
            switch (VariantType)
            {
                case EVariantType.Bool: return ((bool[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.Int: return ((long[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.UInt: return ((ulong[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.Float: return ((double[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.Vector2: return ((Vector2[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.Vector3: return ((Vector3[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.Quaternion: return ((Quaternion[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.NamedId: return ((NamedId[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.DateTime: return ((DateTime[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.TimeSpan: return ((DateTime[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.String: return ((string[])_objref).ToStringExt(ArrayStringDelimeter);
                case EVariantType.RVector: return ((RVector[])_objref).ToStringExt(ArrayDelimeter);
                case EVariantType.Color: return ((Color[])_objref).ToStringExt(ArrayDelimeter);
            }

            return "Undefined";
        }
        else
        {
            switch (VariantType)
            {
                case EVariantType.Bool: return ToBool().ToString();
                case EVariantType.Int: return ToLong().ToString();
                case EVariantType.UInt: return ToULong().ToString();
                case EVariantType.Float: return ToDouble().ToStringExt();
                case EVariantType.Vector3: return ToVector3().ToStringExt();
                case EVariantType.Vector2: return ToVector2().ToStringExt();
                case EVariantType.Quaternion: return ToQuaternion().ToStringExt();
                case EVariantType.NamedId: return ToNamedId().ToString();
                case EVariantType.DateTime: return ToDateTime(DateTime.UtcNow).ToStringExt();//2021-11-05T14:28:13.7438608Z
                case EVariantType.TimeSpan: return ToTimeSpan(TimeSpan.Zero).ToStringExt(); //10.09:08:07.0060000 - 10 days, 9 hours, 8 minutes, 7 seconds, 6 milliseconds

                case EVariantType.String: return ToStringValue();
                case EVariantType.RVector: return ToRVector().ToString();
                case EVariantType.Color: return ToColor(Color.white).ToStringExt(); //0.1234,0.3,0.5,0.9
            }
            return "Undefined";
        }
    }

    const string array_prefix = "*";
    public string SaveToString()
    {
        var sb = new StringBuilder();
        if (IsArray)
            sb.Append(array_prefix);
        sb.Append(VariantType.ToStringExt());

        string vals = ToString();
        if(!string.IsNullOrEmpty(vals))
        {
            sb.Append(" ");
            sb.Append(vals);
        }
        //"*Float 12.4; 23; 6.7"
        //"*Vector3 12.4, 23, 6.7; 32.5, 56.2, 72.3"
        //"*String aaaaa\nasd fsdd; fsdfs\nsasda asd 2e sd23 ;a asda2,  AWSD"
        //
        return sb.ToString();
    }

    public bool LoadFromString(string inString)
    {
        this = CreateFromString(inString);
        return VariantType != EVariantType.Undefined;
    }

    public static Variant CreateFromString(string inString)
    {
        if(inString.IsEmpty())
            return Variant.Undefined;

        bool is_array = inString.StartsWith(array_prefix);
        int shift = is_array ? 1 : 0;
        var span_str1 = new SStringSpan(inString, shift);

        (EVariantType var_type, SStringSpan span_str2) = VariantStringConverter.ReadVariantType(span_str1);
        if(var_type == EVariantType.Undefined)
        {
            LogContext.WriteToLog(ELogLevel.ERROR, "Undefined variant", "String", new Variant(inString));
            return Variant.Undefined;
        }

        SStringSpan span_str3 = span_str2.ForwardTrim();
        if (span_str3.IsEmpty)
        {
            LogContext.WriteToLog(ELogLevel.ERROR, "Undefined variant - EmptyString", "String", new Variant(inString));
            return Variant.Undefined;
        }

        string str_val = span_str3.ToString();
        if (!is_array)
        {
            return TryParse(var_type, str_val);
        }

        string delimeter = var_type == EVariantType.String ? ArrayStringDelimeter : ArrayDelimeter;

        string[] str_vals = str_val.Split(new string[] { delimeter }, StringSplitOptions.RemoveEmptyEntries);

        object objref = null;
        switch (var_type)
        {
            case EVariantType.Bool:         objref = TryParseArrayAsBool(str_vals); break;
            case EVariantType.Int:          objref = TryParseArrayAsLong(str_vals); break;
            case EVariantType.UInt:         objref = TryParseArrayAsULong(str_vals); break;
            case EVariantType.Float:        objref = TryParseArrayAsDouble(str_vals); break;
            case EVariantType.Vector2:      objref = TryParseArrayAsVector2(str_vals); break;
            case EVariantType.Vector3:      objref = TryParseArrayAsVector3(str_vals); break;
            case EVariantType.Quaternion:   objref = TryParseArrayAsQuaternion(str_vals); break;
            case EVariantType.NamedId:      objref = TryParseArrayAsNamedId(str_vals); break;
            case EVariantType.DateTime:     objref = TryParseArrayAsDateTime(str_vals); break;
            case EVariantType.TimeSpan:     objref = TryParseArrayAsTimeSpan(str_vals); break;
            case EVariantType.RVector:      objref = TryParseArrayAsRVector(str_vals); break;
            case EVariantType.Color:        objref = TryParseArrayAsColor(str_vals); break;
            case EVariantType.String:       objref = str_vals; break;
        }

        Variant vr = new Variant();

        if (objref != null)
        {
            vr._flags = (int)var_type | ArrayBitMask;
            vr._objref = objref;
        }

        return vr;
    }

    static object TryParseArrayAsBool(string[] str_vals)
    {
        bool[] arr = new bool[str_vals.Length];
        for(int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!bool.TryParse(str.Trim(), out bool val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt bool", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsLong(string[] str_vals)
    {
        long[] arr = new long[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!long.TryParse(str.Trim(), out long val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt long", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsULong(string[] str_vals)
    {
        ulong[] arr = new ulong[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!ulong.TryParse(str.Trim(), out ulong val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt ulong", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsDouble(string[] str_vals)
    {
        double[] arr = new double[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!double.TryParse(str.Trim(), NumberStyles.Float, CultureInfo.InvariantCulture, out double val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt double", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsVector2(string[] str_vals)
    {
        Vector2[] arr = new Vector2[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!StringExt.TryParse(str.Trim(), out Vector2 val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt Vector2", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsVector3(string[] str_vals)
    {
        Vector3[] arr = new Vector3[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!StringExt.TryParse(str.Trim(), out Vector3 val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt Vector3", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsQuaternion(string[] str_vals)
    {
        Quaternion[] arr = new Quaternion[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!StringExt.TryParse(str.Trim(), out Quaternion val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt Quaternion", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsNamedId(string[] str_vals)
    {
        NamedId[] arr = new NamedId[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            arr[i] = NamedId.GetNamedId(str);
        }
        return arr;
    }

    static object TryParseArrayAsDateTime(string[] str_vals)
    {
        DateTime[] arr = new DateTime[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!DateTime.TryParse(str.Trim(), out DateTime val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt DateTime", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsTimeSpan(string[] str_vals)
    {
        TimeSpan[] arr = new TimeSpan[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!TimeSpan.TryParse(str.Trim(), out TimeSpan val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt TimeSpan", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsRVector(string[] str_vals)
    {
        RVector[] arr = new RVector[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!RVector.TryParse(str.Trim(), out RVector val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt RVector", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static object TryParseArrayAsColor(string[] str_vals)
    {
        Color[] arr = new Color[str_vals.Length];
        for (int i = 0; i < str_vals.Length; i++)
        {
            string str = str_vals[i];
            if (!StringExt.TryParse(str.Trim(), out Color val))
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Value isnt Color", "String", new Variant(str));
                return null;
            }
            arr[i] = val;
        }
        return arr;
    }

    static private Variant TryParse(EVariantType inVariantType, string inString)
    {
        switch(inVariantType)
        {
            case EVariantType.Bool:
                {
                    if (bool.TryParse(inString, out bool val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.Int:
                {
                    if (int.TryParse(inString, out int val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.UInt:
                {
                    if (uint.TryParse(inString, out uint val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.Float:
                {
                    if (double.TryParse(inString, out double val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.Vector2:
                {
                    if (StringExt.TryParse(inString, out Vector2 val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.Vector3:
                {
                    if (StringExt.TryParse(inString, out Vector3 val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.Quaternion:
                {
                    if (StringExt.TryParse(inString, out Quaternion val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.NamedId:
                {
                    return new Variant(NamedId.GetNamedId(inString));
                }
            case EVariantType.DateTime:
                {
                    if (DateTime.TryParse(inString, out DateTime val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.TimeSpan:
                {
                    if (TimeSpan.TryParse(inString, out TimeSpan val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.RVector:
                {
                    if (RVector.TryParse(inString, out RVector val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.Color:
                {
                    if (StringExt.TryParse(inString, out Color val))
                        return new Variant(val);
                    return Variant.Undefined;
                }
            case EVariantType.String:
                {
                    return new Variant(inString);
                }
        }

        LogContext.WriteToLog(ELogLevel.ERROR, "Unknown type of Variant");
        return new Variant();
    }
}
