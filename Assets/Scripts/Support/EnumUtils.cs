﻿using System;
using System.Collections.Generic;

public static class EnumUtils
{
    public static T ToEnum<T>(string value)
    {
        if (string.IsNullOrEmpty(value) || !Enum.IsDefined(typeof(T), value))
            throw new ArgumentException(string.Format("{0} is not defined", value));

        return (T)Enum.Parse(typeof(T), value);
    }

    public static T ToEnum<T>(string value, T default_value)
    {
        if (string.IsNullOrEmpty(value) || !Enum.IsDefined(typeof(T), value))
            return default_value;

        return (T)Enum.Parse(typeof(T), value);
    }

    public static bool TryParse<T>(string svalue, out T out_value)
    {
        out_value = default(T);
        if (string.IsNullOrEmpty(svalue) || !Enum.IsDefined(typeof(T), svalue))
            return false;

        out_value = (T)Enum.Parse(typeof(T), svalue);
        return true;
    }

    public static void Foreach<T>(Action<T> action)
    {
        foreach (T item in Enum.GetValues(typeof(T)))
            action(item);
    }

    public static Dictionary<string, T> EnumToNameDictionary<T>()
    {
        var dic = new Dictionary<string, T>();
        foreach (T item in Enum.GetValues(typeof(T)))
        {
            dic.Add(item.ToString(), item);
        }
        return dic;
    }
}
