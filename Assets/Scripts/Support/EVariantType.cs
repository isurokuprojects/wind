﻿using System;
using System.Collections.Generic;

public enum EVariantType : byte
{
    Undefined = 0,
    Bool,
    Float,
    Int,
    UInt,
    DateTime,
    TimeSpan,
    Vector3,
    Vector2,
    Quaternion,
    NamedId, //влазят в 12 байт

    //не влазят в 12 байт
    String, //важная позиция: до - конвертируем в VariantSmall, с - нет
    RVector,
    Color,
}

static class VariantStringConverter
{
    static Dictionary<string, EVariantType> _dic_st;
    static string[] _array_ts;

    public static string ToStringExt(this EVariantType inType)
    {
        CreateCaches();
        int i = (int)inType;
        return _array_ts[i];
    }

    public static EVariantType StringToType(string inName)
    {
        CreateCaches();
        if (_dic_st.TryGetValue(inName, out EVariantType tp))
            return tp;
        return EVariantType.Undefined;
    }

    static void CreateCaches()
    {
        if (_array_ts != null)
            return;

        Array arr = Enum.GetValues(typeof(EVariantType));
        _dic_st = new Dictionary<string, EVariantType>();
        _array_ts = new string[arr.Length];
        for(int i = 0; i < arr.Length; i++)
        {
            EVariantType v = (EVariantType)arr.GetValue(i);
            string s = v.ToString();
            _dic_st.Add(s, v);
            _array_ts[i] = s;
        }
    }

    static public (EVariantType, SStringSpan) ReadVariantType(in SStringSpan inStr)
    {
        for(int i = 0; i < _array_ts.Length; i++)
        {
            if (inStr.StartsWith(_array_ts[i]))
                return ((EVariantType)i, new SStringSpan(inStr.Source, inStr.Start + _array_ts[i].Length));
        }
        return (EVariantType.Undefined, new SStringSpan());
    }
}