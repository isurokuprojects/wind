﻿public static class HashFunctions
{
    public static int HashCombine(int A, int C)
    {
        return (int)HashCombine((uint)A, (uint)C);
    }

    public static uint HashCombine(uint A, int C)
    {
        return HashCombine(A, (uint)C);
    }

    public static uint HashCombine(uint A, uint C)
    {
        //Предполагается, что магическое число B состоит из 32 случайных битов, 
        //каждый из которых с равной вероятностью будет 0 или 1, и без простой корреляции между битами. 
        //Обычный способ найти строку таких битов -использовать двоичное расширение иррационального числа; 
        //в данном случае это число является обратной величиной золотого сечения:
        //phi = (1 + sqrt(5)) / 2
        //2 ^ 32 / phi = 0x9e3779b9
        uint B = 0x9e3779b9;

        //http://burtleburtle.net/bob/hash/doobs.html
        //далее идет смешение из статьи выше. Суть: быстро и правильно смешать так, чтобы все биты учавствовали
        //и не затеняли друг друга
        //Этаже функция используется в unreal engine
        A += B;
        A -= B; A -= C; A ^= (C >> 13);
        B -= C; B -= A; B ^= (A << 8);
        C -= A; C -= B; C ^= (B >> 13);
        A -= B; A -= C; A ^= (C >> 12);
        B -= C; B -= A; B ^= (A << 16);
        C -= A; C -= B; C ^= (B >> 5);
        A -= B; A -= C; A ^= (C >> 3);
        B -= C; B -= A; B ^= (A << 10);
        C -= A; C -= B; C ^= (B >> 15);

        return C;
    }
}