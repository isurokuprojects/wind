﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Reflection;

public static class TransformExtensions
{
    /// <summary>
    /// Find all children of the Transform by tag (includes self)
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="tags"></param>
    /// <returns></returns>
    public static IEnumerable<Transform> FindChildrenByTag(this Transform transform, params string[] tags)
    {
        List<Transform> list = new List<Transform>();
        foreach (var tran in transform.Cast<Transform>())
            list.AddRange(tran.FindChildrenByTag(tags)); // recursively check children
        if (tags.Any(tag => string.Equals(tag, transform.tag)))
            list.Add(transform); // we matched, add this transform
        return list;
    }

    /// <summary>
    /// Find all children of the GameObject by tag (includes self)
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="tags"></param>
    /// <returns></returns>
    public static IEnumerable<GameObject> FindChildrenByTag(this GameObject gameObject, params string[] tags)
    {
        return FindChildrenByTag(gameObject.transform, tags)
            //.Cast<GameObject>() // Can't use Cast here :(
            .Select(tran => tran.gameObject)
            .Where(gameOb => gameOb != null);
    }

    /// <summary>
    /// Finds a first deep child Transform with given name
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public static Transform FindChildByName(this Transform parent, string name)
    {
        foreach (Transform child in parent)
        {
            if (child.name == name)
                return child;

            Transform result = child.FindChildByName(name);
            if (result != null)
                return result;
        }

        return null;
    }

    /// <summary>
    /// Finds a first deep child GameObject with given name
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public static GameObject FindChildByName(this GameObject parent, string name)
    {
        Transform child = parent.transform.FindChildByName(name);
        if (child != null)
            return child.gameObject;

        return null;
    }

    public static T FindChildByName<T>(this Transform parent, string name, bool silent = false) where T : Component
    {
        var children = parent.GetComponentsInChildren<T>(true);
        if (children != null)
        {
            foreach (var child in children)
            {
                if (child.name == name) return child;
            }
        }

#if ASSERTS
		if (!silent)
		{
			CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => nameof(TransformExtensions), () => MethodBase.GetCurrentMethod().Name, () => $"{typeof(T)} in {name} not found");
		}
#endif
        return default;
    }

    public static IEnumerable<T> FindChildrenByName<T>(this Transform parent, string name)
    {
        List<T> result = new List<T>();

        FindChildrenByName(parent, name, result);

        return result;
    }

    public static void FindChildrenByName<T>(this Transform obj, string name, List<T> outList)
    {
        if (string.Equals(obj.name, name))
        {
            T result = obj.GetComponent<T>();

            if (result == null)
            {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "TransformExtensions", () => MethodBase.GetCurrentMethod().Name, () => string.Format("{0} in {1} not found", typeof(T).ToString(), name));
#endif
            }

            if (result != null)
            {
                outList.Add(result);
            }
        }

        for (int i = 0; i < obj.childCount; i++)
        {
            FindChildrenByName(obj.GetChild(i), name, outList);
        }
    }

    public static T FindChildByName<T>(this GameObject parent, string name) where T : Component
    {
        return parent.transform.FindChildByName<T>(name);
    }

    public static GameObject FindChildBy<T>(this GameObject inGameObject, Predicate<T> inPredicate) where T : MonoBehaviour
    {
        var child = inGameObject.GetComponentsInChildren<T>().FirstOrDefault(entity => inPredicate(entity));

        if (child != null)
            return child.gameObject;

        return null;
    }

    public static Transform FindChildBy<T>(this Transform inTransform, Predicate<T> inPredicate) where T : MonoBehaviour
    {
        var child = inTransform.GetComponentsInChildren<T>().FirstOrDefault(entity => inPredicate(entity));

        if (child != null)
            return child.transform;

        return null;
    }

    public static IEnumerable<GameObject> FindChildrenBy<T>(this GameObject inGameObject, Predicate<T> inPredicate) where T : MonoBehaviour
    {
        return inGameObject.GetComponentsInChildren<T>().Where(entity => inPredicate(entity)).Select(entity => entity.gameObject);
    }

    public static void FindChildrenByName(this Transform inTransform, string inName, List<GameObject> outGameObjects)
    {
        foreach (Transform child in inTransform)
        {
            if (string.Equals(child.name, inName))
                outGameObjects.Add(child.gameObject);

            FindChildrenByName(child, inName, outGameObjects);
        }
    }

    public static void FindChildrenBySpecName(this Transform inTransform, Action<Transform> inGOAction)
    {
        foreach (Transform child in inTransform)
        {
            if (child.name[0] == '$')
                inGOAction(child);

            FindChildrenBySpecName(child, inGOAction);
        }
    }

    public static IEnumerable<Transform> FindChildrenBy<T>(this Transform inTransform, Predicate<T> inPredicate, bool includeInactive) where T : MonoBehaviour
    {
        return inTransform.GetComponentsInChildren<T>(includeInactive).Where(entity => inPredicate(entity)).Select(entity => entity.transform);
    }

    public static Transform GetMainParent(this Transform inTr)
    {
        if (inTr == null)
            return null;

        if (inTr.parent == null)
            return inTr;
        return GetMainParent(inTr.parent);
    }

    public static T GetComponentWithCheck<T>(this Transform inTransform) where T : Component
    {
        T res = inTransform.GetComponent<T>();

        if (res == null)
        {
#if ASSERTS
            CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "TransformExtensions", () => MethodInfo.GetCurrentMethod().Name,
                       () => string.Format("{0}: Here must be {1} component", inTransform.name, typeof(T).Name));
#endif
        }

        return res;
    }

    public static IEnumerable<string> GetDisabledChildName(Transform tObject, bool inExludeBaseTransform = true)
    {
        string base_name = tObject.name;
        var disableNames = new List<string>();
        var childTransforms = tObject.GetComponentsInChildren<Transform>(true);
        foreach (var t in childTransforms)
        {
            if (!t.gameObject.activeSelf)
            {
                if (inExludeBaseTransform && string.Equals(base_name, t.name))
                    continue;

                if (disableNames.Contains(t.name))
                    continue;

                disableNames.Add(t.name);
            }
        }

        return disableNames;
    }

    //public static SRayCastResult RayCast(this Transform inTransform, Vector3 inPos, Vector3 inDir, float inDist, Vector3 inUnityShift)
    //{
    //    Collider[] colliders = inTransform.GetComponentsInChildren<Collider>();
    //    return RayCast(colliders, inPos, inDir, inDist, inUnityShift);
    //}

    //public static SRayCastResult RayCast(Collider[] inColliders, Vector3 inPos, Vector3 inDir, float inDist, Vector3 inUnityShift)
    //{
    //    Ray ray = new Ray(inPos + inUnityShift, inDir);
    //    RaycastHit res_hit = new RaycastHit();
    //    bool was_found = false;
    //    foreach (var col in inColliders)
    //    {
    //        RaycastHit hit;
    //        if (col.Raycast(ray, out hit, inDist))
    //        {
    //            if (!was_found || hit.distance < res_hit.distance)
    //            {
    //                res_hit = hit;
    //                was_found = true;
    //            }
    //        }
    //    }

    //    if (was_found)
    //        return new SRayCastResult(ERayCastResult.Obstacle, res_hit, inUnityShift);
    //    return new SRayCastResult(inDist);
    //}

    public static string GetPath(this Transform tObject)
    {
        if (tObject == null)
            return "null";

        Transform t = tObject;
        string s = t.name;
        while (t.parent != null)
        {
            s = string.Format("{0}/{1}", t.parent.name, s);
            t = t.parent;
        }
        return s;
    }

    public static void SetLayerAllChildren(this Transform obj, int inLayer)
    {
        for (int i = 0; i < obj.childCount; i++)
        {
            obj.GetChild(i).gameObject.layer = inLayer;
            SetLayerAllChildren(obj.GetChild(i), inLayer);
        }
    }
}