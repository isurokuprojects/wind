﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

[StructLayout(LayoutKind.Explicit, Pack = 4)]
public struct VariantSmall
{
    [FieldOffset(0)]
    ulong _long_value;
    [FieldOffset(0)]
    double _double_value;

    [FieldOffset(8)]
    uint _int_value;
    [FieldOffset(8)]
    float _float_value;

    [FieldOffset(12)]
    int _flags; //12 + 4 = 16

    const int TypeCodeBitMask = 0xff;

    public void Clear()
    {
        _flags = 0;
        _long_value = 0;
        _int_value = 0;
    }

    public int Flags { get { return _flags; } }

    public EVariantType VariantType
    {
        get
        {
            byte t = (byte)(TypeCodeBitMask & _flags);
            return (EVariantType)t;
        }
    }

    public static VariantSmall Undefined = new VariantSmall();

    public bool IsUndefined { get { return _flags == 0; } }

    public static bool Approximately(VariantSmall A, VariantSmall B, double inEpsilon = 0.0001)
    {
        if (A.IsFloatable() && B.IsFloatable())
        {
            return GameMath.Approximately(A.ToFloat(), B.ToFloat(), inEpsilon);
        }
        return A.Equals(B);
    }

    #region Equals
    public bool Equals(VariantSmall other)
    {
        return
            _flags == other._flags
            && _long_value == other._long_value
            && _int_value == other._int_value;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is VariantSmall && Equals((VariantSmall)obj);
    }

    public static bool operator ==(VariantSmall left, VariantSmall right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(VariantSmall left, VariantSmall right)
    {
        return !left.Equals(right);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int r = _long_value.GetHashCode();
            r = HashFunctions.HashCombine(r, (int)_int_value);
            r = HashFunctions.HashCombine(r, _flags);
            return r;
        }
    }
    #endregion //Equals

    bool CheckType(EVariantType inNeedType)
    {
        int it = (int)inNeedType;
        return (TypeCodeBitMask & _flags ^ it) == 0;
    }

    public VariantSmall(VariantSmall value)
    {
        _long_value = value._long_value;
        _double_value = value._double_value;
        _int_value = value._int_value;
        _float_value = value._float_value;

        _flags = value._flags;
    }

    #region INT
    public VariantSmall(long value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = (ulong)value;

        _flags = (int)EVariantType.Int;
    }

    public void Set(long value)
    {
        Clear();

        _long_value = (ulong)value;
        _flags = (int)EVariantType.Int;
    }

    public int ToInt(int inDefault = 0)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Bool))
            return _long_value > 0 ? 1 : 0;

        if (!CheckType(EVariantType.Int))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
            return inDefault;
        }

        return (int)_long_value;
    }

    public long ToLong(long inDefault = 0)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Int))
            return (long)_long_value;
        if (CheckType(EVariantType.DateTime))
            return (long)_long_value;
        if (CheckType(EVariantType.TimeSpan))
            return (long)_long_value;
        if (CheckType(EVariantType.Bool))
            return _long_value > 0 ? 1 : 0;

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return inDefault;
    }

    #endregion INT

    #region DateTime
    public VariantSmall(DateTime value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = (ulong)value.ToBinary();

        _flags = (int)EVariantType.DateTime;
    }

    public void Set(DateTime value)
    {
        Clear();

        _long_value = (ulong)value.ToBinary();

        _flags = (int)EVariantType.DateTime;
    }

    public DateTime ToDateTime(DateTime inDefault)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.DateTime) || CheckType(EVariantType.Int))
            return DateTime.FromBinary((long)_long_value);

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");

        return inDefault;
    }
    #endregion DateTime

    #region TimeSpan
    public VariantSmall(TimeSpan value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = (ulong)value.Ticks;

        _flags = (int)EVariantType.TimeSpan;
    }

    public void Set(TimeSpan value)
    {
        Clear();

        _long_value = (ulong)value.Ticks;

        _flags = (int)EVariantType.TimeSpan;
    }
    public TimeSpan ToTimeSpan(TimeSpan inDefault)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.TimeSpan) || CheckType(EVariantType.Int))
            return TimeSpan.FromTicks((long)_long_value);

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");

        return inDefault;
    }
    #endregion TimeSpan

    #region UINT
    public VariantSmall(ulong value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = value;

        _flags = (int)EVariantType.UInt;
    }

    public void Set(ulong value)
    {
        Clear();

        _long_value = value;

        _flags = (int)EVariantType.UInt;
    }

    public uint ToUInt(uint inDefault = 0)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Bool))
            return _long_value > 0 ? 1u : 0u;

        if (!CheckType(EVariantType.UInt))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");

            return inDefault;
        }

        return (uint)_long_value;
    }

    public ulong ToULong(ulong inDefault = 0)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Bool))
            return _long_value > 0 ? 1u : 0u;

        if (!CheckType(EVariantType.UInt))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
            return inDefault;
        }

        return _long_value;
    }
    #endregion UINT

    #region Bool
    public VariantSmall(bool value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = value ? 1u : 0;

        _flags = (int)EVariantType.Bool;
    }

    public void Set(bool value)
    {
        Clear();
        _long_value = value ? 1u : 0;
        _flags = (int)EVariantType.Bool;
    }

    public bool ToBool(bool inDefault = false)
    {
        if (IsUndefined)
            return inDefault;

        if (!CheckType(EVariantType.Bool))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
            return inDefault;
        }

        return _long_value > 0;
    }

    #endregion Bool

    #region FLOAT
    public VariantSmall(double value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _double_value = value;
        _flags = (int)EVariantType.Float;
    }

    public void Set(double value)
    {
        Clear();
        _double_value = value;
        _flags = (int)EVariantType.Float;
    }

    public static bool IsFloatable(EVariantType inType)
    {
        return inType == EVariantType.Float
            || inType == EVariantType.Int
            || inType == EVariantType.UInt
            || inType == EVariantType.Bool;
    }

    public bool IsFloatable()
    {
        return IsFloatable(VariantType);
    }

    public float ToFloat(float inDefault = 0)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Float))
            return (float)_double_value;
        if (CheckType(EVariantType.Int))
            return _long_value;
        if (CheckType(EVariantType.UInt))
            return _long_value;
        if (CheckType(EVariantType.Bool))
            return _long_value > 0 ? 1 : 0;

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return inDefault;
    }

    public double ToDouble(double inDefault = 0)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Float))
            return _double_value;
        if (CheckType(EVariantType.Int))
            return _long_value;
        if (CheckType(EVariantType.UInt))
            return _long_value;
        if (CheckType(EVariantType.Bool))
            return _long_value > 0 ? 1 : 0;

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return inDefault;
    }
    #endregion FLOAT

    #region Vector3
    public VariantSmall(Vector3 value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = GameMath.TwoFloatToULong(value.x, value.y);
        _float_value = value.z;

        _flags = (int)EVariantType.Vector3;
    }

    public void Set(Vector3 value)
    {
        Clear();

        _long_value = GameMath.TwoFloatToULong(value.x, value.y);
        _float_value = value.z;

        _flags = (int)EVariantType.Vector3;
    }

    public Vector3 ToVector3()
    {
        return ToVector3(Vector3.zero);
    }

    public Vector3 ToVector3(Vector3 inDefault)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Vector3))
        {
            float x, y;
            GameMath.ULongToFloat(_long_value, out x, out y);
            return new Vector3(x, y, _float_value);
        }

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return inDefault;
    }

    #endregion Vector3

    #region Quaternion
    public VariantSmall(Quaternion value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = GameMath.TwoFloatToULong(value.x, value.y);
        _float_value = value.z;

        _flags = (int)EVariantType.Quaternion;
    }

    public void Set(Quaternion value)
    {
        Clear();

        _long_value = GameMath.TwoFloatToULong(value.x, value.y);
        _float_value = value.z;

        _flags = (int)EVariantType.Quaternion;
    }

    public Quaternion ToQuaternion()
    {
        return ToQuaternion(Quaternion.identity);
    }

    public Quaternion ToQuaternion(Quaternion inDefault)
    {
        if (IsUndefined)
            return inDefault;

        if (CheckType(EVariantType.Quaternion))
        {
            float x, y;
            GameMath.ULongToFloat(_long_value, out x, out y);
            float w = (float)Math.Sqrt(1 - x*x - y*y - _float_value * _float_value);
            return new Quaternion(x, y, _float_value, w);
        }

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return inDefault;
    }

    #endregion Quaternion

    #region Vector2

    public VariantSmall(Vector2 value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = GameMath.TwoFloatToULong(value.x, value.y);

        _flags = (int)EVariantType.Vector2;
    }

    public void Set(Vector2 value)
    {
        Clear();

        _long_value = GameMath.TwoFloatToULong(value.x, value.y);

        _flags = (int)EVariantType.Vector2;
    }

    public Vector2 ToVector2()
    {
        return ToVector2(Vector2.zero);
    }

    public Vector2 ToVector2(Vector2 inDefault)
    {
        if (IsUndefined)
            return inDefault;

        if (!CheckType(EVariantType.Vector2))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
            return inDefault;
        }

        float x, y;
        GameMath.ULongToFloat(_long_value, out x, out y);
        return new Vector2(x, y);
    }

    #endregion Vector

    #region NamedId
    public VariantSmall(NamedId value)
    {
        _long_value = 0;
        _double_value = 0;
        _int_value = 0;
        _float_value = 0;

        _long_value = value.id;

        _flags = (int)EVariantType.NamedId;
    }

    public void Set(NamedId value)
    {
        Clear();

        _long_value = value.id;

        _flags = (int)EVariantType.NamedId;
    }

    public NamedId ToNamedId()
    {
        return ToNamedId(NamedId.Empty);
    }

    public NamedId ToNamedId(NamedId inDefault)
    {
        if (IsUndefined)
            return inDefault;
        
        if (CheckType(EVariantType.NamedId))
            return NamedId.GetNamedId((uint)_long_value);

        LogContext.WriteToLog(ELogLevel.ERROR, $"Error type: {VariantType}");
        return inDefault;
    }

    #endregion NamedId

    public override string ToString()
    {
        switch (VariantType)
        {
            case EVariantType.Bool: return ToBool().ToString();
            case EVariantType.Int: return ToLong().ToString();
            case EVariantType.UInt: return ToULong().ToString();
            case EVariantType.Float: return ToDouble().ToStringExt();
            case EVariantType.Vector3: return ToVector3().ToStringExt();
            case EVariantType.Vector2: return ToVector2().ToStringExt();
            case EVariantType.Quaternion: return ToQuaternion().eulerAngles.ToStringExt();
            case EVariantType.NamedId: return ToNamedId().ToString();
            case EVariantType.DateTime: return ToDateTime(DateTime.UtcNow).ToString("o");//2021-11-05T14:28:13.7438608Z
            case EVariantType.TimeSpan: return ToTimeSpan(TimeSpan.Zero).ToString(); //10.09:08:07.0060000 - 10 days, 9 hours, 8 minutes, 7 seconds, 6 milliseconds
        }
        return "Undefined";
    }

    public static bool IsMathSupport(EVariantType inType)
    {
        return inType == EVariantType.Float
            || inType == EVariantType.Int
            || inType == EVariantType.Vector3
            || inType == EVariantType.Vector2
            || inType == EVariantType.Bool;
    }

    public static EVariantType GetMathCompatibleType(EVariantType inType1, EVariantType inType2)
    {
        if (inType1 == inType2)
            return inType1;

        if (inType1 == EVariantType.Float && inType2 == EVariantType.Int
            || inType2 == EVariantType.Float && inType1 == EVariantType.Int)
            return EVariantType.Float;

        return EVariantType.Undefined;
    }

    public enum ESumResult { Success, BothUndefined, NotEqualsType, MathUnSupport, ForgottenType, ArrayPresent }
    public static ESumResult GetSum(VariantSmall v1, VariantSmall v2, ref VariantSmall sum)
    {
        if (v1.IsUndefined && v2.IsUndefined)
        {
            return ESumResult.BothUndefined;
        }

        if (!v1.IsUndefined && v2.IsUndefined)
        {
            sum = v1;
            return ESumResult.Success;
        }

        if (v1.IsUndefined && !v2.IsUndefined)
        {
            sum = v2;
            return ESumResult.Success;
        }

        EVariantType compatible_type = GetMathCompatibleType(v1.VariantType, v2.VariantType);

        if (compatible_type == EVariantType.Undefined)
            return ESumResult.NotEqualsType;

        if (!IsMathSupport(compatible_type))
            return ESumResult.MathUnSupport;

        switch (compatible_type)
        {
            case EVariantType.Float:
                sum.Set(v1.ToDouble() + v2.ToDouble());
                return ESumResult.Success;
            case EVariantType.Int:
                sum.Set(v1.ToInt() + v2.ToInt());
                return ESumResult.Success;
            case EVariantType.Vector3:
                sum.Set(v1.ToVector3() + v2.ToVector3());
                return ESumResult.Success;
            case EVariantType.Vector2:
                sum.Set(v1.ToVector2() + v2.ToVector2());
                return ESumResult.Success;
            case EVariantType.Bool:
                sum.Set(v1.ToBool() || v2.ToBool());
                return ESumResult.Success;
        }

        return ESumResult.ForgottenType;
    }
}
