﻿using System;
using System.Collections;
using System.Collections.Generic;

public sealed class CUpdatingList<T> : IAddOnlyList<T>, IReadOnlyList<T>
{
    private sealed class ObjComparer : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            return y.GetHashCode().CompareTo(x.GetHashCode());
        }
    }
    IComparer<T> _comparer;

    private List<T> _list = new List<T>();
    private List<int> _deleted = new List<int>();
    private List<T> _added = new List<T>();

    public T this[int index] { get { return _list[index]; } }
    public int Count { get { return _list.Count; } }

    bool _updated;

    bool _no_change_order;

    class CUpdateMarker : IDisposable
    {
        CUpdatingList<T> _owner;

        public CUpdateMarker(CUpdatingList<T> owner) { _owner = owner; }

        public void Dispose()
        {
            _owner.CloseUpdate();
        }
    }

    CUpdateMarker _marker;

    public CUpdatingList(bool inNoChangeOrder = false)
    {
        _no_change_order = inNoChangeOrder;
        _marker = new CUpdateMarker(this);

        _comparer = new ObjComparer();
    }

    public CUpdatingList(IComparer<T> comparer)
    {
        _marker = new CUpdateMarker(this);

        _comparer = comparer;
    }


    public IDisposable StartUpdate()
    {
        _updated = true;
        return _marker;
    }

    void CloseUpdate()
    {
        for (int i = _deleted.Count - 1; i >= 0; i--)
            _list.RemoveAt(_deleted[i]);
        _deleted.Clear();

        for (int i = 0; i < _added.Count; i++)
            AddToMain(_added[i]);

        _added.Clear();

        _updated = false;
    }

    public void Add(T item)
    {
        if (_updated)
        {
            _added.Add(item);
            return;
        }

        AddToMain(item);
    }

    void AddToMain(T item)
    {
        int index = -1;
        if (_no_change_order)
            index = _list.IndexOf(item);
        else
            index = _list.BinarySearch(item, _comparer);

        if (index >= 0)
            return; //уже есть в списке

        index = ~index;
        _list.Insert(index, item);
    }

    void RemoveFromMain(T item)
    {
        if (_no_change_order)
        {
            _list.Remove(item);
            return;
        }

        int index = _list.BinarySearch(item, _comparer);
        if (index < 0)
            return;

        _list.RemoveAt(index);
    }

    public void Remove(T item)
    {
        if (!_updated)
        {
            RemoveFromMain(item);
            return;
        }

        int index = -1;
        if (_no_change_order)
            index = _list.IndexOf(item);
        else
            index = _list.BinarySearch(item, _comparer);

        if (index < 0)
            return;

        int pos = _deleted.BinarySearch(index);
        if (pos >= 0)
            return; //already deleted

        pos = ~pos;
        _deleted.Insert(pos, index);
    }

    public bool IsDeleted(int index)
    {
        int pos = _deleted.BinarySearch(index);
        return pos >= 0; //already deleted
    }

    public bool Contains(T item)
    {
        if (_no_change_order)
            return _list.Contains(item);

        int index = _list.BinarySearch(item, _comparer);
        return index >= 0;
    }

    public bool Exists(Predicate<T> match) { return _list.Exists(match); }

    public IEnumerator<T> GetEnumerator() { return _list.GetEnumerator(); }

    IEnumerator IEnumerable.GetEnumerator() { return _list.GetEnumerator(); }

    internal void Clear() { _list.Clear(); }
}

