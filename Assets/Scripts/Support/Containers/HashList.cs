﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class CHashList<K> : IEnumerable<K>
{
    private readonly Dictionary<K, int> _ent_pos;
    private readonly List<K> _entities;

    public CHashList()
    {
        _ent_pos = new Dictionary<K, int>();
        _entities = new List<K>();
    }

    public CHashList(IEqualityComparer<K> comparer)
    {
        _ent_pos = new Dictionary<K, int>(comparer);
        _entities = new List<K>();
    }

    public override string ToString()
    {
        var sb = new StringBuilder();

        sb.AppendFormat("[{0}]", Count);
        if (Count == 0)
            return sb.ToString();

        sb.Append(": ");

        int cnt = 3;
        if (Count < cnt)
            cnt = Count;

        for (int i = 0; i < cnt; ++i)
        {
            if (i == 0)
                sb.AppendFormat("{0}", _entities[i]);
            else
                sb.AppendFormat("; {0}", _entities[i]);
        }

        if (Count > cnt)
            sb.Append("...");

        return sb.ToString();
    }

    public bool Add(K inKey)
    {
        if (_ent_pos.ContainsKey(inKey))
            return false;

        int lst_pos = _entities.Count;
        _entities.Add(inKey);
        _ent_pos.Add(inKey, lst_pos);

        return true;
    }

    public void Add(IEnumerable<K> inKeys)
    {
        foreach (K k in inKeys)
            Add(k);
    }

    public bool Remove(K inKey)
    {
        int pos;
        if (!_ent_pos.TryGetValue(inKey, out pos))
            return false;

        Remove(inKey, pos);
        return true;
    }

    public void RemoveAt(int index)
    {
        var ent = _entities[index];
        Remove(ent, index);
    }

    private void Remove(K inKey, int pos)
    {
        _ent_pos.Remove(inKey);

        if (pos < _entities.Count - 1)
        {
            var last_ent = _entities[_entities.Count - 1];
            _entities[pos] = last_ent;

            _ent_pos[last_ent] = pos;

            _entities.RemoveAt(_entities.Count - 1);
        }
        else
            _entities.RemoveAt(pos);
    }

    public void Clear()
    {
        _ent_pos.Clear();
        _entities.Clear();
    }

    public int Count { get { return _entities.Count; } }
    public K GetKeyByIndex(int inIndex) { return _entities[inIndex]; }

    public int GetIndexByKey(K inKey)
    {
        if (_ent_pos.TryGetValue(inKey, out int pos))
            return pos;

        return -1;
    }

    public bool Contains(K inKey) { return _ent_pos.ContainsKey(inKey); }

    public IEnumerator<K> GetEnumerator() { return _entities.GetEnumerator(); }
    IEnumerator IEnumerable.GetEnumerator() { return _entities.GetEnumerator(); }

    public K this[int index] { get { return _entities[index]; } }

    public K[] ToArray()
    {
        K[] array = new K[_entities.Count];
        for (int i = 0; i < _entities.Count; i++)
            array[i] = _entities[i];
        return array;
    }

    public int ToArray(ref K[] ioArray, bool inCut = false)
    {
        if (ioArray == null)
            ioArray = new K[_entities.Count];
        else if (ioArray.Length < _entities.Count || ioArray.Length > _entities.Count && inCut)
            Array.Resize(ref ioArray, _entities.Count);

        for (int i = 0; i < _entities.Count; i++)
            ioArray[i] = _entities[i];

        return _entities.Count;
    }
}