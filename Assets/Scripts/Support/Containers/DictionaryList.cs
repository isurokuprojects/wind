﻿using System;
using System.Collections.Generic;
using System.Text;


public class CDicionaryList<K, V>
{
    private readonly Dictionary<K, int> _ent_pos;
    private readonly List<KeyValuePair<K, V>> _entities;

    public CDicionaryList()
    {
        _ent_pos = new Dictionary<K, int>();
        _entities = new List<KeyValuePair<K, V>>();
    }

    public CDicionaryList(IEqualityComparer<K> comparer)
    {
        _ent_pos = new Dictionary<K, int>(comparer);
        _entities = new List<KeyValuePair<K, V>>();
    }

    public CDicionaryList(CDicionaryList<K, V> inDic)
    {
        _ent_pos = new Dictionary<K, int>(inDic._ent_pos, inDic._ent_pos.Comparer);
        _entities = new List<KeyValuePair<K, V>>(inDic._entities);
    }

    public override string ToString()
    {
        var sb = new StringBuilder();

        sb.AppendFormat("[{0}]", Count);
        if (Count == 0)
            return sb.ToString();

        sb.Append(": ");

        int cnt = 3;
        if (Count < cnt)
            cnt = Count;

        for (int i = 0; i < cnt; ++i)
        {
            if (i == 0)
                sb.AppendFormat("{0}:{1}", _entities[i].Key, _entities[i].Value);
            else
                sb.AppendFormat("; {0}:{1}", _entities[i].Key, _entities[i].Value);
        }

        if (Count > cnt)
            sb.Append("...");

        return sb.ToString();
    }

    public void CopyFrom(CDicionaryList<K, V> inDic)
    {
        _ent_pos.Clear();
        _entities.Clear();

        foreach (var kv in inDic._ent_pos)
            _ent_pos.Add(kv.Key, kv.Value);
        _entities.AddRange(inDic._entities);
    }

    public bool CopyFrom(K[] inKeys, V[] inValues)
    {
        if (inKeys.Empty())
        {
            _ent_pos.Clear();
            _entities.Clear();
            return true;
        }

        if (inKeys.Length != inValues.Length)
        {
            return false;
        }

        _ent_pos.Clear();
        _entities.Clear();

        for (int i = 0; i < inKeys.Length; i++)
        {
            int lst_pos = _entities.Count;
            _entities.Add(new KeyValuePair<K, V>(inKeys[i], inValues[i]));
            _ent_pos.Add(inKeys[i], lst_pos);
        }

        return true;
    }

    public int Add(K inKey, V inValue)
    {
        if (_ent_pos.ContainsKey(inKey))
            return -1;

        int lst_pos = _entities.Count;
        _entities.Add(new KeyValuePair<K, V>(inKey, inValue));
        _ent_pos.Add(inKey, lst_pos);

        return lst_pos;
    }

    public bool Remove(K inKey)
    {
        int pos;
        if (!_ent_pos.TryGetValue(inKey, out pos))
            return false;

        _ent_pos.Remove(inKey);

        if (pos < _entities.Count - 1)
        {
            var last_ent = _entities[_entities.Count - 1];
            _entities[pos] = last_ent;

            _ent_pos[last_ent.Key] = pos;

            _entities.RemoveAt(_entities.Count - 1);
        }
        else
            _entities.RemoveAt(pos);

        return true;
    }

    public void Clear()
    {
        _ent_pos.Clear();
        _entities.Clear();
    }

    public int Count { get { return _entities.Count; } }
    public K GetKeyByIndex(int inIndex) { return _entities[inIndex].Key; }
    public V GetValueByIndex(int inIndex) { return _entities[inIndex].Value; }

    public KeyValuePair<K, V> GetKeyValueByIndex(int inIndex) { return _entities[inIndex]; }

    public bool Contains(K inKey) { return _ent_pos.ContainsKey(inKey); }

    public bool TryGetValue(K inKey, out V outValue)
    {
        int pos;
        if (_ent_pos.TryGetValue(inKey, out pos))
        {
            outValue = _entities[pos].Value;
            return true;
        }

        outValue = default(V);
        return false;
    }

    public int GetIndexByKey(K inKey)
    {
        if (_ent_pos.TryGetValue(inKey, out int pos))
            return pos;

        return -1;
    }

    public V GetValueByKey(K inKey)
    {
        int pos;
        if (_ent_pos.TryGetValue(inKey, out pos))
            return _entities[pos].Value;
        return default(V);
    }

    public void SetValueByIndex(int i, V inNewValue)
    {
        var kv = _entities[i];
        _entities[i] = new KeyValuePair<K, V>(kv.Key, inNewValue);
    }

    /// <summary>
    /// Added or Replaced: true - added, false - replaced
    /// </summary>
    /// <returns>true - added, false - replaced</returns>
    public bool SetValueByKey(K inKey, V inNewValue)
    {
        int pos;
        if (_ent_pos.TryGetValue(inKey, out pos))
        {
            _entities[pos] = new KeyValuePair<K, V>(inKey, inNewValue);
            return false;
        }
        Add(inKey, inNewValue);
        return true;
    }

    public void GetValues(ICollection<V> outValues)
    {
        for (int i = 0; i < _entities.Count; ++i)
            outValues.Add(_entities[i].Value);
    }

    public void RemoveAt(int i)
    {
        var kv = _entities[i];
        _ent_pos.Remove(kv.Key);

        if (i == _entities.Count - 1)
        {
            _entities.RemoveAt(i);
            return;
        }

        var last_ent = _entities[_entities.Count - 1];
        _entities[i] = last_ent;

        _ent_pos[last_ent.Key] = i;

        _entities.RemoveAt(_entities.Count - 1);
    }

    public KeyValuePair<K, V> this[int index] { get { return _entities[index]; } }

    public int ToArray(ref KeyValuePair<K, V>[] ioArray, bool inCut = false)
    {
        if (ioArray == null)
            ioArray = new KeyValuePair<K, V>[_entities.Count];
        else if (ioArray.Length < _entities.Count || ioArray.Length > _entities.Count && inCut)
            Array.Resize(ref ioArray, _entities.Count);

        for (int i = 0; i < _entities.Count; i++)
            ioArray[i] = _entities[i];

        return _entities.Count;
    }

    public int ToKeyArray(ref K[] ioArray, bool inCut = false)
    {
        if (ioArray == null)
            ioArray = new K[_entities.Count];
        else if (ioArray.Length < _entities.Count || ioArray.Length > _entities.Count && inCut)
            Array.Resize(ref ioArray, _entities.Count);

        for (int i = 0; i < _entities.Count; i++)
            ioArray[i] = _entities[i].Key;

        return _entities.Count;
    }

    public int ToValueArray(ref V[] ioArray, bool inCut = false)
    {
        if (ioArray == null)
            ioArray = new V[_entities.Count];
        else if (ioArray.Length < _entities.Count || ioArray.Length > _entities.Count && inCut)
            Array.Resize(ref ioArray, _entities.Count);

        for (int i = 0; i < _entities.Count; i++)
            ioArray[i] = _entities[i].Value;

        return _entities.Count;
    }

}

