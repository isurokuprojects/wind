﻿using System;
using System.Collections.Generic;

public class CStructStore<T> where T : struct
{
    private struct SData
    {
        public bool Removed;
        public T Data;

        public override string ToString()
        {
            if (Removed)
                return "Removed";
            return $"{Data}";
        }
    }

    SData[] _array;
    Queue<int> _removed = new Queue<int>();
    int _count;

    public CStructStore(int inInitSize = 10, bool inFill = false)
    {
        _array = new SData[inInitSize];
        if (inFill)
            _count = inInitSize;
    }

    public void Clear(bool inClearData = true)
    {
        for (int i = 0; i < _array.Length; i++)
        {
            ref SData data = ref _array[i];
            if (data.Removed)
                return;

            if (inClearData)
                data.Data = default(T);

            data.Removed = false;
        }
        _removed.Clear();

        _count = 0;
    }

    public int Count { get { return _count; } }
    public int Capacity { get { return _array.Length; } }

    public ref T this[int index]
    {
        get
        {
            ref SData data = ref _array[index];
            return ref data.Data;
        }
    }

    public int Add()
    {
        int index;
        if (_removed.Count > 0)
            index = _removed.Dequeue();
        else
        {
            index = _count;

            _count++;
            if (_count >= _array.Length)
                Array.Resize(ref _array, 2 * _count + 1);
        }

        ref SData data = ref _array[index];
        data.Removed = false;

        return index;
    }

    /// <param name="inStruct">по ref для оптимизации</param>
    public int Add(ref T inStruct)
    {
        int index = Add();

        ref SData data = ref _array[index];

        data.Data = inStruct;

        return index;
    }

    public bool IsRemoved(int inIndex)
    {
        if (inIndex < 0)
        {
#if ASSERTS
			CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
						() => $"inIndex {inIndex} is not valid!");
#endif //ASSERTS
            return false;
        }

        if (inIndex >= _count)
            return true;

        ref SData data = ref _array[inIndex];
        return data.Removed;
    }

    public void Remove(int inIndex, bool inClearData = true)
    {
        if (inIndex < 0 || inIndex >= _count)
        {
#if ASSERTS
			CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
						() => $"inIndex {inIndex} is not valid!");
#endif //ASSERTS
            return;
        }

        ref SData data = ref _array[inIndex];
        if (data.Removed)
            return;

        if (inClearData)
            data.Data = default(T);

        if (inIndex == _count - 1)
        {
            _count--;
        }
        else
        {
            data.Removed = true;
            _removed.Enqueue(inIndex);
        }
    }
}

