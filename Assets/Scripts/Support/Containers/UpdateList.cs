﻿using System;
using System.Collections.Generic;
using System.Text;

public class UpdateEventArgs : EventArgs
{
    private float _time;

    public float DeltaTime { get { return _time; } }

    public UpdateEventArgs() { }

    public UpdateEventArgs(float time) { _time = time; }
    public override string ToString() { return _time.ToString(); }

    public void SetParams(float time) { _time = time; }
}

public class CUpdateList : IDisposable
{
    private UpdateEventArgs _arg = new UpdateEventArgs();
    private CUpdatingList<Action<object, UpdateEventArgs>> _delegates = new CUpdatingList<Action<object, UpdateEventArgs>>();

    private object _sender;

    public CUpdateList(object inSender = null)
    {
        _sender = inSender ?? this;
    }

    public void Dispose()
    {
        _delegates.Clear();
    }

    public void AddDelegate(Action<object, UpdateEventArgs> inDelegate)
    {
        _delegates.Add(inDelegate);
    }

    public void RemoveDelegate(Action<object, UpdateEventArgs> inDelegate)
    {
        _delegates.Remove(inDelegate);
    }

    public virtual void Update(float inTime)
    {
        _arg.SetParams(inTime);
        using (_delegates.StartUpdate())
        {
            for (int i = 0; i < _delegates.Count; i++)
            {
                if (!_delegates.IsDeleted(i))
                    _delegates[i].Invoke(_sender, _arg);
            }
        }
    }
}
