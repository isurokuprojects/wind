﻿using System;
using System.Collections.Generic;
using System.Text;

public interface IReadOnlyList<T> : IEnumerable<T>
{
    int Count { get; }
    T this[int index] { get; }
    bool Contains(T item);
    bool Exists(Predicate<T> match);
}

public interface IAddOnlyList<T>
{
    void Add(T item);
}

public class CAdvancedList<T> : List<T>, IAddOnlyList<T>, IReadOnlyList<T>, IDisposable
{
    private readonly bool _pooled;

    public CAdvancedList(bool inFromPool = false) : base() { _pooled = inFromPool; }
    public CAdvancedList(IEnumerable<T> collection, bool inFromPool = false) : base(collection) { _pooled = inFromPool; }
    public CAdvancedList(int capacity, bool inFromPool = false) : base(capacity) { _pooled = inFromPool; }

    public void Dispose()
    {
        if(_pooled)
            ListPool<T>.Release(this);
    }

    public override string ToString()
    {
        var sb = new StringBuilder();

        sb.AppendFormat("[{0}]", Count);
        if (Count == 0)
            return sb.ToString();

        sb.Append(": ");

        int cnt = 3;
        if (Count < cnt)
            cnt = Count;

        for (int i = 0; i < cnt; ++i)
        {
            if (i == 0)
                sb.AppendFormat("{0}", base[i]);
            else
                sb.AppendFormat("; {0}", base[i]);
        }

        return sb.ToString();
    }

    public int ToArray(ref T[] ioArray, bool inCut = false)
    {
        if (ioArray == null)
            ioArray = new T[Count];
        else if (ioArray.Length < Count || ioArray.Length > Count && inCut)
            Array.Resize(ref ioArray, Count);

        for (int i = 0; i < Count; i++)
            ioArray[i] = this[i];

        return Count;
    }
}
