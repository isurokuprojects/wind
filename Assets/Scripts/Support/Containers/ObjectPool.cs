﻿using System;
using System.Collections.Generic;

public class ObjectPool<T>
{
    class ObjectPoolContainer
    {
        private T item;

        public bool Used { get; private set; }

        public void Consume()
        {
            Used = true;
        }

        public T Item
        {
            get
            {
                return item;
            }
            set
            {
                item = value;
            }
        }

        public void Release()
        {
            Used = false;
        }
    }

    private List<ObjectPoolContainer> _list;
    private Dictionary<T, ObjectPoolContainer> _lookup;
    private Func<T> _factoryFunc;
    private int _lastIndex = 0;

    public ObjectPool(int initialSize, Func<T> factoryFunc)
    {
        this._factoryFunc = factoryFunc;

        _list = new List<ObjectPoolContainer>(initialSize);
        _lookup = new Dictionary<T, ObjectPoolContainer>(initialSize);

        if (initialSize < 1)
            initialSize = 1;

        Warm(initialSize);

        _lastIndex = 0;
    }

    public void Clear()
    {
        _list.Clear();
        _lookup.Clear();
        _factoryFunc = null;
    }

    private void Warm(int capacity)
    {
        for (int i = 0; i < capacity; i++)
            CreateContainer();
    }

    private ObjectPoolContainer CreateContainer()
    {
        var container = new ObjectPoolContainer();
        container.Item = _factoryFunc();
        _lastIndex = _list.Count;
        _list.Add(container);
        return container;
    }

    private readonly object _lock_obj = new object();

    public T GetItem()
    {
        lock (_lock_obj)
        {
            for (int i = 0; i < _list.Count && _list[_lastIndex].Used; i++)
            {
                _lastIndex++;
                if (_lastIndex > _list.Count - 1) _lastIndex = 0;
            }

            ObjectPoolContainer container = _list[_lastIndex];

            if (container.Used)
                container = CreateContainer();

            container.Consume();
            _lookup.Add(container.Item, container);

            if (_lookup.Count == _list.Count)
            {
                int lastIndex = _lastIndex;
                Warm(_list.Count);
                _lastIndex = lastIndex;
            }

            return container.Item;
        }
    }

    public bool ReleaseItem(T item)
    {
        lock (_lock_obj)
        {
            if (_lookup.ContainsKey(item))
            {
                var container = _lookup[item];
                container.Release();
                _lookup.Remove(item);
                return true;
            }
            return false;
        }
    }

    public int Size()
    {
        return _list.Count;
    }
}