﻿
using System;

public readonly struct Span<T>
{
    private readonly T[] _array;
    private readonly int _start;
    private readonly int _length;

    public int Length => _length;

    public Span(T[] array, int start = 0, int length = -1)
    {
        _array = array;
        _start = start;
        _length = length < 0 ? array.Length : length;
    }

    public ref T this[int index]
    {
        get
        {
            return ref _array[_start + index];
        }
    }
}

public readonly struct SStringSpan
{
    private readonly string _array;
    private readonly int _start;
    private readonly int _length;

    public int Length => _length;
    public int Start => _start;
    public string Source => _array;

    public bool IsEmpty => string.IsNullOrEmpty(_array) || _length <= 0;

    public SStringSpan(string array, int start = 0, int length = -1)
    {
        _array = array;
        _start = start;
        _length = length < 0 ? array.Length - start : length;
    }

    public override string ToString()
    {
        return _array.Substring(_start, _length);
    }

    public char this[int index]
    {
        get
        {
            return _array[_start + index];
        }
    }

    public bool StartsWith(string inString, bool inIgnoreCase = false)
    {
        int len = Math.Min(_length, inString.Length);
        for (int i = 0; i < len; i++)
        {
            char c1 = _array[_start + i];
            char c2 = inString[i];

            if(inIgnoreCase)
            {
                c1 = char.ToUpper(c1);
                c2 = char.ToUpper(c2);
            }

            if (!c1.Equals(c2))
                return false;
        }
        return true;
    }

    private static char[] TrimChars = { ' ' };

    private static bool ContainChar(char[] inTrimChars, char inChar, bool inIgnoreCase = false)
    {
        char in_char = inIgnoreCase ? char.ToUpper(inChar) : inChar;

        for (int i = 0; i < inTrimChars.Length; i++)
        {
            char c1 = inIgnoreCase ? char.ToUpper(inTrimChars[i]) : inTrimChars[i];
            if (c1.Equals(in_char))
                return true;
        }
        return false;
    }

    public SStringSpan ForwardTrim(char[] inTrimChars = null, bool inIgnoreCase = false)
    {
        char[] trim_chars = inTrimChars ?? TrimChars;

        for (int i = 0; i < _length; i++)
        {
            char c1 = _array[_start + i];

            if(!ContainChar(trim_chars, c1, inIgnoreCase))
            {
                return new SStringSpan(_array, _start + i, _length - i);
            }
        }
        return new SStringSpan(_array, _start + _length, 0);
    }
}

