﻿using System;

static class UPerfSample
{
    private static CUPerfSample _sample = new CUPerfSample();
    public static IDisposable Start(string name)
    {
#if PERF_CHECK
        UnityEngine.Profiling.Profiler.BeginSample(name);
#endif
        return _sample;
    }

    private class CUPerfSample : IDisposable
    {
        public void Dispose()
        {
#if PERF_CHECK
            UnityEngine.Profiling.Profiler.EndSample();
#endif
        }
    }
}
