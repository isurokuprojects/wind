﻿using System;
using System.Collections.Generic;

public delegate string DObjectToString<T>(T inObj);

public static class ListExt
{
    public static bool Empty<T>(this List<T> list) { return list == null || list.Count == 0; }

    public static bool Any<T>(this List<T> list)
    {
        return !list.Empty();
    }

    public static bool Any<T>(this List<T> list, Predicate<T> match)
    {
        if (list.Empty())
            return false;

        for (int i = 0; i < list.Count; ++i)
        {
            if (match(list[i]))
                return true;
        }
        return false;
    }

    public static bool All<T>(this List<T> list, Predicate<T> match)
    {
        if (list.Empty())
            return true;

        for (int i = 0; i < list.Count; ++i)
        {
            if (!match(list[i]))
                return false;
        }
        return true;
    }

    public static T LastOrDefault<T>(this List<T> list)
    {
        if (list.Empty())
            return default(T);
        return list[list.Count - 1];
    }

    public static T FirstOrDefault<T>(this List<T> list)
    {
        if (list.Empty())
            return default(T);
        return list[0];
    }

    public static T FirstOrDefault<T>(this List<T> list, Predicate<T> match)
    {
        if (list.Empty())
            return default(T);

        for (int i = 0; i < list.Count; ++i)
        {
            if (match(list[i]))
                return list[i];
        }
        return default(T);
    }

    public static string ToString<T>(this List<T> list, string delimeter)
    {
        if (list.Empty())
            return string.Empty;

        string[] sarr = new string[list.Count];
        for (int i = 0; i < list.Count; i++)
            sarr[i] = list[i].ToString();
        return string.Join(delimeter, sarr);
    }

    public static string ToString<T>(this List<T> list, string delimeter, DObjectToString<T> inFunctor)
    {
        if (list.Empty())
            return string.Empty;

        string[] sarr = new string[list.Count];
        for (int i = 0; i < list.Count; i++)
            sarr[i] = inFunctor(list[i]);
        return string.Join(delimeter, sarr);
    }

    public static T Pick<T>(this List<T> list, Predicate<T> predicate)
    {
        var index = list.FindIndex(predicate);

        if (index < 0)
            return default(T);

        T item = list[index];

        list.RemoveAt(index);

        return item;
    }

    /// <summary>
    /// Extracts element from the list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="index"></param>
    /// <returns></returns>
    public static T Pick<T>(this List<T> list, int index = 0)
    {
        if (!IndexCheck(list, index))
            return default(T);

        T item = list[index];

        list.RemoveAt(index);

        return item;
    }

    public static bool IndexCheck<T>(this List<T> list, int index)
    {
        if (list.Empty())
            return false;

        if (index < 0 || index >= list.Count)
            return false;

        return true;
    }


}
