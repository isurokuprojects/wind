﻿using UnityEngine;

public static class QuaternionExt
{
    public static bool IsIdentity(this Quaternion inThis)
    {
        return inThis == Quaternion.identity;
    }

    public static bool IsZero(this Quaternion inThis)
    {
        return GameMath.ApproximatelyZero(inThis.x) &&
               GameMath.ApproximatelyZero(inThis.y) &&
               GameMath.ApproximatelyZero(inThis.z) &&
               GameMath.ApproximatelyZero(inThis.w);
    }

    public static bool IsEqual(this Quaternion inThis, Quaternion inOther, float inEpsilon = GameMath.Epsilon)
    {
        Vector3 t = inThis.eulerAngles;
        Vector3 o = inOther.eulerAngles;
        return t.IsEqual(o, inEpsilon);
    }

    public static int GetHashValue(this Quaternion inThis, int QualityFactor = 1000)
    {
        int h = HashFunctions.HashCombine((int)(inThis.x * QualityFactor), (int)(inThis.y * QualityFactor));
        h = HashFunctions.HashCombine(h, (int)(inThis.z * QualityFactor));
        h = HashFunctions.HashCombine(h, (int)(inThis.w * QualityFactor));
        return h;
    }
}
