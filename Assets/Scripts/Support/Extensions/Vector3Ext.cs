﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Ext
{
    public static Vector3 GetMin(Vector3 inPoint1, Vector3 inPoint2)
    {
        return new Vector3(Mathf.Min(inPoint1.x, inPoint2.x), Mathf.Min(inPoint1.y, inPoint2.y), Mathf.Min(inPoint1.z, inPoint2.z));
    }

    public static Vector3 GetMax(Vector3 inPoint1, Vector3 inPoint2)
    {
        return new Vector3(Mathf.Max(inPoint1.x, inPoint2.x), Mathf.Max(inPoint1.y, inPoint2.y), Mathf.Max(inPoint1.z, inPoint2.z));
    }

    public static float GetXZMaxValue(this Vector3 inThis)
    {
        return Mathf.Max(inThis.x, inThis.z);
    }

    public static float GetMaxValue(this Vector3 inThis)
    {
        return Mathf.Max(inThis.x, Mathf.Max(inThis.y, inThis.z));
    }

    public static float GetMinValue(this Vector3 inThis)
    {
        return Mathf.Min(inThis.x, Mathf.Min(inThis.y, inThis.z));
    }

    public static float GetVolume(this Vector3 inThis)
    {
        return inThis.x * inThis.y * inThis.z;
    }

    public static float GetAverage(this Vector3 inThis)
    {
        return (inThis.x + inThis.y + inThis.z) / 3;
    }

    public static Vector3 GetDimSign0(this Vector3 inThis)
    {
        return new Vector3(GameMath.Sign(inThis.x), GameMath.Sign(inThis.y), GameMath.Sign(inThis.z));
    }

    public static Vector3 GetDimSign(this Vector3 inThis)
    {
        return new Vector3(Mathf.Sign(inThis.x), Mathf.Sign(inThis.y), Mathf.Sign(inThis.z));
    }

    public static (Vector3, float) GetNormalized(this Vector3 inThis, float Epsilon = 1E-06f)
    {
        float d = inThis.magnitude;
        if (GameMath.ApproximatelyZero(d, Epsilon))
            return (Vector3.zero, 0);
        Vector3 v = inThis / d;
        return (v, d);
    }

    public static float DistancePow2To(this Vector3 inThis, Vector3 inPoint)
    {
        return (inPoint.x - inThis.x) * (inPoint.x - inThis.x) + (inPoint.y - inThis.y) * (inPoint.y - inThis.y) + (inPoint.z - inThis.z) * (inPoint.z - inThis.z);
    }

    public static float FlatDistancePow2To(this Vector3 inThis, Vector3 inPoint)
    {
        return (inPoint.x - inThis.x) * (inPoint.x - inThis.x) + (inPoint.z - inThis.z) * (inPoint.z - inThis.z);
    }

    public static bool IsZero(this Vector3 inThis)
    {
        return IsEmpty(inThis);
    }

    public static bool IsEmpty(this Vector3 inThis)
    {
        return GameMath.ApproximatelyZero(inThis.x) &&
               GameMath.ApproximatelyZero(inThis.y) &&
               GameMath.ApproximatelyZero(inThis.z);
    }

    public static bool IsEqual(this Vector3 inThis, Vector3 inOther, float Epsilon = GameMath.Epsilon)
    {
        return GameMath.Approximately(inThis.x, inOther.x, Epsilon) &&
               GameMath.Approximately(inThis.y, inOther.y, Epsilon) &&
               GameMath.Approximately(inThis.z, inOther.z, Epsilon);
    }

    public static bool IsEqual(this Vector2 inThis, Vector2 inOther, float Epsilon = GameMath.Epsilon)
    {
        return GameMath.Approximately(inThis.x, inOther.x, Epsilon) &&
               GameMath.Approximately(inThis.y, inOther.y, Epsilon);
    }

    public static int GetHashValue(this Vector3 inThis, float QualityFactor = 1000)
    {
        int h = HashFunctions.HashCombine((int)(inThis.x * QualityFactor), (int)(inThis.y * QualityFactor));
        h = HashFunctions.HashCombine(h, (int)(inThis.z * QualityFactor));
        return h;
    }

    public static Vector2 ToVector2XZ(this Vector3 inThis)
    {
        return new Vector2(inThis.x, inThis.z);
    }

    public static Vector3 RoughZero(this Vector3 inThis, float inEps = 0.001f)
    {
        return new Vector3(GameMath.ApproximatelyZero(inThis.x, inEps) ? 0 : inThis.x,
                            GameMath.ApproximatelyZero(inThis.y, inEps) ? 0 : inThis.y,
                            GameMath.ApproximatelyZero(inThis.z, inEps) ? 0 : inThis.z);
    }

    public static Vector3 FindNearest(this Vector3 inThis, IEnumerable<Vector3> inPoints)
    {
        float dist = float.MaxValue;
        Vector3 res = Vector3.zero;

        foreach (var p in inPoints)
        {
            float d = inThis.DistancePow2To(p);
            if (d > dist)
                continue;
            dist = d;
            res = p;
        }
        return res;
    }


    class CDistComparer : IComparer<Vector3>
    {
        Vector3 _base_point;

        void Init(Vector3 base_point) { _base_point = base_point; }

        public int Compare(Vector3 x, Vector3 y)
        {
            float dx = x.DistancePow2To(_base_point);
            float dy = y.DistancePow2To(_base_point);
            return dx.CompareTo(dy);
        }

        static CDistComparer _comparer = new CDistComparer();

        public static CDistComparer Get(Vector3 base_point) { _comparer.Init(base_point); return _comparer; }
    }

    public static void DistanceSort(this Vector3 inThis, List<Vector3> inPoints)
    {
        inPoints.Sort(CDistComparer.Get(inThis));
    }

    public static void DistanceSort(this Vector3 inThis, Vector3[] inPoints)
    {
        Array.Sort(inPoints, CDistComparer.Get(inThis));
    }
}
