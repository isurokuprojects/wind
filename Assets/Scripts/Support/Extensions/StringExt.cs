﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using UnityEngine;

public static class StringExt
{
    public sealed class CStringComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            return string.CompareOrdinal(x, y);
        }
    }

    public static CStringComparer StringComparer = new CStringComparer();

    public static string CutMax(this string instr, int maxlength)
    {
        if (string.IsNullOrEmpty(instr))
            return string.Empty;

        if (instr.Length > maxlength)
            return instr.Substring(0, maxlength);
        return instr;
    }

    public static string ClearStringForServer(this string instr)
    {
        if (string.IsNullOrEmpty(instr))
            return string.Empty;

        string s = instr.Replace('>', '.');
        s = s.Replace("<", string.Empty);
        return s;
    }

    public static string CutCloneFromName(this string instr)
    {
        if (string.IsNullOrEmpty(instr))
            return string.Empty;

        string clone_str = "(Clone)";
        if (!instr.EndsWith(clone_str))
            return instr;

        return instr.Substring(0, instr.Length - clone_str.Length).Trim();
    }

    public static bool IsEmpty(this string instr)
    {
        return string.IsNullOrEmpty(instr);
    }

    public static bool IsEquals(this string instr, string other)
    {
        if (string.IsNullOrEmpty(instr) || string.IsNullOrEmpty(other))
            return false;

        return string.Equals(instr, other, System.StringComparison.InvariantCulture);
    }

    public static bool IsSimilar(this string instr, string other)
    {
        if (string.IsNullOrEmpty(instr) || string.IsNullOrEmpty(other))
            return false;
        return string.Equals(instr, other, System.StringComparison.InvariantCultureIgnoreCase);
    }

    public static string CreateIntent(int inIntentCount)
    {
        return new string('\t', inIntentCount);
    }

    public static int GetCheckedHashCode(this string instr)
    {
        if (instr.IsEmpty())
            return 0;
        return instr.GetHashCode();
    }

    public static string ToClassicNameForm(this string inName)
    {
        if (string.IsNullOrEmpty(inName))
            return string.Empty;

        char[] new_name_buff = null;
        for (int i = 0; i < inName.Length; i++)
        {
            if (i == 0)
            {
                if (char.IsLetter(inName[i]) && char.IsLower(inName[i]))
                {
                    if (new_name_buff == null)
                        new_name_buff = inName.ToCharArray();
                    new_name_buff[i] = char.ToUpper(inName[i]);
                }
            }
            else
            {
                if (char.IsLetter(inName[i]) && char.IsUpper(inName[i]))
                {
                    if (new_name_buff == null)
                        new_name_buff = inName.ToCharArray();
                    new_name_buff[i] = char.ToLower(inName[i]);
                }
            }
        }

        string name = inName;
        if (new_name_buff != null)
            name = new string(new_name_buff);

        return name;
    }

    static char[] _div = new char[1];
    static float[] _res_cache = new float[10];

    /// <summary>
    /// Результат нельзя запоминать, он статический, получить, использовать и сразу отпустить
    /// </summary>
    public static (float[], int) ParseToFloatArray(this string inStrValue, char inDiv = ',')
    {
        if (string.IsNullOrEmpty(inStrValue))
            return (null, 0);

        string s1 = inStrValue;

        if (inStrValue[0] == '(' || inStrValue[0] == '[')
            s1 = inStrValue.Substring(1, inStrValue.Length - 2);

        _div[0] = inDiv;

        string[] p = s1.Split(_div);

        for (int i = 0; i < p.Length; i++)
        {
            if (!float.TryParse(p[i].Trim(), NumberStyles.Float, CultureInfo.InvariantCulture, out float x))
                return (null, 0);
            _res_cache[i] = x;
        }

        return (_res_cache, p.Length);
    }

    static int[] Decimals = {0, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };

    public static string ToStringExt(this float inValue, int inCountAfterComma = 6)
    {
        int CountAfterComma = Mathf.Min(inCountAfterComma, Decimals.Length - 1);
        float f = inValue * Decimals[CountAfterComma];
        int i = (int)f;
        float f2 = (float)i / (Decimals[CountAfterComma]);
        return f2.ToString(CultureInfo.InvariantCulture);
    }

    public static string ToStringExt(this double inValue, int inCountAfterComma = 6)
    {
        int CountAfterComma = Mathf.Min(inCountAfterComma, Decimals.Length - 1);
        double f = inValue * Decimals[CountAfterComma];
        int i = (int)f;
        double f2 = (double)i / (Decimals[CountAfterComma]);
        return f2.ToString(CultureInfo.InvariantCulture);
    }

    public static string ToStringExt(this DateTime inValue)
    {
        return inValue.ToString("o");//2021-11-05T14:28:13.7438608Z;
    }

    public static string ToStringExt(this TimeSpan inValue)
    {
        //10.09:08:07.0060000 - 10 days, 9 hours, 8 minutes, 7 seconds, 6 milliseconds
        return inValue.ToString();
    }

    public static float ParseToFloat(this string inStrValue)
    {
        if (float.TryParse(inStrValue, NumberStyles.Float, CultureInfo.InvariantCulture, out float x))
            return x;
        return 0;
    }

    public static string ToStringExt(this Quaternion inThis, int inCountAfterComma = 6)
    {
        return inThis.eulerAngles.ToStringExt(inCountAfterComma);
    }

    public static bool TryParse(string inStrValue, out Quaternion outResult)
    {
        outResult = Quaternion.identity;

        if (string.IsNullOrEmpty(inStrValue))
            return false;

        (float[] arr, int arr_len) = inStrValue.ParseToFloatArray();

        if (arr_len == 3)
        {
            var vec = new Vector3(arr[0], arr[1], arr[2]);
            outResult = Quaternion.Euler(vec);
            return true;
        }

        return false;
    }

    public static string ToStringExt(this Vector3 inThis, int inCountAfterComma = 4)
    {
        return $"{inThis.x.ToStringExt(inCountAfterComma)}, {inThis.y.ToStringExt(inCountAfterComma)}, {inThis.z.ToStringExt(inCountAfterComma)}";
    }

    public static bool TryParse(string inStrValue, out Vector3 outResult)
    {
        outResult = Vector3.zero;

        if (string.IsNullOrEmpty(inStrValue))
            return false;

        (float[] arr, int arr_len) = inStrValue.ParseToFloatArray();

        if (arr_len == 3)
        {
            outResult = new Vector3(arr[0], arr[1], arr[2]);
            return true;
        }

        return false;
    }

    public static string ToStringExt(this Vector2 inThis, int inCountAfterComma = 4)
    {
        return $"{inThis.x.ToStringExt(inCountAfterComma)}, {inThis.y.ToStringExt(inCountAfterComma)}";
    }

    public static bool TryParse(string inStrValue, out Vector2 outResult)
    {
        outResult = Vector3.zero;

        if (string.IsNullOrEmpty(inStrValue))
            return false;

        (float[] arr, int arr_len) = inStrValue.ParseToFloatArray();

        if (arr_len == 2)
        {
            outResult = new Vector2(arr[0], arr[1]);
            return true;
        }

        return false;
    }

    public static string ToStringExt(this Color color)
    {
        string r = color.r.ToStringExt();
        string g = color.g.ToStringExt();
        string b = color.b.ToStringExt();

        if (GameMath.Approximately(color.a, 1))
        {
            return $"{r},{g},{b}";
        }

        string a = color.a.ToStringExt();
        return $"{r},{g},{b},{a}";
    }

    public static bool TryParse(string inStrValue, out Color outResult)
    {
        outResult = Color.black;

        if (string.IsNullOrEmpty(inStrValue))
            return false;

        (float[] arr, int arr_len) = inStrValue.ParseToFloatArray();

        if (arr_len == 3)
        {
            outResult = new Color(arr[0], arr[1], arr[2]);
            return true;
        }

        if (arr_len == 4)
        {
            outResult = new Color(arr[0], arr[1], arr[2], arr[3]);
            return true;
        }

        return false;
    }

    public static string ToStringExt(this bool[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToString());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this long[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToString());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this ulong[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToString());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this double[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToStringExt());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this Vector2[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToStringExt());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this Vector3[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToStringExt());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this Quaternion[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToStringExt());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this NamedId[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToString());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this DateTime[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToStringExt());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this TimeSpan[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToStringExt());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this string[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i]);
        }

        return sb.ToString();
    }

    public static string ToStringExt(this RVector[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToString());
        }

        return sb.ToString();
    }

    public static string ToStringExt(this Color[] array, string delimeter = "; ")
    {
        if (array.Empty())
            return string.Empty;

        var sb = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
                sb.Append(delimeter);
            sb.Append(array[i].ToStringExt());
        }

        return sb.ToString();
    }
}
