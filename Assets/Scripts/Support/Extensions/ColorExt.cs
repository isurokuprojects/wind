﻿using UnityEngine;

public static class ColorExt
{
    public static float GetHsvHue(this Color color)
    {
        Color.RGBToHSV(color, out float h, out float s, out float v);

        return h;
    }

    public static float GetHsvSaturation(this Color color)
    {
        Color.RGBToHSV(color, out float h, out float s, out float v);

        return s;
    }

    public static float GetHsvValue(this Color color)
    {
        Color.RGBToHSV(color, out float h, out float s, out float v);

        return v;
    }
}

public class HSVColor
{
    public float H;
    public float S;
    public float V;

    public HSVColor(Color color) => Color.RGBToHSV(color, out H, out S, out V);
    public HSVColor(float h, float s, float v)
    {
        H = h;
        S = s;
        V = v;
    }

    public void FromRGB(Color color) => Color.RGBToHSV(color, out H, out S, out V);
    public Color ToRGB() => Color.HSVToRGB(H, S, V);
}