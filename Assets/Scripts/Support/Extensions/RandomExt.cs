﻿
using UnityEngine;

static public class RandomExt
{
    /// <summary>
    /// Generates either -1.0f or 1.0f randomly.
    /// </summary>
    /// <returns></returns>
    public static float Sign()
    {
        return Bool(0.5f) ? -1.0f : 1.0f;
    }

    /// <summary>
    /// Generates a random bool, true with the given probability.
    /// </summary>
    /// <param name="probability"></param>
    /// <returns></returns>
    public static bool Bool(float probability)
    {
        return Random.value < probability; //Returns a random number between 0.0 [inclusive] and 1.0 [inclusive] (Read Only).
    }

    public static bool Bool()
    {
        return Bool(0.5f);
    }

    public static float Next()
    {
        return Random.value;
    }

    /// <param name="min">inclusive</param>
    /// <param name="max">exclusive</param>
    ///<returns>Returns a random float number between and min [inclusive] and max [exclusive]</returns>
    public static int Next(int min, int max)
    {
        return Random.Range(min, max);
    }

    /// <param name="min">inclusive</param>
    /// <param name="max">inclusive</param>
    ///<returns>Returns a random float number between and min [inclusive] and max [inclusive]</returns>
    public static float Next(float min, float max)
    {
        return Random.Range(min, max);
    }
}

