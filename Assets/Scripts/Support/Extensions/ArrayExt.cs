﻿using System;
using System.Text;

public static class ArrayExt
{
    public static bool Empty<T>(this T[] array) { return array == null || array.Length == 0; }
    public static bool Empty<T>(this T[,] array) { return array == null || array.Length == 0; }

    public static bool Any<T>(this T[] array) { return !array.Empty(); }
    public static bool Any<T>(this T[,] array) { return !array.Empty(); }

    public static T FindCheck<T>(this T[] array, Predicate<T> match)
    {
        if (array.Empty())
            return default(T);

        return Array.Find(array, match);
    }

    public static bool ContainsCheck<T>(this T[] array, Predicate<T> match)
    {
        if (array.Empty())
            return false;

        for (int i = 0; i < array.Length; i++)
        {
            if (match(array[i]))
                return true;
        }
        return false;
    }

    public static bool ContainsCheck<T>(this T[] array, T value)
    {
        if (array.Empty())
            return false;

        for (int i = 0; i < array.Length; i++)
        {
            ref T el = ref array[i];
            if (el.Equals(value))
                return true;
        }
        return false;
    }

    public static bool ContainsCheck(this NamedId[] array, NamedId value)
    {
        if (array.Empty())
            return false;

        for (int i = 0; i < array.Length; i++)
        {
            ref NamedId el = ref array[i];
            if (el.Equals(value))
                return true;
        }
        return false;
    }

    public static bool IndexCheck<T>(this T[] array, int index)
    {
        if (array.Empty())
            return false;

        if (index < 0 || index >= array.Length)
            return false;

        return true;
    }

    public static void Foreach<T>(this T[] array, Action<T> action)
    {
        if (array.Empty())
            return;

        for (int i = 0; i < array.Length; i++)
            action(array[i]);
    }

    public static T[] CreateLightCopy<T>(this T[] array)
    {
        if (array.Empty())
            return new T[0];

        T[] na = new T[array.Length];
        for (int i = 0; i < array.Length; i++)
        {
            na[i] = array[i];
        }

        return na;
    }

    public static int GetCheckedHashCode<T>(this T[] array)
    {
        if (array.Empty())
            return 0;
        int res = array[0].GetHashCode();
        for (int i = 1; i < array.Length; i++)
            res = HashFunctions.HashCombine(res, array[i].GetHashCode());
        return res;
    }

    public static int GetSafeLength<T>(this T[] array)
    {
        if (array.Empty())
            return 0;
        return array.Length;
    }

    public static string Join(this string[] array, string separator = null)
    {
        if (string.IsNullOrEmpty(separator))
            separator = ", ";

        return string.Join(separator, array);
    }

    public static bool Any<T>(this T[] array, Predicate<T> match)
    {
        if (array.Empty())
            return false;

        for (int i = 0; i < array.Length; ++i)
        {
            if (match(array[i]))
                return true;
        }
        return false;
    }

    public static bool All<T>(this T[] array, Predicate<T> match)
    {
        if (array.Empty())
            return true;

        for (int i = 0; i < array.Length; ++i)
        {
            if (!match(array[i]))
                return false;
        }
        return true;
    }

    public static T PickRandom<T>(this T[] array)
    {
        if (array.Empty())
            return default(T);

        int index = RandomExt.Next(0, array.Length);
        return array[index];
    }

    //public static void DisposeElements<T>(this T[] array) where T: IDisposable
    //{
    //    if (array.Empty())
    //        return;

    //    for (int i = 0; i < array.Length; ++i)
    //        array[i].Dispose();
    //}
}
