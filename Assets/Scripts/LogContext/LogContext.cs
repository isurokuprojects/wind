﻿
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public enum ELogLevel : byte { DEBUG = 0, INFO = 1, IMPORTANT_INFO, WARNING, ERROR, EXCEPTION, NONE }

public static class LogContext
{
    public static ELogLevel CurrentLogLevel = ELogLevel.INFO;

    public static bool Assert(bool inTrueCondition, string inMessage,
        bool inWriteTime = false,
        [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string source_file = null)
    {
        if (!inTrueCondition)
        {
            WriteToLog(ELogLevel.ERROR, inMessage, inWriteTime, lineNumber, caller, source_file);
            return false;
        }
        return true;
    }

    public static bool Assert(bool inTrueCondition, string inMessage, string inNameParam, Variant inParamValue,
        bool inWriteTime = false,
        [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string source_file = null)
    {
        if (!inTrueCondition)
        {
            WriteToLog(ELogLevel.ERROR, inMessage, inNameParam, inParamValue, inWriteTime, lineNumber, caller, source_file);
            return false;
        }
        return true;
    }

    public static void WriteToLog(ELogLevel inLogLevel, string inMessage, string inNameParam, Variant inParamValue,
        bool inWriteTime = false,
        [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string source_file = null)
    {
        if (inLogLevel < CurrentLogLevel)
            return;

        var lo = new SLogObject(inMessage);
        lo.AddValue(inNameParam, inParamValue);
        WriteToLog(inLogLevel, ref lo, inWriteTime, lineNumber, caller, source_file);
    }

    public static void WriteToLog(ELogLevel inLogLevel, ref SLogObject inLog,
        bool inWriteTime = false,
        [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string source_file = null)
    {
        if (inLogLevel < CurrentLogLevel)
            return;

        WriteToLogCore(inLogLevel, inLog.ToString(), inWriteTime, lineNumber, caller, source_file);
    }

    public static void WriteToLog(ELogLevel inLogLevel, string inMessage,
        bool inWriteTime = false,
        [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null, [CallerFilePath] string source_file = null)
    {
        if (inLogLevel < CurrentLogLevel)
            return;

        WriteToLogCore(inLogLevel, inMessage, inWriteTime, lineNumber, caller, source_file);
    }

    private static void WriteToLogCore(ELogLevel inLogLevel, string inMessage, bool inWriteTime, int lineNumber, string caller, string source_file)
    {
        string fn = Path.GetFileName(source_file);
        string loginfo;
        if (inWriteTime)
            loginfo = $"{DateTime.Now.ToString("H:mm:ss:ff")}: {fn}:{caller}:{lineNumber}: {inMessage}";
        else
            loginfo = $"{fn}:{caller}:{lineNumber}: {inMessage}";

        if (inLogLevel == ELogLevel.EXCEPTION || inLogLevel == ELogLevel.ERROR)
            Debug.LogError(loginfo);
        else if (inLogLevel == ELogLevel.WARNING)
            Debug.LogWarning(WriteWithColor(loginfo.ToString(), Color.yellow));
        else if (inLogLevel == ELogLevel.IMPORTANT_INFO)
            Debug.LogWarning(WriteWithColor(loginfo.ToString(), Color.green));
        else if (inLogLevel == ELogLevel.INFO)
            Debug.Log(WriteWithColor(loginfo.ToString(), Color.white));
        else if (inLogLevel == ELogLevel.DEBUG)
            Debug.Log(WriteWithColor(loginfo.ToString(), new Color(198 / 255f, 198 / 255f, 198 / 255f)));
    }

    static string WriteWithColor(string inText, Color inColor)
    {
        Color32 clr32 = (Color32)inColor;
        string col = ColorToHex(clr32);
        StringBuilder sb = new StringBuilder("<color=");
        sb.Append(col);
        sb.Append("> ");
        sb.Append(inText);
        sb.Append(" </color>");
        return sb.ToString();
    }

    static string ColorToHex(Color32 color)
    {
        return string.Format("#{0}{1}{2}{3}", color.r.ToString("X2"), color.g.ToString("X2"), color.b.ToString("X2"), color.a.ToString("X2"));
    }
}

