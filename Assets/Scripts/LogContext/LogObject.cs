﻿using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

public struct SLogObject
{
    private string _message;

    private string _val_name0;
    private Variant _val0;
    private string _val_name1;
    private Variant _val1;
    private string _val_name2;
    private Variant _val2;

    private int _val_named_count;

    public SLogObject(string inMessage)
    {
        _message = inMessage;

        _val_named_count = 0;
        _val_name0 = string.Empty;
        _val0 = Variant.Undefined;
        _val_name1 = string.Empty;
        _val1 = Variant.Undefined;
        _val_name2 = string.Empty;
        _val2 = Variant.Undefined;
    }

    public void AddValue(string inName, float inValue)
    {
        AddValue(inName, new Variant(inValue));
    }

    public void AddValue(string inName, bool inValue)
    {
        AddValue(inName, new Variant(inValue));
    }

    public void AddValue(string inName, int inValue)
    {
        AddValue(inName, new Variant(inValue));
    }

    public void AddValue(string inName, Variant inValue)
    {
        if (_val_named_count + 1 > 3)
            return;

        if (_val_named_count == 0)
        {
            _val_name0 = inName;
            _val0 = inValue;
        }

        if (_val_named_count == 1)
        {
            _val_name1 = inName;
            _val1 = inValue;
        }

        if (_val_named_count == 2)
        {
            _val_name2 = inName;
            _val2 = inValue;
        }

        _val_named_count++;
    }

    public string GetDetails()
    {
        if (_val_named_count == 0)
            return string.Empty;

        var sb = new StringBuilder();

        if (_val_named_count > 0)
        {
            sb.Append(_val_name0);
            sb.Append(": ");
            sb.Append(_val0);
        }

        if (_val_named_count > 1)
        {
            sb.Append("; ");
            sb.Append(_val_name1);
            sb.Append(": ");
            sb.Append(_val1);
        }

        if (_val_named_count > 2)
        {
            sb.Append("; ");
            sb.Append(_val_name2);
            sb.Append(": ");
            sb.Append(_val2);
        }

        return sb.ToString();
    }

    public override string ToString()
    {
        var sb = new StringBuilder(_message);

        if (_val_named_count > 0)
        {
            sb.Append(" [");
            sb.Append(GetDetails());
            sb.Append("]");
        }

        return sb.ToString();
    }
}
