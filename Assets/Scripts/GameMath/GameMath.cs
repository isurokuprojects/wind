﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;

public enum ERoundType : byte { Floor, Normal, Ceil }
public enum EAggregateOperation : byte { Undefined, Min, Average, Max, Sum, Mul }
public enum EDirectionSide : byte { None, Left, Right, Forward, Backward }
public enum EFlexBool : byte { Undefined, True, False }

public static class GameMath
{
    public const float Epsilon = 1E-06f;

    public static float GetValueInScale01(float x, float inMin, float inMax, bool inClamp)
    {
        return GetValueInScale(x, inMin, inMax, 0, 1, inClamp);
    }

    public static float GetValueInScale11(float x, float inMin, float inMax, bool inClamp)
    {
        return GetValueInScale(x, inMin, inMax, -1, 1, inClamp);
    }

    public static float GetValueInScale(float x, float inMin, float inMax, float outMin, float outMax, bool inClamp)
    {
        float range = inMax - inMin;
        if (ApproximatelyZero(range))
            return 0.5f * (outMax - outMin) + outMin;

        float res = (x - inMin) / range;
        res = res * (outMax - outMin) + outMin;

        if (inClamp)
            res = outMin < outMax ? Mathf.Clamp(res, outMin, outMax) : Mathf.Clamp(res, outMax, outMin);

        return res;
    }

    public static float GetAngleValueIn180Scale(float x)
    {
        int a = (int)(x / 360);
        float d = x - a * 360;
        if (d > 180)
            d = d - 360;
        else if (d < -180)
            d = d + 360;
        return d;
    }

    /// <summary>
    /// Natural - only positive exlude zero!
    /// </summary>
    public static uint SignIntToNatural(int inInt)
    {
        var r = (uint)(inInt >= 0 ? 2 * inInt : -2 * inInt - 1);
        r++;
        return r;
    }

    /// <summary>
    /// Natural - only positive exlude zero!
    /// </summary>
    public static ulong TwoNaturalToOneDim(uint N1, uint N2)
    {
        ulong s = N1 + N2;
        ulong p1 = s - 1;
        ulong p2 = s - 2;
        return p1 * p2 / 2 + N1;
    }

    public static float GetAngleDistance(float TargetAngle, float CurrentAngle)
    {
        float sign_target = Mathf.Sign(TargetAngle);
        float sign_curr = Mathf.Sign(CurrentAngle);
        if (sign_target == sign_curr)
            return TargetAngle - CurrentAngle;

        float d1 = TargetAngle - CurrentAngle;
        float ad1 = sign_target * d1;

        float TargetO = sign_target * 180 - TargetAngle;
        float CurrentO = sign_curr * 180 - CurrentAngle;

        float d2 = TargetO - CurrentO;
        float ad2 = sign_target * d2;
        d2 = -1 * d2;

        if (ad1 < ad2)
            return d1;
        return d2;
    }

    public static bool Approximately(float Value1, float Value2, float Epsilon = Epsilon)
    {
        float d = Value1 - Value2;
        return Math.Abs(d) <= Epsilon;
    }

    public static bool Approximately(double Value1, double Value2, double Epsilon = Epsilon)
    {
        double d = Value1 - Value2;
        return Math.Abs(d) <= Epsilon;
    }

    public static bool ApproximatelyZero(float Value1, float Epsilon = Epsilon)
    {
        return Math.Abs(Value1) <= Epsilon;
    }

    public static int Sign(float value, float Epsilon = Epsilon)
    {
        if (ApproximatelyZero(value, Epsilon))
            return 0;
        if (value > 0)
            return 1;
        return -1;
    }

    /// <summary>
    /// вариант свычеслением угла уже с поворотом
    /// </summary>
    /// <param name="quaternionA"></param>
    /// <param name="quaternionB"></param>
    /// <param name="axis">по какой оси будем вращать</param>
    /// <returns></returns>
    public static float GetSignedAngle(Quaternion quaternionA, Quaternion quaternionB, Vector3 axis)
    {
        var angle = 0f;
        Vector3 angle_axis;
        (quaternionB * Quaternion.Inverse(quaternionA)).ToAngleAxis(out angle, out angle_axis);

        if (Vector3.Angle(axis, angle_axis) > 90f)
            angle = -angle;

        return Mathf.DeltaAngle(0f, angle);
    }

    /// <param name="targetDir">must be normilized</param>
    /// <param name="facing">must be normilized</param>
    /// <param name="localAxis"></param>
    public static float GetSignedAngle(Vector3 targetDir, Vector3 facing, Vector3 localAxis)
    {
        Vector3 perp = Vector3.Cross(facing, targetDir);
        float target_angle = Mathf.Asin(Mathf.Clamp01(perp.magnitude)) * Mathf.Rad2Deg;

        float coll = Vector3.Dot(facing, targetDir);
        if (coll < 0)
            target_angle = 180 - target_angle;

        float ccw = Vector3.Dot(perp, localAxis);
        if (ccw < 0.0f)
            target_angle = target_angle * -1;

        return target_angle;
    }

    public static EDirectionSide GetDirectionSide(Vector3 targetDir, Vector3 facing, Vector3 localAxis, float Epsilon = Epsilon)
    {
        float angle = GetSignedAngle(targetDir, facing, localAxis);
        return GetDirectionSide(angle, Epsilon);
    }

    public static EDirectionSide GetDirectionSide(float angle, float Epsilon = Epsilon)
    {
        if (-Epsilon < angle && angle < Epsilon)
            return EDirectionSide.Forward;

        float oppozite_angle = GetAngleValueIn180Scale(angle + 180);
        if (-Epsilon < oppozite_angle && oppozite_angle < Epsilon)
            return EDirectionSide.Backward;

        return angle > 0 ? EDirectionSide.Right : EDirectionSide.Left;
    }

    public static int GetNearestRotation(float angel)
    {
        return angel <= 180 ? -1 : 1;
    }

    public static Quaternion LerpLookAt(Vector3 pos, Vector3 target, Vector3 upwards)
    {
        return Quaternion.LookRotation(target - pos, upwards);
    }

    public static Quaternion LerpLookAt(Vector3 pos, Vector3 target)
    {
        return Quaternion.LookRotation(target - pos);
    }

    public static Vector3 LocalToWorldPosition(Vector3 inLocalPos, Vector3 inParentPos, Quaternion inParentRotate)
    {
        return inParentRotate * inLocalPos + inParentPos;
    }

    public static Vector3 WorldToLocalPosition(Vector3 inWorldPos, Vector3 inParentPos, Quaternion inParentRotate)
    {
        return Quaternion.Inverse(inParentRotate) * (inWorldPos - inParentPos);
    }

    public static Vector3 WorldToLocalDirection(Vector3 inWorldDir, Quaternion inParentRotate)
    {
        return Quaternion.Inverse(inParentRotate) * inWorldDir;
    }

    //при умножении одного кватерниона на другой получается первое вращение, повёрнутое на второе.
    public static Quaternion RotationToLocalSpace(Quaternion parent_rotation, Quaternion rotation)
    {
        return Quaternion.Inverse(parent_rotation) * rotation; //так правильно
    }

    /// <summary>
    /// Gets the Quaternion from rotation "from" to rotation "to".
    /// </summary>
    public static Quaternion FromToRotation(Quaternion from, Quaternion to)
    {
        if (to == from) return Quaternion.identity;

        return to * Quaternion.Inverse(from);
    }

    public static int Round(float Value, ERoundType inRoundType = ERoundType.Normal)
    {
        if (inRoundType == ERoundType.Ceil)
        {
            int r = (int)Value;
            if (Value > r)
                return r + 1;
            return r;
        }

        if (inRoundType == ERoundType.Normal)
        {
            return (int)(Value + 0.5f);
        }

        return (int)Value;
    }

    //http://stackoverflow.com/questions/919612/mapping-two-integers-to-one-in-a-unique-and-deterministic-way
    public static long PerfectlyHashThem(int a, int b)
    {
        var A = (ulong)(a >= 0 ? 2 * (long)a : -2 * (long)a - 1);
        var B = (ulong)(b >= 0 ? 2 * (long)b : -2 * (long)b - 1);
        var C = (long)((A >= B ? A * A + A + B : A + B * B) / 2);
        return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;
    }

    public static int PerfectlyHashThem(short a, short b)
    {
        var A = (uint)(a >= 0 ? 2 * a : -2 * a - 1);
        var B = (uint)(b >= 0 ? 2 * b : -2 * b - 1);
        var C = (int)((A >= B ? A * A + A + B : A + B * B) / 2);
        return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;
    }


    ///Объём сегмента сферы с высотой h через радиус сферы R:
    public static float GetSphereSegmentVolume(float R, float h)
    {
        return (3 * R - h) * h * h * Mathf.PI / 3;
    }

    ///Объём сферы через радиус R:
    public static float GetSphereVolume(float R)
    {
        return 4 * R * R * R * Mathf.PI / 3;
    }

    ///Площадь поверхности сферы через радиус R:
    public static float GetSphereSquare(float R)
    {
        return 4 * R * R * Mathf.PI;
    }

    ///Площадь поверхности сегмента сферы с высотой h через радиус R:
    public static float GetSphereSegmentSquare(float R, float h)
    {
        return 2 * R * h * Mathf.PI;
    }

    public static R GetRandomObj<R>(Tuple<R, float>[] table)
    {
        if (table == null || table.Length == 0)
            return default(R);

        float total = 0;

        for (int i = 0; i < table.Length; i++)
            total += table[i].Item2;

        float random = UnityEngine.Random.value;

        float sum = 0;

        for (int i = 0; i < table.Length; i++)
        {
            sum += table[i].Item2 / total;

            if (sum >= random)
                return table[i].Item1;
        }

        return table[table.Length - 1].Item1;
    }

    public static ulong TwoFloatToULong(float a, float b)
    {
        ulong res = 0;
        unsafe
        {
            uint t = *(uint*)(&a);
            res = t;
            res = res << 32;
            t = *(uint*)(&b);
            res = res | (ulong)t;
        }
        return res;
    }

    public static void ULongToFloat(ulong value, out float a, out float b)
    {
        unsafe
        {
            uint t = (uint)value;
            b = *(float*)(&t);
            t = (uint)(value >> 32);
            a = *(float*)(&t);
        }
    }

    public static float CheckNaN(float inValue, string inValueName, float inDefaultValue = default(float), [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNumber = 0)
    {
        if (float.IsNaN(inValue))
        {
#if ASSERTS
            var loginfo = $"NaN - {inValueName} - IsNaN!!! File {filePath}; Line {lineNumber}";
            CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "GameMath", () => MethodInfo.GetCurrentMethod().Name, () => loginfo);
#endif
            return inDefaultValue;
        }
        return inValue;
    }
}

