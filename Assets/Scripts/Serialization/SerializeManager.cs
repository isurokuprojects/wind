﻿
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace Serialization
{
    public static class CSerializeManager
    {
        static private ObjectPool<CSerializeStorage> _storage_pool;

        static Version _ver = new Version(0, 1);

        static CSerializeManager()
        {
            _storage_pool = new ObjectPool<CSerializeStorage>(2, () => new CSerializeStorage());
        }

        public static CSerializeStorage GetSerializeStorage()
        {
            return _storage_pool.GetItem();
        }

        public static void ReleaseStorage(CSerializeStorage inStorage)
        {
            inStorage.Clear();
            _storage_pool.ReleaseItem(inStorage);
        }

        public static void SerializeStorage(CSerializeStorage inStorage, EFilePathes inSaveCategory, string inFileName, bool inTxtFormat)
        {
            if (inTxtFormat)
            {
                using (UPerfSample.Start("SerializeStorageInTxt"))
                {
                    string file_name = inFileName;
                    string ext = Path.GetExtension(file_name);
                    if (string.IsNullOrEmpty(ext))
                        file_name += ".txt";

                    var sb = new StringBuilder();
                    bool res = false;
                    using (UPerfSample.Start("StorageSaveToInTxt"))
                    {
                        res = inStorage.SaveTo(sb, _ver);
                    }

                    if (res)
                    {
                        using (UPerfSample.Start("StorageWriteTextToFile"))
                        {
                            FileIOHelper.WriteTextToFile(inSaveCategory, file_name, sb.ToString());
                        }
                    }
                }
            }
            //Test
            //ReleaseStorage(inStorage);

            //DeserializeStorage(inFileName);
        }

        public static bool IsFileExist(EFilePathes inSaveCategory, string inFileName, EFlexBool inTextFormat)
        {
            string file_name = inFileName;

            bool txt_format = true;
            if (txt_format)
            {
                string ext = Path.GetExtension(file_name);
                if (string.IsNullOrEmpty(ext))
                    file_name += ".txt";
            }

            return FileIOHelper.IsFileExist(inSaveCategory, file_name);
        }

        static string[] LineDivs = new string[] { Environment.NewLine };
        public static CSerializeStorage DeserializeStorage(EFilePathes inSaveCategory, string inFileName, EFlexBool inTextFormat)
        {
            CSerializeStorage storage = _storage_pool.GetItem();

            bool txt_format = true;

            if (txt_format)
            {
                string file_name = inFileName;
                string ext = Path.GetExtension(file_name);
                if (string.IsNullOrEmpty(ext))
                    file_name += ".txt";

                string text = FileIOHelper.LoadTextFromFile(inSaveCategory, file_name, true);

                if (!string.IsNullOrEmpty(text))
                {
                    string[] lines = text.Split(LineDivs, StringSplitOptions.RemoveEmptyEntries);
                    var sb = new StringBuilder();
                    if (!storage.LoadFrom(lines, sb, _ver))
                    {
                        storage.Clear();

                        LogContext.WriteToLog(ELogLevel.ERROR, sb.ToString());
                    }
                }
            }
            return storage;
        }
    }
}
