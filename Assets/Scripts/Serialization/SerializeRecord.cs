﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Serialization
{
    public struct SSerializeRecord
    {
        CDicionaryList<SSerializeKey, int> _rec_indexes;
        CDicionaryList<SSerializeKey, int> _values_indexes;

        public bool RecursiveEquals(CSerializeStorage2 inOwnStorage, CSerializeStorage2 inOtherStorage, in SSerializeRecord inOther)
        {
            if ((_values_indexes == null) != (inOther._values_indexes == null))
            {
                return false;
            }

            if (_values_indexes != null)
            {
                for (int i = 0; i < _values_indexes.Count; i++)
                {
                    SSerializeKey here_key = _values_indexes.GetKeyByIndex(i);

                    if (!inOther._values_indexes.Contains(here_key))
                    {
                        return false;
                    }

                    int other_value_index = inOther._values_indexes.GetValueByKey(here_key);
                    ref Variant other_value = ref inOtherStorage.GetValue(other_value_index);

                    int here_value_index = _values_indexes.GetValueByIndex(i);
                    ref Variant here_value = ref inOtherStorage.GetValue(here_value_index);

                    if (!here_value.Equals(other_value))
                        return false;
                }
            }

            if ((_rec_indexes == null) != (inOther._rec_indexes == null))
                return false;

            return true;
        }

        public void Clear()
        {
            if (_rec_indexes != null)
                _rec_indexes.Clear();
            if (_values_indexes != null)
                _values_indexes.Clear();
        }

        public void AddChildRecordIndex(SSerializeKey inKey, int inChildIndexInStorage)
        {
            if (_rec_indexes == null)
                _rec_indexes = new CDicionaryList<SSerializeKey, int>(SSerializeKey.Comparer);

            _rec_indexes.Add(inKey, inChildIndexInStorage);
        }

        public int GetChildRecordIndex(in SSerializeKey inKey)
        {
            if (_rec_indexes == null)
                return -1;

            if (_rec_indexes.TryGetValue(inKey, out int index))
                return index;
            return -1;
        }

        public void AddValueIndex(SSerializeKey inKey, int inValueIndex)
        {
            if (_values_indexes == null)
                _values_indexes = new CDicionaryList<SSerializeKey, int>(SSerializeKey.Comparer);

            _values_indexes.Add(inKey, inValueIndex);
        }

        public int GetValueIndex(SSerializeKey inKey)
        {
            if (_values_indexes == null)
                return -1;

            if (_values_indexes.TryGetValue(inKey, out int index))
                return index;
            return -1;
        }

        public static char Divider = '#';

        public void Save(CSerializeStorage2 inStorage, StringBuilder inSB, int inDeep)
        {
            string intend = new string('\t', inDeep);
            if (_values_indexes != null)
            {
                for (int i = 0; i < _values_indexes.Count; i++)
                {
                    int value_index = _values_indexes.GetValueByIndex(i);
                    ref Variant value = ref inStorage.GetValue(value_index);

                    SSerializeKey key = _values_indexes.GetKeyByIndex(i);

                    inSB.AppendLine($"{intend}{key}{Divider}{value.SaveToString()}");
                }
            }

            if (_rec_indexes != null)
            {
                for (int i = 0; i < _rec_indexes.Count; i++)
                {
                    SSerializeKey key = _rec_indexes.GetKeyByIndex(i);
                    inSB.AppendLine($"{intend}{key}{Divider}");

                    int child_rec_index = _rec_indexes.GetValueByIndex(i);
                    ref SSerializeRecord child_rec = ref inStorage.GetChildRecord(child_rec_index);

                    child_rec.Save(inStorage, inSB, inDeep + 1);
                }
            }
        }
    }
}