﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System;
using System.Reflection;

namespace Serialization
{
    public class CSerializedDictionary
    {
        //private CDicionaryList<SSerializeKey, CAdvancedList<int>> _dic_values;
        private CDicionaryList<SSerializeKey, CSerializedDictionary> _dic_recs;

        private CSerializeStorage _storage;
        public CSerializeStorage Storage { get { return _storage; } }

        private uint _id;
        private string _own_name;

        private CSerializedDictionary _parent;

        int _load_string_index;

        int _start_data_index = -1;
        int _last_data_index = -1;

        bool _data_present;
        public bool IsDataPresent { get { return _data_present; } }

        public static CSerializedDictionary Empty = new CSerializedDictionary();

        public bool Inited { get { return _storage != null; } }

        public CSerializedDictionary() { }

        public void Init(CSerializeStorage inStorage, CSerializedDictionary inParent, string inOwnName, uint inId)
        {
            _storage = inStorage;
            _parent = inParent;
            _own_name = inOwnName;
            _id = inId;
            _start_data_index = -1;
            _last_data_index = -1;
        }

        public void Release()
        {
            using (UPerfSample.Start("CSerializedDictionary.Release"))
            {
                if (_dic_recs != null)
                {
                    for (int i = 0; i < _dic_recs.Count; i++)
                    {
                        _storage.ReleaseRecord(_dic_recs.GetValueByIndex(i));
                    }
                    _dic_recs.Clear();
                }

                //if (_dic_values != null)
                //{
                //    for (int i = 0; i < _dic_values.Count; i++)
                //    {
                //        _storage.ReleaseIntList(_dic_values.GetValueByIndex(i));
                //    }
                //    _dic_values.Clear();
                //}

                _start_data_index = -1;
                _last_data_index = -1;

                _storage = null;
                _parent = null;
                _own_name = string.Empty;
            }
        }


        //void LoadValuesFromOther(CSerializedDictionary inOther)
        //{
        //    for (int i = 0; i < inOther.GetValueCount(); i++)
        //    {
        //        SSerializeKey other_value_name = inOther.GetValueKey(i);
        //        IReadOnlyList<int> other_values = inOther.GetValues(other_value_name);
        //        if (other_values != null)
        //        {
        //            for (int j = 0; j < other_values.Count; j++)
        //            {
        //                int other_value_index = other_values[j];
        //                ref Variant other_value = ref inOther.GetValue(other_value_index);
        //                AddValue(other_value_name, ref other_value);
        //            }
        //        }
        //    }
        //}

        void LoadValuesFromOther2(CSerializedDictionary inOther)
        {
            for (int i = inOther._start_data_index; i <= inOther._last_data_index; i++)
            {
                string name;
                ref Variant value = ref inOther._storage.GetValue(i, out name);

                _storage.AddValue(name, ref value, ref _last_data_index);
                if (_start_data_index == -1)
                    _start_data_index = _last_data_index;
            }
        }

        public void LoadFromOther(CSerializedDictionary inOther)
        {
            using (UPerfSample.Start("CSerializedDictionary.GetRecord"))
            {
                if (!Inited)
                {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "SSerializedDictionary", () => MethodBase.GetCurrentMethod().Name,
                            () => "Not Inited!");
#endif
                    return;
                }

                //LoadValuesFromOther(inOther);
                LoadValuesFromOther2(inOther);

                for (int i = 0; i < inOther.GetRecordCount(); i++)
                {
                    SSerializeKey other_rec_name = inOther.GetRecordNameInternal(i);

                    CSerializedDictionary here_rec = GetRecord(other_rec_name, true);
                    CSerializedDictionary other_rec = inOther.GetRecord(other_rec_name, false);
                    here_rec.LoadFromOther(other_rec);
                }
            }
        }

        #region Records
        public int GetRecordCount()
        {
            if (_dic_recs == null)
                return 0;
            return _dic_recs.Count;
        }

        public string GetRecordName(int inIndex)
        {
            return GetRecordNameInternal(inIndex).Name;
        }


        private SSerializeKey GetRecordNameInternal(int inIndex)
        {
            if (_dic_recs == null)
                return SSerializeKey.Empty;

            return _dic_recs.GetKeyByIndex(inIndex);
        }

        public CSerializedDictionary GetRecord(string inName, bool inCreate)
        {
            return GetRecord(new SSerializeKey(inName), inCreate);
        }

        public CSerializedDictionary GetRecord(NamedId inName, bool inCreate)
        {
            return GetRecord(new SSerializeKey(inName), inCreate);
        }

        private CSerializedDictionary GetRecord(SSerializeKey inName, bool inCreate)
        {
            using (UPerfSample.Start("CSerializedDictionary.GetRecord"))
            {
                if (!Inited)
                {
#if ASSERTS
                if (inCreate)
                {
                    CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "SSerializedDictionary", () => MethodBase.GetCurrentMethod().Name,
                                () => "Not Inited!");
                }
#endif
                    return Empty;
                }

                if (_dic_recs == null)
                {
                    if (inCreate)
                        _dic_recs = new CDicionaryList<SSerializeKey, CSerializedDictionary>(SSerializeKey.Comparer);
                    else
                        return Empty;
                }

                CSerializedDictionary child = _dic_recs.GetValueByKey(inName);
                if (child == null)
                {
                    if (inCreate)
                    {
                        child = _storage.CreateRecord(this, inName.ToString());
                        _dic_recs.Add(inName, child);
                    }
                    else
                        child = Empty;
                }
                return child;
            }
        }
        #endregion Records

        #region MultiRecords

        const string MULTIREC_COUNT = "MULTIREC_COUNT_";

        public CAdvancedList<CSerializedDictionary> CreateMultiRecords(string inName, int inCount)
        {
            using (UPerfSample.Start("CSerializedDictionary.CreateMultiRecords"))
            {
                CAdvancedList<CSerializedDictionary> lst = ListPool<CSerializedDictionary>.Get();

                if (!Inited)
                {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "SSerializedDictionary", () => MethodBase.GetCurrentMethod().Name,
                            () => "Not Inited!");
#endif
                    return lst;
                }

                if (inCount <= 0)
                    return lst;

                if (_dic_recs == null)
                    _dic_recs = new CDicionaryList<SSerializeKey, CSerializedDictionary>(SSerializeKey.Comparer);

                for (int i = 0; i < inCount; i++)
                {
                    var key = new SSerializeKey(inName, i);
                    CSerializedDictionary rec = GetRecord(key, true);
                    lst.Add(rec);
                }

                SetValue($"{MULTIREC_COUNT}{inName}", inCount);

                //CreateMultiRecords(inName, inCount, lst);
                return lst;
            }
        }

        public CAdvancedList<CSerializedDictionary> GetPoolMultiRecords(string inName)
        {
            using (UPerfSample.Start("CSerializedDictionary.GetPoolMultiRecords"))
            {
                CAdvancedList<CSerializedDictionary> lst = ListPool<CSerializedDictionary>.Get();

                if (!Inited || _dic_recs == null)
                    return lst;

                int rec_count = GetValue($"{MULTIREC_COUNT}{inName}", 0);
                if (rec_count == 0)
                {
                    for (int i = 0; i < _dic_recs.Count; i++)
                    {
                        SSerializeKey key = _dic_recs.GetKeyByIndex(i);
                        if (string.Compare(key.Name, inName, StringComparison.Ordinal) == 0)
                        {
                            rec_count++;
                            if (key.Index >= rec_count)
                                rec_count = key.Index + 1;
                        }
                    }
                }

                for (int i = 0; i < rec_count; i++)
                {
                    var key = new SSerializeKey(inName, i);
                    CSerializedDictionary rec = GetRecord(key, false);
                    lst.Add(rec);
                }

                //GetMultiRecords(inName, lst);
                return lst;
            }
        }

        public CSerializedDictionary GetRecordFromMultiRecords(string inName, int inIndex)
        {
            if (!Inited || _dic_recs == null)
                return Empty;

            var key = new SSerializeKey(inName, inIndex);
            CSerializedDictionary rec = GetRecord(key, false);
            return rec;
        }


        #endregion MultiRecords

        #region Values
        private void SetDataPresent()
        {
            if (_data_present)
                return;

            _data_present = true;
            if (_parent != null)
                _parent.SetDataPresent();
        }

        public bool IsValuePresent()
        {
            return _start_data_index >= 0;
        }

        public void GetAllValues(Action<string, Variant> inAction)
        {
            if (_start_data_index < 0 || _storage == null)
                return;

            _storage.GetAllValues(_start_data_index, inAction);
        }

        //public int GetValueCount()
        //{
        //    if (_dic_values == null)
        //        return 0;
        //    return _dic_values.Count;
        //}

        //public string GetValueName(int inIndex)
        //{
        //    return GetValueKey(inIndex).Name;
        //}

        //private SSerializeKey GetValueKey(int inIndex)
        //{
        //    if (_dic_values == null)
        //        return SSerializeKey.Empty;

        //    return _dic_values.GetKeyByIndex(inIndex);
        //}

        //private IReadOnlyList<int> GetValues(string inName)
        //{
        //    return GetValues(new SSerializeKey(inName));
        //}

        //private IReadOnlyList<int> GetValues(SSerializeKey inName)
        //{
        //    if (_dic_values == null)
        //        return null;

        //    IReadOnlyList<int> values = _dic_values.GetValueByKey(inName);
        //    return values;
        //}

        //public bool RemoveValue(string inName)
        //{
        //    if (_dic_values == null)
        //        return false;

        //    return _dic_values.Remove(new SSerializeKey(inName));
        //}

        //        private CAdvancedList<int> GetValueList(string inName)
        //        {
        //            return GetValueList(new SSerializeKey(inName));
        //        }

        //        private CAdvancedList<int> GetValueList(SSerializeKey inName)
        //        {
        //            using (UPerfSample.Start("CSerializedDictionary.GetValueList"))
        //            {
        //                if (!Inited)
        //                {
        //#if ASSERTS
        //                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "SSerializedDictionary", () => MethodBase.GetCurrentMethod().Name,
        //                            () => "Not Inited!");
        //#endif
        //                    return null;
        //                }

        //                if (_dic_values == null)
        //                    _dic_values = new CDicionaryList<SSerializeKey, CAdvancedList<int>>(SSerializeKey.Comparer);

        //                CAdvancedList<int> values = _dic_values.GetValueByKey(inName);
        //                if (values == null)
        //                {
        //                    values = _storage.GetNewIntList();
        //                    _dic_values.Add(inName, values);
        //                }
        //                return values;
        //            }
        //        }

        //        private void AddValue(CAdvancedList<int> inValueList, ref Variant inValue)
        //        {
        //            using (UPerfSample.Start("CSerializedDictionary.AddValue"))
        //            {
        //                if (!Inited)
        //                {
        //#if ASSERTS
        //                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "SSerializedDictionary", () => MethodBase.GetCurrentMethod().Name,
        //                            () => "Not Inited!");
        //#endif
        //                    return;
        //                }

        //                int value_index = _storage.GetNewValueIndex();
        //                inValueList.Add(value_index);

        //                ref Variant value = ref _storage.GetValue(value_index);
        //                value = inValue;

        //                SetDataPresent();
        //            }
        //        }

        public void AddValue(string inName, ref Variant inValue)
        {
            //AddValue(new SSerializeKey(inName), ref inValue);

            _storage.AddValue(inName, ref inValue, ref _last_data_index);
            if (_start_data_index == -1)
            {
                _start_data_index = _last_data_index;
                SetDataPresent();
            }
        }

        //        private void AddValue(SSerializeKey inName, ref Variant inValue)
        //        {
        //            if (!Inited)
        //            {
        //#if ASSERTS
        //                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "SSerializedDictionary", () => MethodBase.GetCurrentMethod().Name,
        //                            () => "Not Inited!");
        //#endif
        //                return;
        //            }

        //            CAdvancedList<int> values = GetValueList(inName);
        //            AddValue(values, ref inValue);
        //        }

        //private ref Variant GetValue(int inValueIndex)
        //{
        //    if (!Inited)
        //        return ref Variant.Undefined;

        //    return ref _storage.GetValue(inValueIndex);
        //}

        public ref Variant GetValue(string inName)
        {
            if (!Inited || _start_data_index == -1)
                return ref Variant.Undefined;

            //IReadOnlyList<int> lst = GetValues(inName);
            //if (lst == null || lst.Count == 0)
            //    return ref Variant.Undefined;

            //ref Variant value1 = ref GetValue(lst[0]);

            ref Variant value2 = ref _storage.GetFirstValue(inName, _start_data_index);

            //#if ASSERTS
            //            if (value2 != value1)
            //            {
            //                string s1 = value1.ToString();
            //                string s2 = value2.ToString();
            //                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => "SSerializedDictionary", () => MethodBase.GetCurrentMethod().Name,
            //                                () => $"value1 {s1} != value2 {s2}");
            //            }
            //#endif

            return ref value2;
        }

        #region string
        public void SetValue(string inName, string inValue)
        {
            SetValue(inName, inValue, string.Empty);
        }

        public void SetValue(string inName, string inValue, string inDefault)
        {
            if (inValue == null || string.CompareOrdinal(inValue, inDefault) == 0)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public string GetValue(string inName, string defaultvalue)
        {
            return GetValue(inName).ToStringValue(defaultvalue);
        }
        #endregion string

        #region NamedId
        public void SetValue(string inName, NamedId inValue)
        {
            SetValue(inName, inValue, NamedId.Empty);
        }

        public void SetValue(string inName, NamedId inValue, NamedId inDefault)
        {
            if (inValue == inDefault)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public NamedId GetValue(string inName, NamedId defaultvalue)
        {
            return GetValue(inName).ToNamedId(defaultvalue);
        }
        #endregion NamedId

        #region int
        public void SetValue(string inName, int inValue, int inDefault = 0)
        {
            if (inValue == inDefault)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public int GetValue(string inName, int defaultvalue)
        {
            return GetValue(inName).ToInt(defaultvalue);
        }
        #endregion int

        #region uint
        public void SetValue(string inName, uint inValue, uint inDefault = 0)
        {
            if (inValue == inDefault)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public uint GetValue(string inName, uint defaultvalue)
        {
            return GetValue(inName).ToUInt(defaultvalue);
        }
        #endregion uint

        #region byte
        public void SetValue(string inName, byte inValue, byte inDefault = 0)
        {
            if (inValue == inDefault)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public byte GetValue(string inName, byte defaultvalue)
        {
            return (byte)GetValue(inName).ToInt(defaultvalue);
        }
        #endregion byte

        #region long
        public void SetValue(string inName, long inValue, long inDefault = 0)
        {
            if (inValue == inDefault)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public long GetValue(string inName, long defaultvalue)
        {
            return GetValue(inName).ToLong(defaultvalue);
        }
        #endregion long

        #region ulong
        public void SetValue(string inName, ulong inValue, ulong inDefault = 0)
        {
            if (inValue == inDefault)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public ulong GetValue(string inName, ulong defaultvalue)
        {
            return GetValue(inName).ToULong(defaultvalue);
        }
        #endregion ulong

        #region bool
        public void SetValue(string inName, bool inValue, bool inDefault = false)
        {
            if (inValue == inDefault)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public bool GetValue(string inName, bool defaultvalue)
        {
            return GetValue(inName).ToBool(defaultvalue);
        }
        #endregion bool

        #region float
        public void SetValue(string inName, float inValue, float inDefault = 0f)
        {
            if (GameMath.Approximately(inValue, inDefault))
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public float GetValue(string inName, float defaultvalue)
        {
            return GetValue(inName).ToFloat(defaultvalue);
        }
        #endregion float

        #region double
        public void SetValue(string inName, double inValue, double inDefault = 0)
        {
            if (GameMath.Approximately(inValue, inDefault))
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public double GetValue(string inName, double defaultvalue)
        {
            return GetValue(inName).ToDouble(defaultvalue);
        }
        #endregion double

        #region Vector3

        public void SetValue(string inName, Vector3 inValue)
        {
            SetValue(inName, inValue, Vector3.zero);
        }
        public void SetValue(string inName, Vector3 inValue, Vector3 inDefault)
        {
            if (inValue.IsEqual(inDefault))
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public Vector3 GetValue(string inName, Vector3 defaultvalue)
        {
            return GetValue(inName).ToVector3(defaultvalue);
        }
        #endregion Vector3

        #region Vector2
        public void SetValue(string inName, Vector2 inValue)
        {
            SetValue(inName, inValue, Vector3.zero);
        }
        public void SetValue(string inName, Vector2 inValue, Vector3 inDefault)
        {
            if (inValue.IsEqual(inDefault))
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public Vector2 GetValue(string inName, Vector2 defaultvalue)
        {
            return GetValue(inName).ToVector2(defaultvalue);
        }
        #endregion Vector3

        #region RVector
        public void SetValue(string inName, RVector inValue)
        {
            if (inValue.IsEmpty())
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public RVector GetValue(string inName, RVector defaultvalue)
        {
            return GetValue(inName).ToRVector(defaultvalue);
        }
        #endregion RVector

        #region Quaternion
        public void SetValue(string inName, Quaternion inValue)
        {
            SetValue(inName, inValue, Quaternion.identity);
        }
        public void SetValue(string inName, Quaternion inValue, Quaternion inDefault)
        {
            if (inDefault.IsEqual(inValue))
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public Quaternion GetValue(string inName, Quaternion defaultvalue)
        {
            return GetValue(inName).ToQuaternion(defaultvalue);
        }
        #endregion Quaternion

        #region DateTime
        public void SetValue(string inName, DateTime inValue)
        {
            SetValue(inName, inValue, DateTime.MinValue);
        }

        public void SetValue(string inName, DateTime inValue, DateTime inDefault)
        {
            if (inValue == inDefault)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public DateTime GetValue(string inName, DateTime defaultvalue)
        {
            return GetValue(inName).ToDateTime(defaultvalue);
        }
        #endregion

        #region Color
        public void SetValue(string inName, Color inValue)
        {
            SetValue(inName, inValue, Color.black);
        }
        public void SetValue(string inName, Color inValue, Color inDefault)
        {
            if (inDefault == inValue)
                return;

            var value = new Variant(inValue);
            AddValue(inName, ref value);
        }

        public Color GetValue(string inName, Color defaultvalue)
        {
            return GetValue(inName).ToColor(defaultvalue);
        }
        #endregion Color

        #region Enum
        public void SetValueEnum<T>(string inName, T inValue) where T : struct
        {
            var value = new Variant(inValue.ToString());
            AddValue(inName, ref value);
        }

        public T GetValueEnum<T>(string inName, T defaultvalue) where T : struct
        {
            string s = GetValue(inName).ToStringValue();
            if (string.IsNullOrEmpty(s))
                return defaultvalue;
            if (EnumUtils.TryParse(s, out T value))
                return value;
            return default;
        }
        #endregion Enum

        private void GetValuesToCache(string inName, List<Variant> inValueCache)
        {
            if (_start_data_index < 0 || _storage == null)
                return;

            int index = _start_data_index;
            do
            {
                ref Variant val = ref _storage.GetValue(inName, ref index);
                if (!val.IsUndefined)
                    inValueCache.Add(val);
            }
            while (index > 0);
        }

        #region string[]
        public void SetValue(string inName, string[] inValues)
        {
            if (inValues.Empty())
                return;

            for (int i = 0; i < inValues.Length; i++)
            {
                var v = new Variant(inValues[i] ?? string.Empty);
                AddValue(inName, ref v);
            }
        }

        public void SetValue(string inName, List<string> inValues)
        {
            if (inValues.Empty())
                return;

            for (int i = 0; i < inValues.Count; i++)
            {
                var v = new Variant(inValues[i] ?? string.Empty);
                AddValue(inName, ref v);
            }
        }

        public void GetValues(string inName, List<string> outValues)
        {
            if (_start_data_index < 0 || _storage == null)
                return;

            int index = _start_data_index;
            do
            {
                ref Variant val = ref _storage.GetValue(inName, ref index);
                if (!val.IsUndefined)
                    outValues.Add(val.ToStringValue());
            }
            while (index > 0);
        }

        public void GetValues(string inName, out string[] outValues)
        {
            if (_start_data_index < 0 || _storage == null)
            {
                outValues = new string[0];
                return;
            }

            using (CAdvancedList<Variant> value_cache = ListPool<Variant>.Get())
            {
                GetValuesToCache(inName, value_cache);

                outValues = new string[value_cache.Count];
                for (int i = 0; i < value_cache.Count; i++)
                    outValues[i] = value_cache[i].ToStringValue();
            }
        }

        public int GetValuesToArray(string inName, string[] outValues)
        {
            if (_start_data_index < 0 || _storage == null)
            {
                return 0;
            }

            int count = 0;
            using (CAdvancedList<Variant> value_cache = ListPool<Variant>.Get())
            {
                GetValuesToCache(inName, value_cache);

                count = Mathf.Min(value_cache.Count, outValues.Length);
                for (int i = 0; i < count; i++)
                    outValues[i] = value_cache[i].ToStringValue();
            }

            return count;
        }
        #endregion string[]

        #region NamedId[]
        public void SetValue(string inName, NamedId[] inValues)
        {
            if (inValues.Empty())
                return;

            for (int i = 0; i < inValues.Length; i++)
            {
                var v = new Variant(inValues[i]);
                AddValue(inName, ref v);
            }
        }

        public void SetValue(string inName, List<NamedId> inValues)
        {
            if (inValues.Empty())
                return;

            for (int i = 0; i < inValues.Count; i++)
            {
                var v = new Variant(inValues[i]);
                AddValue(inName, ref v);
            }
        }

        public void GetValues(string inName, List<NamedId> outValues)
        {
            if (_start_data_index < 0 || _storage == null)
                return;

            int index = _start_data_index;
            do
            {
                ref Variant val = ref _storage.GetValue(inName, ref index);
                if (!val.IsUndefined)
                    outValues.Add(val.ToNamedId());
            }
            while (index > 0);
        }

        public void GetValues(string inName, out NamedId[] outValues)
        {
            if (_start_data_index < 0 || _storage == null)
            {
                outValues = new NamedId[0];
                return;
            }

            using (CAdvancedList<Variant> value_cache = ListPool<Variant>.Get())
            {
                GetValuesToCache(inName, value_cache);

                outValues = new NamedId[value_cache.Count];
                for (int i = 0; i < value_cache.Count; i++)
                    outValues[i] = value_cache[i].ToNamedId();
            }
        }
        #endregion NamedId[]

        #region int[]
        public void SetValue(string inName, int[] inValues)
        {
            if (inValues.Empty())
                return;

            for (int i = 0; i < inValues.Length; i++)
            {
                var v = new Variant(inValues[i]);
                AddValue(inName, ref v);
            }
        }

        public void SetValue(string inName, List<int> inValues)
        {
            if (inValues.Empty())
                return;

            for (int i = 0; i < inValues.Count; i++)
            {
                var v = new Variant(inValues[i]);
                AddValue(inName, ref v);
            }
        }

        public void GetValues(string inName, List<int> outValues)
        {
            if (_start_data_index < 0 || _storage == null)
                return;

            int index = _start_data_index;
            do
            {
                ref Variant val = ref _storage.GetValue(inName, ref index);
                if (!val.IsUndefined)
                    outValues.Add(val.ToInt());
            }
            while (index > 0);
        }

        public void GetValues(string inName, out int[] outValues)
        {
            if (_start_data_index < 0 || _storage == null)
            {
                outValues = new int[0];
                return;
            }

            using (CAdvancedList<Variant> value_cache = ListPool<Variant>.Get())
            {
                GetValuesToCache(inName, value_cache);

                outValues = new int[value_cache.Count];
                for (int i = 0; i < value_cache.Count; i++)
                    outValues[i] = value_cache[i].ToInt();
            }
        }
        #endregion int[]

        #region ulong[]
        public void SetValue(string inName, ulong[] inValues)
        {
            if (inValues.Empty())
                return;

            for (int i = 0; i < inValues.Length; i++)
            {
                var v = new Variant(inValues[i]);
                AddValue(inName, ref v);
            }
        }

        public void SetValue(string inName, List<ulong> inValues)
        {
            if (inValues.Empty())
                return;

            for (int i = 0; i < inValues.Count; i++)
            {
                var v = new Variant(inValues[i]);
                AddValue(inName, ref v);
            }
        }

        public void GetValues(string inName, List<ulong> outValues)
        {
            if (_start_data_index < 0 || _storage == null)
                return;

            int index = _start_data_index;
            do
            {
                ref Variant val = ref _storage.GetValue(inName, ref index);
                if (!val.IsUndefined)
                    outValues.Add(val.ToULong());
            }
            while (index > 0);
        }

        public void GetValues(string inName, out ulong[] outValues)
        {
            if (_start_data_index < 0 || _storage == null)
            {
                outValues = new ulong[0];
                return;
            }

            using (CAdvancedList<Variant> value_cache = ListPool<Variant>.Get())
            {
                GetValuesToCache(inName, value_cache);

                outValues = new ulong[value_cache.Count];
                for (int i = 0; i < value_cache.Count; i++)
                    outValues[i] = value_cache[i].ToULong();
            }
        }
        #endregion ulong[]

        #endregion Values

        public string GetPath()
        {

            if (!Inited)
                return string.Empty;

            string path = string.Empty;
            if (_parent != null)
                path = _parent.GetPath();

            if (_load_string_index == 0)
                return $"{path}/{_own_name}";
            return $"{path}/{_own_name}[{_load_string_index}]";
        }

        private string KeyValuesToString()
        {
            if (_start_data_index < 0 || _storage == null)
                return string.Empty;

            var sb = new StringBuilder("Values: [");
            int index = _start_data_index;
            string last_name = string.Empty;
            do
            {
                (string name, string value) = _storage.GetValueAsStringPair(ref index, false);
                if (string.Equals(last_name, name))
                {
                    sb.Append($", {value}");
                }
                else
                {
                    if (string.IsNullOrEmpty(last_name))
                        sb.Append($"{name}: {value}");
                    else
                        sb.Append($"; {name}: {value}");
                    last_name = name;
                }
            }
            while (index > 0);

            sb.Append("] ");
            return sb.ToString();
        }

        public override string ToString()
        {
            var sb = new StringBuilder($"Path[{_load_string_index + 1}]: {GetPath()};");

            sb.Append(KeyValuesToString());

            if (_dic_recs != null && _dic_recs.Count > 0)
            {
                sb.Append(" Records: [");
                for (int i = 0; i < _dic_recs.Count; i++)
                {
                    if (i > 0)
                        sb.Append(", ");

                    SSerializeKey key = _dic_recs.GetKeyByIndex(i);
                    sb.Append(key);
                }
                sb.Append("]");
            }

            return sb.ToString();
        }

        public bool IsEmpty()
        {
            return _start_data_index == -1 && _dic_recs == null;
        }

        public bool IsValuePresent(string inName)
        {
            return _start_data_index != -1 && _storage != null && _storage.IsValuePresent(inName, _start_data_index);
        }

        public bool IsRecordPresent(string inName)
        {
            return _dic_recs != null && _dic_recs.Contains(new SSerializeKey(inName));
        }

        public static char ValueDivider = '#';
        public static string ValueCaption = "Values";
        public static string RecordsCaption = "Records";

        const int MAX_INTEND = 50;
        static string[] Intends = new string[MAX_INTEND];
        static string GetIntend(int length)
        {
            if (length >= MAX_INTEND)
                return string.Empty;

            if (Intends[length] == null)
            {
                Intends[length] = new string('\t', length);
            }
            return Intends[length];
        }

        public void Save(StringBuilder inSB, int inDeep)
        {
            if (!_data_present)
                return;

            bool val_present = _start_data_index >= 0;
            bool rec_present = _dic_recs != null && _dic_recs.Count > 0;

            if (!val_present && !rec_present)
                return;

            string intend0 = GetIntend(inDeep);
            string intend1 = GetIntend(inDeep + 1);
            string intend2 = GetIntend(inDeep + 2);
            string intend3 = GetIntend(inDeep + 3);

            inSB.AppendLine($"{intend0}{_own_name}");

            if (val_present)
            {
                inSB.AppendLine($"{intend1}{ValueCaption}");

                int index = _start_data_index;

                string last_name = string.Empty;
                do
                {
                    (string name, string value) = _storage.GetValueAsStringPair(ref index, true);
                    if (!string.Equals(last_name, name))
                    {
                        inSB.AppendLine($"{intend2}{name}");
                        last_name = name;
                    }

                    inSB.AppendLine($"{intend3}{value}");
                }
                while (index > 0);
            }

            if (rec_present)
            {
                bool wrote_caption = false;

                for (int i = 0; i < _dic_recs.Count; i++)
                {
                    CSerializedDictionary child_rec = _dic_recs.GetValueByIndex(i);
                    if (child_rec.IsDataPresent)
                    {
                        if (!wrote_caption)
                        {
                            inSB.AppendLine($"{intend1}{RecordsCaption}");
                            wrote_caption = true;
                        }

                        child_rec.Save(inSB, inDeep + 2);
                    }
                }
            }
        }

        public int Load(string[] inLines, int inIndex, StringBuilder inErrorSB, int inCallDeep)
        {
            if (inCallDeep >= 200)
            {
                inErrorSB.AppendLine("Read records: Too deep!");
                inErrorSB.AppendLine($"Line index: {inIndex}");
                return inIndex;
            }

            //_own_name = CSerializeStorage.ClearTabs(inLines[inIndex], out int start_deep);
            _load_string_index = inIndex;
            inIndex++;

            string next_str = CSerializeStorage.ClearTabs(inLines[inIndex], out int next_deep);

            if (string.Equals(ValueCaption, next_str, StringComparison.Ordinal))
            {
                int value_caption_deep = next_deep;

                inIndex++;
                if (inIndex >= inLines.Length)
                {
                    inErrorSB.AppendLine("Wrong end of Record: Open Values");
                    inErrorSB.AppendLine($"Line index: {inIndex}");
                    return inIndex;
                }

                do
                {
                    (inIndex, next_deep, next_str) = LoadValues(inLines, inIndex, inErrorSB);
                    if (inIndex == -1)
                    {
                        return -1;
                    }

                    if (inIndex >= inLines.Length)
                    {
                        return inIndex;
                    }
                }
                while (next_deep == value_caption_deep + 1);
            }

            if (string.Equals(RecordsCaption, next_str, StringComparison.Ordinal))
            {
                int record_caption_deep = next_deep;

                inIndex++;
                if (inIndex >= inLines.Length)
                {
                    inErrorSB.AppendLine("Wrong end of Record: Open Records");
                    inErrorSB.AppendLine($"Line index: {inIndex}");
                    return inIndex;
                }

                string rec_key_str = CSerializeStorage.ClearTabs(inLines[inIndex], out next_deep);
                while (next_deep == record_caption_deep + 1)
                {
                    if (!SSerializeKey.TryParse(rec_key_str, out SSerializeKey key))
                        return -1;

                    CSerializedDictionary child = _storage.CreateRecord(this, key.ToString());
                    try
                    {
                        inIndex = child.Load(inLines, inIndex, inErrorSB, inCallDeep + 1);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError(ex);
                        return -1;
                    }
                    if (inIndex == -1)
                        return -1;

                    if (_dic_recs == null)
                        _dic_recs = new CDicionaryList<SSerializeKey, CSerializedDictionary>(SSerializeKey.Comparer);

                    _dic_recs.Add(key, child);

                    if (inIndex >= inLines.Length)
                        return inIndex;

                    rec_key_str = CSerializeStorage.ClearTabs(inLines[inIndex], out next_deep);
                }
            }

            return inIndex;
        }

        private (int, int, string) LoadValues(string[] inLines, int inIndex, StringBuilder inErrorSB)
        {
            string values_name = CSerializeStorage.ClearTabs(inLines[inIndex], out int start_deep);

            if (!SSerializeKey.TryParse(values_name, out SSerializeKey key))
            {
                inErrorSB.AppendLine($"Can't parse SSerializeKey: {values_name}");
                inErrorSB.AppendLine($"Line index: {inIndex}");
                return (-1, -1, string.Empty);
            }

            inIndex++;
            if (inIndex >= inLines.Length)
            {
                inErrorSB.AppendLine("Wrong end of Value");
                inErrorSB.AppendLine($"Line index: {inIndex}");
                return (-1, -1, string.Empty);
            }

            string next_str = CSerializeStorage.ClearTabs(inLines[inIndex], out int next_deep);
            while (next_deep == start_deep + 1)
            {
                {
                    var value = new Variant();
                    if (!value.LoadFromString(next_str))
                    {
                        inErrorSB.AppendLine($"Can't parse Variant: {next_str}");
                        inErrorSB.AppendLine($"Line index: {inIndex}");
                        return (-1, -1, string.Empty);
                    }

                    _storage.AddValue(key.Name, ref value, ref _last_data_index);
                    if (_start_data_index == -1)
                        _start_data_index = _last_data_index;
                }

                inIndex++;
                if (inIndex >= inLines.Length)
                    return (inIndex, 0, string.Empty);

                next_str = CSerializeStorage.ClearTabs(inLines[inIndex], out next_deep);
            }

            return (inIndex, next_deep, next_str);
        }
    }
}