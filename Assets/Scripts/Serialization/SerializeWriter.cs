﻿
using System;
using UnityEngine;

namespace Serialization
{
    public struct SSerializeWriter
    {
        private CSerializeStorage2 _storage;
        public CSerializeStorage2 Storage { get { return _storage; } }

        private int _index_in_storage;
        public int IndexInStorage { get { return _index_in_storage; } }

        public SSerializeWriter(CSerializeStorage2 inStorage)
        {
            _storage = inStorage;
            _index_in_storage = -1;
        }

        private void TakeIndex()
        {
            if (_index_in_storage != -1)
                return;
            _index_in_storage = _storage.GetNewRecordIndex();

            //CascadeParser.IKeyFactory.CreateKey
        }

        #region NamedId
        public bool SetValue(string inName, NamedId inValue)
        {
            return SetValue(inName, inValue, NamedId.Empty);
        }

        public bool SetValue(string inName, NamedId inValue, NamedId inDefaultValue)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, NamedId inValue, NamedId inDefaultValue)
        {
            if (inValue == inDefaultValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion NamedId

        #region float
        public bool SetValue(string inName, float inValue, float inDefaultValue = 0)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, float inValue, float inDefaultValue)
        {
            if (GameMath.Approximately(inValue, inDefaultValue))
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion float

        #region double
        public bool SetValue(string inName, double inValue, double inDefaultValue = 0)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, double inValue, double inDefaultValue)
        {
            if (GameMath.Approximately(inValue, inDefaultValue))
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion double

        #region int
        public bool SetValue(string inName, int inValue, int inDefaultValue = 0)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, int inValue, int inDefaultValue)
        {
            if (inValue == inDefaultValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion int

        #region uint
        public bool SetValue(string inName, uint inValue, uint inDefaultValue = 0)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, uint inValue, uint inDefaultValue)
        {
            if (inValue == inDefaultValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall((ulong)inValue));

            return true;
        }
        #endregion uint

        #region ulong
        public bool SetValue(string inName, ulong inValue, ulong inDefaultValue = 0)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, ulong inValue, ulong inDefaultValue)
        {
            if (inValue == inDefaultValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion ulong

        #region long
        public bool SetValue(string inName, long inValue, long inDefaultValue = 0)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, long inValue, long inDefaultValue)
        {
            if (inValue == inDefaultValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion long

        #region bool
        public bool SetValue(string inName, bool inValue, bool inDefaultValue = false)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, bool inValue, bool inDefaultValue)
        {
            if (inValue == inDefaultValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion bool

        #region Vector3
        public bool SetValue(string inName, Vector3 inValue)
        {
            return SetValue(new SSerializeKey(inName), inValue, Vector3.zero);
        }

        public bool SetValue(string inName, Vector3 inValue, Vector3 inDefaultValue)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, Vector3 inValue, Vector3 inDefaultValue)
        {
            if (inValue == inDefaultValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion Vector3

        #region Vector2
        public bool SetValue(string inName, Vector2 inValue)
        {
            return SetValue(new SSerializeKey(inName), inValue, Vector2.zero);
        }

        public bool SetValue(string inName, Vector2 inValue, Vector2 inDefaultValue)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, Vector2 inValue, Vector2 inDefaultValue)
        {
            if (inValue == inDefaultValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, new VariantSmall(inValue));

            return true;
        }
        #endregion Vector3

        #region string
        public bool SetValue(string inName, string inValue)
        {
            return SetValue(inName, inValue, string.Empty);
        }

        public bool SetValue(string inName, string inValue, string inDefaultValue)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, string inValue, string inDefaultValue)
        {
            if (string.Compare(inValue, inDefaultValue, StringComparison.Ordinal) == 0)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, inValue);

            return true;
        }
        #endregion string

        #region Quaternion
        public bool SetValue(string inName, Quaternion inValue)
        {
            return SetValue(inName, inValue, Quaternion.identity);
        }

        public bool SetValue(string inName, Quaternion inValue, Quaternion inDefaultValue)
        {
            return SetValue(new SSerializeKey(inName), inValue, inDefaultValue);
        }

        private bool SetValue(in SSerializeKey inKey, Quaternion inValue, Quaternion inDefaultValue)
        {
            if (inDefaultValue == inValue)
                return false;

            TakeIndex();

            _storage.WriteValue(_index_in_storage, inKey, inValue);

            return true;
        }
        #endregion Quaternion

        #region AddRecord
        public bool AddRecord(NamedId inName, SSerializeWriter inRecordWriter)
        {
            return AddRecord(new SSerializeKey(inName), inRecordWriter);
        }

        public bool AddRecord(string inName, SSerializeWriter inRecordWriter)
        {
            return AddRecord(new SSerializeKey(inName), inRecordWriter);
        }

        public bool AddRecord(string inName, int inId, SSerializeWriter inRecordWriter)
        {
            return AddRecord(new SSerializeKey(inName, inId), inRecordWriter);
        }

        private bool AddRecord(in SSerializeKey inKey, SSerializeWriter inRecordWriter)
        {
            if (inRecordWriter.IndexInStorage == -1)
                return false;

            TakeIndex();

            _storage.AddRecord(IndexInStorage, inKey, inRecordWriter.IndexInStorage);

            return true;
        }
        #endregion AddRecord
    }
}