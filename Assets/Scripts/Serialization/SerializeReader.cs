﻿using UnityEngine;

namespace Serialization
{
    public struct SSerializeReader
    {
        public static SSerializeReader Empty = new SSerializeReader();

        private CSerializeStorage2 _storage;
        public CSerializeStorage2 Storage { get { return _storage; } }

        private int _index_in_storage;
        public int IndexInStorage { get { return _index_in_storage; } }

        readonly bool _inited;
        public bool Inited { get { return _inited; } }

        public SSerializeReader(CSerializeStorage2 inStorage, int inIndexInStorage)
        {
            _storage = inStorage;
            _index_in_storage = inIndexInStorage;
            _inited = _storage != null && _index_in_storage != -1;
        }

        public static Variant Undefined = new Variant();

        private ref Variant GetValue(in SSerializeKey inKey)
        {
            if (_storage == null || _index_in_storage == -1)
                return ref Undefined;

            ref Variant value = ref _storage.GetValue(_index_in_storage, inKey);
            return ref value;
        }

        #region NamedId
        public NamedId GetValue(string inName, NamedId inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private NamedId GetValue(in SSerializeKey inKey, NamedId inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToNamedId();
        }
        #endregion NamedId

        #region bool
        public bool GetValue(string inName, bool inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private bool GetValue(in SSerializeKey inKey, bool inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToBool();
        }
        #endregion bool

        #region float
        public float GetValue(string inName, float inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private float GetValue(in SSerializeKey inKey, float inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToFloat();
        }
        #endregion float

        #region double
        public double GetValue(string inName, double inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private double GetValue(in SSerializeKey inKey, double inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToDouble();
        }
        #endregion double

        #region string
        public string GetValue(string inName, string inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private string GetValue(in SSerializeKey inKey, string inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToStringValue();
        }
        #endregion string

        #region ulong
        public ulong GetValue(string inName, ulong inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private ulong GetValue(in SSerializeKey inKey, ulong inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToULong();
        }
        #endregion ulong

        #region long
        public long GetValue(string inName, long inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private long GetValue(in SSerializeKey inKey, long inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToLong();
        }
        #endregion long

        #region uint
        public uint GetValue(string inName, uint inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private uint GetValue(in SSerializeKey inKey, uint inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToUInt();
        }
        #endregion uint

        #region int
        public int GetValue(string inName, int inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private int GetValue(in SSerializeKey inKey, int inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToInt();
        }
        #endregion int

        #region Vector2
        public Vector2 GetValue(string inName, Vector2 inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private Vector2 GetValue(in SSerializeKey inKey, Vector2 inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToVector2();
        }
        #endregion Vector2

        #region Vector3
        public Vector3 GetValue(string inName, Vector3 inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private Vector3 GetValue(in SSerializeKey inKey, Vector3 inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;

            return value.ToVector3();
        }
        #endregion Vector3

        #region Quaternion
        public Quaternion GetValue(string inName, Quaternion inDefaultValue)
        {
            return GetValue(new SSerializeKey(inName), inDefaultValue);
        }

        private Quaternion GetValue(in SSerializeKey inKey, Quaternion inDefaultValue)
        {
            ref Variant value = ref GetValue(inKey);
            if (value.IsUndefined)
                return inDefaultValue;
            return value.ToQuaternion();
        }
        #endregion Quaternion

        public SSerializeReader GetSerializeReader(NamedId inName)
        {
            return GetSerializeReader(new SSerializeKey(inName));
        }

        public SSerializeReader GetSerializeReader(string inName)
        {
            return GetSerializeReader(new SSerializeKey(inName));
        }

        public SSerializeReader GetSerializeReader(string inName, int inId)
        {
            return GetSerializeReader(new SSerializeKey(inName, inId));
        }

        private SSerializeReader GetSerializeReader(in SSerializeKey inKey)
        {
            if (_storage == null || _index_in_storage == -1)
                return new SSerializeReader();

            return _storage.GetChildReader(this, inKey);
        }
    }
}