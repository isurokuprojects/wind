﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Serialization
{
    public class CSerializeStorage : IDisposable
    {
        private Version _version;

        public Version SaveVersion { get { return _version; } }

        private CStructStore<Variant> _values;
        private Stack<CSerializedDictionary> _dics;
        private Stack<CAdvancedList<int>> _int_lists;

        private CSerializedDictionary _start_rec;

        uint _id_counter;

        private struct SValueRec
        {
            public string Name;
            public int NameHash;
            public Variant Value;
            public int NextValue;

            public override string ToString()
            {
                if (NextValue == 0)
                    return $"{Name}: {Value}";
                return $"{Name}: {Value}; Next {NextValue}";
            }
        }

        static int Size = 1000;
        SValueRec[] _flat_values = new SValueRec[Size];
        int _last_index;

        public CSerializeStorage()
        {
            _values = new CStructStore<Variant>(100);
            _dics = new Stack<CSerializedDictionary>();
            _int_lists = new Stack<CAdvancedList<int>>();
        }

        public void Clear()
        {
            _last_index = 0;

            if (_start_rec != null)
            {
                _start_rec.Release();
                _start_rec = null;
            }

            _version = default(Version);

            for (int i = 0; i < _values.Count; i++)
            {
                _values[i].Clear();
            }
            _values.Clear(false);
        }

        public void Dispose()
        {
            CSerializeManager.ReleaseStorage(this);
        }

        public void AddValue(string inName, ref Variant inValue, ref int ioCurrIndex)
        {
            if (_last_index + 1 >= _flat_values.Length)
            {
                SValueRec[] oldvalues = _flat_values;
                _flat_values = new SValueRec[oldvalues.Length + Size];
                Array.Copy(oldvalues, _flat_values, oldvalues.Length);
            }

            ref SValueRec rec = ref _flat_values[_last_index];
            rec.Name = inName;
            rec.NameHash = inName.GetHashCode();
            rec.Value = inValue;
            rec.NextValue = 0;

            if (ioCurrIndex >= 0)
            {
                ref SValueRec prev_rec = ref _flat_values[ioCurrIndex];
                prev_rec.NextValue = _last_index;
            }

            ioCurrIndex = _last_index;

            _last_index++;
        }

        public ref Variant GetFirstValue(string inName, int inStartIndex)
        {
            if (inStartIndex < 0)
            {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
                    () => $"inStartIndex [{inStartIndex}] < 0");
#endif
                return ref Variant.Undefined;
            }

            int index = inStartIndex;

            int in_hash = inName.GetHashCode();
            do
            {
                ref SValueRec rec = ref _flat_values[index];

                index = rec.NextValue;

                if (rec.NameHash == in_hash)
                {
                    return ref rec.Value;
                }
            }
            while (index != 0);

            return ref Variant.Undefined;
        }

        public bool IsValuePresent(string inName, int inStartIndex)
        {
            if (inStartIndex < 0)
            {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
                    () => $"inStartIndex [{inStartIndex}] < 0");
#endif
                return false;
            }

            int index = inStartIndex;

            int in_hash = inName.GetHashCode();
            do
            {
                ref SValueRec rec = ref _flat_values[index];

                index = rec.NextValue;

                if (rec.NameHash == in_hash)
                {
                    return true;
                }
            }
            while (index != 0);

            return false;
        }

        public int GetValueCount(string inName, int inStartIndex)
        {
            if (inStartIndex < 0)
            {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
                    () => $"inStartIndex [{inStartIndex}] < 0");
#endif
                return 0;
            }

            int count = 0;

            int index = inStartIndex;

            int in_hash = inName.GetHashCode();
            do
            {
                ref SValueRec rec = ref _flat_values[index];

                index = rec.NextValue;

                if (rec.NameHash == in_hash)
                {
                    count++;
                }
            }
            while (index != 0);

            return count;
        }

        public ref Variant GetValue(string inName, ref int ioCurrIndex)
        {
            if (ioCurrIndex < 0)
            {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
                    () => $"ioCurrIndex < 0");
#endif
                return ref Variant.Undefined;
            }

            int in_hash = inName.GetHashCode();
            do
            {
                ref SValueRec rec = ref _flat_values[ioCurrIndex];

                ioCurrIndex = rec.NextValue;

                if (rec.NameHash == in_hash)
                {
                    return ref rec.Value;
                }
            }
            while (ioCurrIndex != 0);

            return ref Variant.Undefined;
        }

        public ref Variant GetValue(int inIndex, out string outName)
        {
            if (inIndex < 0)
            {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
                    () => $"inStartIndex [{inIndex}] < 0");
#endif
                outName = string.Empty;
                return ref Variant.Undefined;
            }

            ref SValueRec rec = ref _flat_values[inIndex];
            outName = rec.Name;
            return ref rec.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ioCurrIndex"></param>
        /// <returns>Name, Value as string</returns>
        public (string, string) GetValueAsStringPair(ref int ioCurrIndex, bool inValueForSave)
        {
            if (ioCurrIndex < 0)
            {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
                    () => $"ioCurrIndex < 0");
#endif
                return (string.Empty, string.Empty);
            }

            ref SValueRec rec = ref _flat_values[ioCurrIndex];
            ioCurrIndex = rec.NextValue;

            string sval;
            if (inValueForSave)
                sval = rec.Value.SaveToString();
            else
                sval = rec.Value.ToString();

            return (rec.Name, sval);
        }

        public void GetAllValues(int inStartIndex, Action<string, Variant> inAction)
        {
            if (inStartIndex < 0)
            {
#if ASSERTS
                CLogContext.Current.WriteToLog(ELogLevel.ERROR, () => GetType().Name, () => MethodBase.GetCurrentMethod().Name,
                    () => $"inStartIndex < 0");
#endif
                return;
            }

            int index = inStartIndex;
            do
            {
                ref SValueRec rec = ref _flat_values[index];

                inAction(rec.Name, rec.Value);

                index = rec.NextValue;
            }
            while (index != 0);
        }

        public CSerializedDictionary GetStartRecord()
        {
            if (_start_rec == null)
                return CSerializedDictionary.Empty;
            return _start_rec;
        }

        public CSerializedDictionary CreateStartDic(string inOwnName)
        {
            _start_rec = CreateRecord(null, inOwnName);
            return _start_rec;
        }

        public CSerializedDictionary CreateRecord(CSerializedDictionary inParent, string inOwnName)
        {
            CSerializedDictionary new_rec = null;
            if (_dics.Count > 0)
                new_rec = _dics.Pop();
            else
                new_rec = new CSerializedDictionary();

            new_rec.Init(this, inParent, inOwnName, ++_id_counter);

            return new_rec;
        }

        public void ReleaseRecord(CSerializedDictionary inRec)
        {
            inRec.Release();
            _dics.Push(inRec);
        }

        public CAdvancedList<int> GetNewIntList()
        {
            CAdvancedList<int> list = null;
            if (_int_lists.Count > 0)
                list = _int_lists.Pop();
            else
                list = new CAdvancedList<int>();

            return list;
        }

        public void ReleaseIntList(CAdvancedList<int> inList)
        {
            inList.Clear();
            _int_lists.Push(inList);
        }

        public int GetNewValueIndex()
        {
            return _values.Add();
        }

        public ref Variant GetValue(int inValueIndex)
        {
            if (inValueIndex == -1)
                return ref Variant.Undefined;

            return ref _values[inValueIndex];
        }

        const string VersionToken = "Version: ";
        public bool SaveTo(StringBuilder inSB, Version inVersion)
        {
            if (_start_rec == null || !_start_rec.IsDataPresent)
                return false;

            inSB.AppendLine($"{VersionToken}{inVersion}");

            _start_rec.Save(inSB, 0);

            return true;
        }

        public bool LoadFrom(string[] inLines, StringBuilder inErrorSB, Version inVersion)
        {
            int line_index = 0;
            int ind_tok = inLines[line_index].IndexOf(VersionToken);
            if (ind_tok == -1)
            {
                inErrorSB.AppendLine("Can't find VersionToken");
                return false;
            }

            string str = inLines[line_index].Substring(ind_tok + VersionToken.Length);
            if (!Version.TryParse(str, out _version))
            {
                inErrorSB.AppendLine($"Can't parse Version number: {str}");
                return false;
            }

            if (inVersion.Major != _version.Major ||
                inVersion.Minor != _version.Minor)
            {
                return true;
            }

            line_index++;
            string rec_key_str = CSerializeStorage.ClearTabs(inLines[line_index], out int rec_deep);
            if (!SSerializeKey.TryParse(rec_key_str, out SSerializeKey key))
            {
                inErrorSB.AppendLine($"Can't parse SSerializeKey: {rec_key_str}");
                return false;
            }

            _start_rec = CreateRecord(null, key.ToString());

            line_index = _start_rec.Load(inLines, line_index, inErrorSB, 0);

            return line_index >= inLines.Length;
        }

        public static string ClearTabs(string inString, out int outTabCount)
        {
            outTabCount = 0;

            while (inString.Length > outTabCount && inString[outTabCount] == '\t')
            {
                outTabCount++;
            }

            if (inString.Length <= outTabCount)
                return string.Empty;

            return inString.Substring(outTabCount);
        }
    }

    public class CSerializeStorage2
    {
        int _start_index = -1;
        private int _save_version;
        public int SaveVersion { get { return _save_version; } }

        private CStructStore<SSerializeRecord> _records;
        private CStructStore<Variant> _values;

        public CSerializeStorage2()
        {
            _save_version = -1;

            _records = new CStructStore<SSerializeRecord>(20);
            _values = new CStructStore<Variant>(100);
        }

        public void Clear()
        {
            _start_index = -1;
            _save_version = -1;

            for (int i = 0; i < _records.Count; i++)
            {
                _records[i].Clear();
            }
            _records.Clear(false);

            for (int i = 0; i < _values.Count; i++)
            {
                _values[i].Clear();
            }
            _values.Clear(false);
        }

        public SSerializeWriter GetWriter()
        {
            return new SSerializeWriter(this);
        }

        internal void AddRecord(int inParentIndexInStorage, in SSerializeKey inKey, int inChildIndexInStorage)
        {
            ref SSerializeRecord parent_rec = ref _records[inParentIndexInStorage];
            parent_rec.AddChildRecordIndex(inKey, inChildIndexInStorage);
        }

        public SSerializeReader GetChildReader(SSerializeReader inParentReader, in SSerializeKey inKey)
        {
            ref SSerializeRecord parent_rec = ref _records[inParentReader.IndexInStorage];
            int index = parent_rec.GetChildRecordIndex(inKey);
            return new SSerializeReader(this, index);
        }

        public int GetNewRecordIndex()
        {
            return _records.Add();
        }

        public int GetNewValueIndex()
        {
            return _values.Add();
        }

        private ref Variant WriteValue(int inIndexInStorage, in SSerializeKey inKey)
        {
            int value_index = _values.Add();

            ref SSerializeRecord rec = ref _records[inIndexInStorage];
            rec.AddValueIndex(inKey, value_index);

            return ref _values[value_index];
        }

        public void WriteValue(int inIndexInStorage, in SSerializeKey inKey, VariantSmall inValue)
        {
            if (inIndexInStorage == -1)
                return;

            ref Variant value = ref WriteValue(inIndexInStorage, inKey);

            value.Set(inValue);
        }

        public void WriteValue(int inIndexInStorage, in SSerializeKey inKey, string inValue)
        {
            if (inIndexInStorage == -1)
                return;

            ref Variant value = ref WriteValue(inIndexInStorage, inKey);

            value.Set(inValue);
        }

        public void WriteValue(int inIndexInStorage, SSerializeKey inKey, Quaternion inValue)
        {
            if (inIndexInStorage == -1)
                return;

            ref Variant value = ref WriteValue(inIndexInStorage, inKey);

            value.Set(inValue);
        }

        public ref Variant GetValue(int inIndexInStorage, SSerializeKey inKey)
        {
            if (inIndexInStorage == -1)
                return ref Variant.Undefined;

            ref SSerializeRecord rec = ref _records[inIndexInStorage];
            int value_index = rec.GetValueIndex(inKey);

            return ref GetValue(value_index);
        }

        public ref Variant GetValue(int inValueIndex)
        {
            if (inValueIndex == -1)
                return ref Variant.Undefined;

            return ref _values[inValueIndex];
        }

        public void SetVersion(int inVersion)
        {
            _save_version = inVersion;
        }

        public void SetStartWriter(SSerializeWriter inWriter)
        {
            _start_index = inWriter.IndexInStorage;
        }

        public ref SSerializeRecord GetChildRecord(int inIndexInStorage)
        {
            return ref _records[inIndexInStorage];
        }

        const string VersionToken = "Version: ";
        public void SaveTo(StringBuilder inSB)
        {
            inSB.AppendLine($"{VersionToken}{_save_version}");

            if (_start_index == -1)
                return;

            ref SSerializeRecord rec = ref _records[_start_index];
            rec.Save(this, inSB, 0);
        }

        public bool LoadFrom(string[] inLines, StringBuilder inErrorSB)
        {
            int line_index = 0;
            int ind_tok = inLines[line_index].IndexOf(VersionToken);
            if (ind_tok == -1)
            {
                inErrorSB.AppendLine("Can't find VersionToken");
                return false;
            }

            string str = inLines[line_index].Substring(ind_tok + VersionToken.Length);
            if (!int.TryParse(str, out _save_version))
            {
                inErrorSB.AppendLine($"Can't parse Version number: {str}");
                return false;
            }

            line_index++;

            _start_index = _records.Add();
            Dictionary<int, int> recs = new Dictionary<int, int>();
            recs.Add(-1, _start_index);

            for (int i = line_index; i < inLines.Length; i++)
            {
                EParseResult parse_res = ParseLine(inLines, i, out int outTabCount, out SSerializeKey outKey, out int outValueIndex, inErrorSB);

                if (parse_res == EParseResult.Error)
                {
                    return false;
                }

                int parent_index = recs[outTabCount - 1];
                ref SSerializeRecord parent_rec = ref _records[parent_index];

                if (parse_res == EParseResult.Value)
                {
                    parent_rec.AddValueIndex(outKey, outValueIndex);
                }
                else
                {
                    int child_index = _records.Add();
                    parent_rec.AddChildRecordIndex(outKey, child_index);

                    if (recs.ContainsKey(outTabCount))
                        recs[outTabCount] = child_index;
                    else
                        recs.Add(outTabCount, child_index);
                }
            }
            return true;
        }

        enum EParseResult { Error, Record, Value }
        EParseResult ParseLine(string[] inLines, int inLineIndex, out int outTabCount, out SSerializeKey outKey, out int outValueIndex, StringBuilder inErrorSB)
        {
            outTabCount = -1;
            outValueIndex = -1;
            string curr_str = inLines[inLineIndex];

            int div_index = curr_str.IndexOf(SSerializeRecord.Divider);
            if (div_index == -1)
            {
                inErrorSB.AppendLine($"Can't find div({SSerializeRecord.Divider}). String [${inLineIndex + 1}]: {curr_str}");
                outKey = new SSerializeKey();
                return EParseResult.Error;
            }

            string key_str = curr_str.Substring(0, div_index);
            key_str = ClearTabs(key_str, out outTabCount);

            if (!SSerializeKey.TryParse(key_str, out outKey))
            {
                inErrorSB.AppendLine($"Can't parse key({key_str}). String [${inLineIndex + 1}]: {curr_str}");
                return EParseResult.Error;
            }

            if (div_index < curr_str.Length - 1)
            {
                outValueIndex = _values.Add();
                ref Variant value = ref _values[outValueIndex];

                string value_str = curr_str.Substring(div_index + 1).Trim();
                if (!value.LoadFromString(value_str))
                {
                    inErrorSB.AppendLine($"Can't parse variant value({value_str}). String [${inLineIndex + 1}]: {curr_str}");
                    return EParseResult.Error;
                }

                return EParseResult.Value;
            }
            return EParseResult.Record;
        }

        string ClearTabs(string inString, out int outTabCount)
        {
            outTabCount = 0;

            while (inString.Length > outTabCount && inString[outTabCount] == '\t')
            {
                outTabCount++;
            }

            if (inString.Length <= outTabCount)
                return string.Empty;

            return inString.Substring(outTabCount);
        }

        public bool RecursiveEquals(in CSerializeStorage2 other, StringBuilder inErrorSB)
        {
            if (_save_version != other._save_version)
                return false;

            if (_records.Count != other._records.Count)
                return false;

            if (_values.Count != other._values.Count)
                return false;

            return true;
        }
    }
}