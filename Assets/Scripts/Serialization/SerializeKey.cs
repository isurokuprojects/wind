﻿
using System.Collections.Generic;

namespace Serialization
{
    public readonly struct SSerializeKey
    {
        readonly string _name;
        readonly int _index;
        readonly int _hash;
        readonly bool _inited;

        public string Name { get { return _name; } }
        public int Index { get { return _index; } }

        public bool Inited { get { return _inited; } }
        public static SSerializeKey Empty = new SSerializeKey(string.Empty);

        public SSerializeKey(string inName, int inIndex)
        {
            _name = inName;
            _index = inIndex;

            _hash = HashFunctions.HashCombine(_name.GetHashCode(), _index);
            _inited = true;
        }

        public SSerializeKey(string inName)
        {
            _name = inName;
            _index = -1;

            _hash = _name.GetHashCode();
            _inited = true;
        }

        public SSerializeKey(NamedId inName)
        {
            _name = inName.name;
            _index = -1;

            _hash = _name.GetHashCode();
            _inited = true;
        }

        const char Divider = '|';
        public override string ToString()
        {
            if (_index == -1)
                return _name;
            return $"{_index}{Divider}{_name}";
        }

        public static bool TryParse(string inStr, out SSerializeKey outKey)
        {
            int div_index = inStr.IndexOf(Divider);

            if (div_index < 0)
            {
                outKey = new SSerializeKey(inStr);
                return true;
            }

            if (div_index > inStr.Length - 1)
            {
                outKey = new SSerializeKey();
                return false;
            }

            string str = inStr.Substring(0, div_index);
            if (!int.TryParse(str, out int index))
            {
                outKey = new SSerializeKey();
                return false;
            }

            str = inStr.Substring(div_index + 1).Trim();

            outKey = new SSerializeKey(str, index);
            return true;
        }

        public override int GetHashCode()
        {
            return _hash;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SSerializeKey)obj);
        }

        public bool Equals(in SSerializeKey other)
        {
            return _hash == other._hash;
        }

        public static bool operator ==(in SSerializeKey left, in SSerializeKey right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(in SSerializeKey left, in SSerializeKey right)
        {
            return !left.Equals(right);
        }

        public class CRecordKeyComparer : IEqualityComparer<SSerializeKey>
        {
            public bool Equals(SSerializeKey x, SSerializeKey y) { return x == y; }
            public int GetHashCode(SSerializeKey obj) { return obj.GetHashCode(); }
        }

        public static CRecordKeyComparer Comparer = new CRecordKeyComparer();

        public class CSortKeyComparer : IComparer<SSerializeKey>
        {
            public int Compare(SSerializeKey x, SSerializeKey y)
            {
                return x._index.CompareTo(y._index);
            }
        }
        public static CSortKeyComparer SortComparer = new CSortKeyComparer();

    }

    //public readonly struct SSerializeKey2
    //{
    //    readonly string _name;
    //    readonly int _hash;
    //    readonly bool _inited;

    //    public string Name { get { return _name; } }
    //    public bool Inited { get { return _inited; } }

    //    public static SSerializeKey2 Empty = new SSerializeKey2(string.Empty);

    //    public SSerializeKey2(string inName)
    //    {
    //        if (string.IsNullOrEmpty(inName))
    //        {
    //            _name = string.Empty;
    //            _hash = 0;
    //            _inited = false;
    //        }
    //        else
    //        {
    //            _name = inName;
    //            _hash = _name.GetHashCode();
    //            _inited = true;
    //        }
    //    }

    //    public SSerializeKey2(NamedId inName)
    //    {
    //        _name = inName.name;
    //        _hash = _name.GetHashCode();
    //        _inited = true;
    //    }

    //    public override string ToString()
    //    {
    //        return _name;
    //    }

    //    public override int GetHashCode()
    //    {
    //        return _hash;
    //    }

    //    public override bool Equals(object obj)
    //    {
    //        if (ReferenceEquals(null, obj)) return false;
    //        if (obj.GetType() != this.GetType()) return false;
    //        return Equals((SSerializeKey2)obj);
    //    }

    //    public bool Equals(in SSerializeKey2 other)
    //    {
    //        return _hash == other._hash;
    //    }

    //    public static bool operator ==(in SSerializeKey2 left, in SSerializeKey2 right)
    //    {
    //        return left.Equals(right);
    //    }

    //    public static bool operator !=(in SSerializeKey2 left, in SSerializeKey2 right)
    //    {
    //        return !left.Equals(right);
    //    }

    //    public class CRecordKeyComparer : IEqualityComparer<SSerializeKey2>
    //    {
    //        public bool Equals(SSerializeKey2 x, SSerializeKey2 y) { return x == y; }
    //        public int GetHashCode(SSerializeKey2 obj) { return obj.GetHashCode(); }
    //    }

    //    public static CRecordKeyComparer Comparer = new CRecordKeyComparer();
    //}
}