﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public interface IGUIElem
{
    string GetText();
    void SetText(string inText);
    void AddListener(UnityAction call);
    void RemoveListener(UnityAction call);
}

public static class CGUIElemFactory
{
    public static IGUIElem Create(Transform inTr)
    {
        {
            TMP_InputField comp = inTr.GetComponent<TMP_InputField>();
            if (comp != null)
                return new CGUIElem_InputFieldTMP(comp);
        }

        {
            Button comp = inTr.GetComponent<Button>();
            if (comp != null)
                return new CGUIElem_Btn(comp);
        }

        return null;
    }
}

public class CGUIElem_InputFieldTMP : IGUIElem
{
    private TMP_InputField _comp;

    public CGUIElem_InputFieldTMP(TMP_InputField inComp)
    {
        _comp = inComp;
    }

    public string GetText() { return _comp.text; }
    public void SetText(string inText) { _comp.text = inText; }

    public void AddListener(UnityAction call)
    {
        LogContext.WriteToLog(ELogLevel.ERROR, $"Cant add listener for {_comp.transform.name}");
    }
    public void RemoveListener(UnityAction call)
    {
        
    }
}

public class CGUIElem_Btn : IGUIElem
{
    private Button _comp;
    private TMP_Text _comp2;

    public CGUIElem_Btn(Button inComp)
    {
        _comp = inComp;
        _comp2 = _comp.transform.GetComponentInChildren<TMP_Text>();
    }

    public string GetText() { return _comp2.text; }
    public void SetText(string inText) { _comp2.text = inText; }

    public void AddListener(UnityAction call)
    {
        _comp.onClick.AddListener(call);
    }
    public void RemoveListener(UnityAction call)
    {
        _comp.onClick.RemoveListener(call);
    }
}