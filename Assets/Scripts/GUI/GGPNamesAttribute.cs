﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using GGP;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class GGPNamesAttribute : PropertyAttribute
{
    public abstract Type GetNamesClass();
}

public class GGPNamesCommonAttribute : GGPNamesAttribute
{
    public override Type GetNamesClass() { return typeof(GGPNamesCommon); }
}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(GGPNamesAttribute), true)]
public class GGPNamesPropertyDrawer : PropertyDrawer
{
    private int GetPropertyIndex(SerializedProperty property)
    {
        //property.displayName ////Element 1
        //property.propertyPath ////Keys.Array.data[1]
        int space_index = property.displayName.LastIndexOf(' ');
        string last_digitals = property.displayName.Substring(space_index + 1);
        if (!int.TryParse(last_digitals, out int index))
        {
            index = -1;
            Debug.LogError($"Unexpected property.displayName {property.displayName}");
        }
        return index;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        property.serializedObject.Update();

        int property_index = GetPropertyIndex(property);
        if (property_index == -1)
            return;

        GUIInputFieldGGP input_filed = property.serializedObject.targetObject as GUIInputFieldGGP;
        if (input_filed == null)
            return;

        string selected_name = input_filed.Keys[property_index];

        GGPNamesAttribute at = attribute as GGPNamesAttribute;

        Type ggp_names_type = at.GetNamesClass();
        FieldInfo[] minfos = ggp_names_type.GetFields();

        List<string> strnames = new List<string>();
        List<NamedId> names = new List<NamedId>();
        int index_value = -1;
        for (int i = 0; i < minfos.Length; i++)
        {
            FieldInfo mi = minfos[i];
            if (mi.FieldType == typeof(NamedId))
            {
                NamedId v = (NamedId)mi.GetValue(ggp_names_type);
                strnames.Add(v.name);
                names.Add(v);

                if (v.name == selected_name)
                    index_value = i;
            }
        }

        int new_index = EditorGUI.Popup(position, index_value, strnames.ToArray());

        if (new_index != index_value)
            input_filed.Keys[property_index] = strnames[new_index];

        property.serializedObject.ApplyModifiedProperties();

        EditorGUI.EndProperty();
    }
}

#endif