﻿using GGP;
using TMPro;
using UnityEngine;


[RequireComponent(typeof(TMP_InputField))]
public class GUIInputFieldGGP : MonoBehaviour
{
    TMP_InputField _input_field;

    CGGPManager _ggp_manager;

    [SerializeField]
    [GGPNamesCommonAttribute()]
    public string[] Keys;

    private SGGPProvider _provider;

    private TMP_InputField GetInputFiled()
    {
        if (_input_field == null)
            _input_field = transform.GetComponent<TMP_InputField>();
        return _input_field;
    }

    private void Awake()
    {
        FindGGPManager();

        if (_ggp_manager != null)
        {
            NamedId[] nms = new NamedId[Keys.Length];
            for (int i = 0; i < Keys.Length; i++)
                nms[i] = NamedId.GetNamedId(Keys[i]);

            _provider = _ggp_manager.GetProvider(transform.name, nms);
        }
            //Keys

        GetInputFiled().onEndEdit.AddListener(OnInputTextChange);
    }

    public void OnInputTextChange(string inText)
    {
        if(_input_field.contentType == TMP_InputField.ContentType.IntegerNumber)
        {
            if (int.TryParse(inText, out int v))
                _provider.Set(v);
        }
        else if (_input_field.contentType == TMP_InputField.ContentType.DecimalNumber)
        {
            if (float.TryParse(inText, out float v))
                _provider.Set(v);
        }
        else
            _provider.Set(inText);
    }

    private void FindGGPManager()
    {
        Transform tr = transform;
        while (tr != null && tr.GetComponent<GGPHostScript>() == null)
            tr = tr.parent;

        if (tr != null)
            _ggp_manager = tr.GetComponent<GGPHostScript>().GetGGPManager();
    }
}
