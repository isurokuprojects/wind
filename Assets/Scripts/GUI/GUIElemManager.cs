using GGP;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class GUIElemManager : MonoBehaviour
{
    Dictionary<string, IGUIElem> _elems;

    private void Awake()
    {
        _elems = new Dictionary<string, IGUIElem>();
        transform.FindChildrenBySpecName(tr =>
        {
            IGUIElem elem = CGUIElemFactory.Create(tr);
            if (elem != null)
                _elems.Add(tr.name.Trim(), elem);
        });
    }

    private void OnDestroy()
    {
        _elems.Clear();
    }

    public string GetText(string inName)
    {
        if (!_elems.TryGetValue(inName, out IGUIElem elem))
            return string.Empty;

        string txt = elem.GetText();
        return txt;
    }

    public void SetText(string inName, string inText)
    {
        if (!_elems.TryGetValue(inName, out IGUIElem elem))
            return;

        elem.SetText(inText);
    }

    public void AddListener(string inName, UnityAction call)
    {
        if (!_elems.TryGetValue(inName, out IGUIElem elem))
            return;

        elem.AddListener(call);
    }

    public void RemoveListener(string inName, UnityAction call)
    {
        if (!_elems.TryGetValue(inName, out IGUIElem elem))
            return;

        elem.RemoveListener(call);
    }
}
