// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Vertex Colored" 
{
	//show values to edit in inspector
	Properties
	{
		[PerRendererData] _Color("Color", Color) = (0, 0, 0, 1)
	}

	SubShader
	{
		//the material is completely non-transparent and is rendered at the same time as the other opaque geometry
		//Tags{ "RenderType" = "Opaque" "Queue" = "Geometry" "LightMode" = "ForwardBase"}

		Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" "LightMode" = "ForwardBase"}

		Blend SrcAlpha OneMinusSrcAlpha
		//ZWrite off


		Pass
		{
			CGPROGRAM
			//allow instancing
			#pragma multi_compile_instancing

			//shader functions
			#pragma vertex vert
			#pragma fragment frag

			//use unity shader library
			#include "UnityCG.cginc" // for UnityObjectToWorldNormal
			#include "UnityLightingCommon.cginc" // for _LightColor0

			// compile shader into multiple variants, with and without shadows
			// (we don't care about any lightmaps yet, so skip these variants)
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			// shadow helper functions and macros
			#include "AutoLight.cginc"

			/*struct appdata_base 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};*/

			//per vertex data that gets passed from the vertex to the fragment function
			struct v2f 
			{
				SHADOW_COORDS(1) // put shadows data into TEXCOORD1
				float4 pos		: SV_POSITION;
				fixed4 diff		: COLOR0; // diffuse lighting color
				fixed3 ambient	: COLOR1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			UNITY_INSTANCING_BUFFER_START(Props)
			UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
			UNITY_INSTANCING_BUFFER_END(Props)

			v2f vert(appdata_base v)
			{
				v2f o;

				//setup instance id
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				//calculate the position in clip space to render the object
				o.pos = UnityObjectToClipPos(v.vertex);

				// get vertex normal in world space
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				// dot product between normal and light direction for
				// standard diffuse (Lambert) lighting
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				// factor in the light color
				o.diff = nl * _LightColor0;

				// in addition to the diffuse lighting from the main light,
				// add illumination from ambient or light probes
				// ShadeSH9 function from UnityCG.cginc evaluates it,
				// using world space normal
				o.ambient = ShadeSH9(half4(worldNormal, 1));

				// compute shadows data
				TRANSFER_SHADOW(o);

				return o;
			}

			fixed4 frag(v2f i) : SV_TARGET
			{
				//setup instance id
				UNITY_SETUP_INSTANCE_ID(i);
				//get _Color Property from buffer
				fixed4 color = UNITY_ACCESS_INSTANCED_PROP(Props, _Color);

				// compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
				fixed shadow = SHADOW_ATTENUATION(i);
				// darken light's illumination with shadow, keep ambient intact
				fixed3 lighting = i.diff * shadow + i.ambient;
				color.rgb *= lighting;

				//Return the color the Object is rendered in
				return color;
			}
			ENDCG
		}

		// pull in shadow caster from VertexLit built-in shader
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}