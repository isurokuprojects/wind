﻿using Primitives3D;
using System;
using Unity.Collections;
using UnityEngine;

public interface IWindGridOwner
{
    float GetBarometricPressure(float inHeght);
}

public class CWindGrid: IDisposable
{
    private IWindGridOwner _owner;

    private int _sample_count_per_cell;
    private int _xz_cell_count;
    private int _y_cell_count;

    private Vector3 _terr_cell_size;
    private SBoundBox3D _terr_bound;

    private NativeArray<SWindElement> _winds;

    int PlaneCellCount => _xz_cell_count * _xz_cell_count;
    int CellCount => PlaneCellCount * _y_cell_count;

    public CWindGrid(IWindGridOwner owner)
    {
        _owner = owner;
    }

    public void Dispose()
    {
        WindDispose();
    }

    void WindDispose()
    {
        if (_winds.IsCreated)
            _winds.Dispose();
    }

    public void CreateNewGrid(TerrainData inTerrainData, int inDivisionCount, int inYCellCount, float inHeght)
    {

        _sample_count_per_cell = inTerrainData.heightmapResolution - 1; //513 - 1 - количество ячеек террейна между семплами
        _sample_count_per_cell >>= inDivisionCount; //делим на 2 несколько раз - количество семплоа террейна на одну ячейку
        _xz_cell_count = 1 << inDivisionCount;
        _y_cell_count = inYCellCount;

        _terr_bound = new SBoundBox3D(inTerrainData.bounds);

        if(inHeght > _terr_bound.Size.y)
        {
            float diff = inHeght - _terr_bound.Size.y;

            Vector3 origin = _terr_bound.Origin;
            Vector3 hs = _terr_bound.HalfSizes;

            origin.y = origin.y + diff / 2;
            hs.y = hs.y + diff / 2;

            _terr_bound = new SBoundBox3D(origin, hs);
        }

        _terr_cell_size = new Vector3(
            _terr_bound.Size.x / _xz_cell_count,
            inHeght / _y_cell_count,
            _terr_bound.Size.z / _xz_cell_count);

        WindDispose();

        _winds = new NativeArray<SWindElement>(CellCount, Allocator.Persistent);

        for (int i = 0; i < _winds.Length; i++)
        {
            Vector3Int cell = GetWindElemCellByIndex(i);
            SBoundBox3D aabb = GetWindElemBoundByCell(cell);
            SWindElement el = _winds[i];
            float p = _owner.GetBarometricPressure(aabb.Origin.y);
            el.Init(cell, aabb, p);
            _winds[i] = el;
        }
    }

    private Vector3Int GetWindElemCellByIndex(int index)
    {
        int y = index / PlaneCellCount;
        int plane_tail = index - y * PlaneCellCount;
        int z = plane_tail / _xz_cell_count;
        int x = plane_tail - z * _xz_cell_count;
        return new Vector3Int(x, y, z);
    }

    private SBoundBox3D GetWindElemBoundByCell(Vector3Int inCell)
    {
        Vector3 p = new Vector3(_terr_cell_size.x * inCell.x, _terr_cell_size.y * inCell.y, _terr_cell_size.z * inCell.z);
        return new SBoundBox3D(p + _terr_cell_size / 2, _terr_cell_size / 2);
    }


}
