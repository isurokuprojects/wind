﻿
using Primitives3D;
using UnityEngine;

public struct SWindElement
{
    private Vector3Int _cell;
    private SBoundBox3D _aabb;
    private float _heght_pressure;

    public Vector3Int Cell => _cell;
    public SBoundBox3D AABB => _aabb;
    public float HeightPressure => _heght_pressure;

    public void Init(Vector3Int inCell, SBoundBox3D inAABB, float inHeightPressure)
    {
        _cell = inCell;
        _aabb = inAABB;
        _heght_pressure = inHeightPressure;
    }
}

