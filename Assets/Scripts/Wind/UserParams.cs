﻿
public struct SUserParams
{
    public int DivisionCount;
    public int YCellCount;
    public float WindHeight;

    public int XDrawCellIndex;
    public int YDrawCellIndex;
    public int ZDrawCellIndex;
}

