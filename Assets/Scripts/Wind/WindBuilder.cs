﻿using GGP;
using Primitives3D;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Collections;
using UnityEngine;

public class WindBuilder : MonoBehaviour, IWindGridOwner
{
    [SerializeField]
    private Terrain _terrain;

    [SerializeField]
    private GUIElemManager _GUI;

    [SerializeField]
    private OBBMesh _obb_mesh_prefab;

    [SerializeField]
    private Color MinPressureColor;
    [SerializeField]
    private Color MaxPressureColor;

    [SerializeField]
    private float PressureZero = 101325;//давление на уровне 0 Па
    [SerializeField]
    private float PhysGravity = -9.81f; //м/c^2
    [SerializeField]
    private float MolarMass = 0.0289644f; //молярная масса газаж воздух 0.0289644 kg/mol
    [SerializeField]
    private float Temperature = 288.15f; //температура в Кельвинах
    [SerializeField]
    private float HeightFactor = 1000f; //температура в Кельвинах
    private const float R = 8.3144598f; //униаерс. газовая постоянная Дж/(моль∙К).

    private LineRenderer _lrenderer;

    private CWindGrid _grid;

    private OBBMesh[] _wind_obbs;

    private SBoundBox3D _terr_bound;
    private Vector3 _terr_cell_size;

    private const string UserParamFileName = "UserParams.json";
    private SUserParams _user_params;

    CGGPManager _ggp_manager;
    SGGPReader _ggp_gui_div_count;

    private void Awake()
    {
        _ggp_manager = GetComponent<GGPHostScript>().GetGGPManager();

        _ggp_gui_div_count = _ggp_manager.GetReader("WindBuilder", GGPNamesCommon.TerrainDivision);

        _lrenderer = GetComponent<LineRenderer>();
        _terr_bound = new SBoundBox3D(_terrain.terrainData.bounds);

        if (!ReadUserParams())
        {
            _user_params.DivisionCount = 3;
            _user_params.YCellCount = 2;
            //_user_params.CellZCount = 3;
            _user_params.XDrawCellIndex = 0;
            _user_params.YDrawCellIndex = -1;
            _user_params.ZDrawCellIndex = -1;
        }
    }

    private void OnDisable()
    {
        if (_grid != null)
        {
            _grid.Dispose();
            _grid = null;
        }
    }

    private void OnDestroy()
    {
        if (_grid != null)
        {
            _grid.Dispose();
            _grid = null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _GUI.AddListener("$CreateButton", OnCreateNewGrid);
        _GUI.SetText("$XCellCount", _user_params.DivisionCount.ToString());
        _GUI.SetText("$YCellCount", _user_params.YCellCount.ToString());
        //_GUI.SetText("$ZCellCount", _user_params.CellZCount.ToString());

        _GUI.AddListener("$DrawButton", OnDrawGrid);
        _GUI.SetText("$XDrawCellIndex", _user_params.XDrawCellIndex.ToString());
        _GUI.SetText("$YDrawCellIndex", _user_params.YDrawCellIndex.ToString());
        _GUI.SetText("$ZDrawCellIndex", _user_params.ZDrawCellIndex.ToString());
    }

    public float GetBarometricPressure(float inH)
    {
        float h = inH * HeightFactor;
        float P = PressureZero * Mathf.Exp((PhysGravity * MolarMass * h) / (Temperature * R));
        return P;
    }

    private bool ReadCellCountFromGUI()
    {
        if (!int.TryParse(_GUI.GetText("$XCellCount"), out _user_params.DivisionCount) || _user_params.DivisionCount <= 0)
        {
            LogContext.WriteToLog(ELogLevel.ERROR, "Input value in XCellCount wrong");
            return false;
        }

        if (!int.TryParse(_GUI.GetText("$YCellCount"), out _user_params.YCellCount) || _user_params.YCellCount <= 0)
        {
            LogContext.WriteToLog(ELogLevel.ERROR, "Input value in YCellCount wrong");
            return false;
        }

        //NamedId dd = GGPNames.World.Time;

        //if (!int.TryParse(_GUI.GetText("$ZCellCount"), out _user_params.CellZCount) || _user_params.CellZCount <= 0)
        //{
        //    LogContext.WriteToLog(ELogLevel.ERROR, "Input value in ZCellCount wrong");
        //    return false;
        //}
        WriteUserParams();
        return true;
    }



    public void OnCreateNewGrid()
    {
        if (!ReadCellCountFromGUI())
            return;

        if (_grid != null)
            _grid.Dispose();

        _grid = new CWindGrid(this);
        _grid.CreateNewGrid(_terrain.terrainData, _user_params.DivisionCount, _user_params.YCellCount, _user_params.WindHeight);

        ReallocateOBB();
    }

    private void ReallocateOBB()
    {
        //OBBMesh[] old_obbs = _wind_obbs;

        //_wind_obbs = new OBBMesh[_winds.Length];

        //if (old_obbs != null)
        //{
        //    int cnt = Mathf.Min(_winds.Length, old_obbs.Length);
        //    for (int i = 0; i < cnt; i++)
        //    {
        //        if (old_obbs[i] != null)
        //        {
        //            _wind_obbs[i] = old_obbs[i];
        //            _wind_obbs[i].gameObject.SetActive(false);
        //            old_obbs[i] = null;
        //        }
        //    }

        //    if (old_obbs.Length > _winds.Length)
        //    {
        //        for (int i = 0; i < old_obbs.Length; i++)
        //        {
        //            if (old_obbs[i] != null)
        //            {
        //                Destroy(old_obbs[i].gameObject);
        //                old_obbs[i] = null;
        //            }
        //        }
        //    }
        //}
    }

    Vector3Int GetDrawCellIndices()
    {
        if (!int.TryParse(_GUI.GetText("$XDrawCellIndex"), out _user_params.XDrawCellIndex))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, "Input value in XDrawCellIndex wrong");
        }

        if (!int.TryParse(_GUI.GetText("$YDrawCellIndex"), out _user_params.YDrawCellIndex))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, "Input value in YDrawCellIndex wrong");
        }

        if (!int.TryParse(_GUI.GetText("$ZDrawCellIndex"), out _user_params.ZDrawCellIndex))
        {
            LogContext.WriteToLog(ELogLevel.ERROR, "Input value in ZDrawCellIndex wrong");
        }

        WriteUserParams();
        return new Vector3Int(_user_params.XDrawCellIndex, _user_params.YDrawCellIndex, _user_params.ZDrawCellIndex);
    }

    bool IsCellAccessDraw(Vector3Int inTemplate, Vector3Int inCell)
    {
        return (inCell.x == inTemplate.x || inTemplate.x < 0)
            && (inCell.y == inTemplate.y || inTemplate.y < 0)
            && (inCell.z == inTemplate.z || inTemplate.z < 0);
    }

    public void OnDrawGrid()
    {
        //Vector3Int access_indices = GetDrawCellIndices();
        //for (int i = 0; i < _winds.Length; i++)
        //{
        //    bool access = IsCellAccessDraw(access_indices, _winds[i].Cell);

        //    if (access)
        //    {
        //        Vector3 origin = _winds[i].AABB.Origin;
        //        Vector3 hs = _winds[i].AABB.HalfSizes;

        //        if (_wind_obbs[i] == null)
        //        {
        //            var go = GameObject.Instantiate(_obb_mesh_prefab, origin, Quaternion.identity, _terrain.transform);
        //            OBBMesh obb = go.GetComponent<OBBMesh>();
        //            _wind_obbs[i] = obb;
        //        }
        //        else
        //        {
        //            _wind_obbs[i].gameObject.SetActive(true);
        //        }

        //        _wind_obbs[i].SetOBB(origin, hs);

        //        float t = _winds[i].HeightPressure / PressureZero;
        //        Color c = Color.Lerp(MinPressureColor, MaxPressureColor, t);
        //        _wind_obbs[i].SetColor(c);
        //    }
        //    else if (_wind_obbs[i] != null)
        //    {
        //        _wind_obbs[i].gameObject.SetActive(false);
        //    }
        //}
    }

    public void OnCalcPrev()
    {
        Quaternion lbound_rot = Quaternion.Inverse(transform.rotation);
        Vector3 lbound_origin = lbound_rot * (_terr_bound.Origin - transform.position);

        SBox3D lbox = new SBox3D(lbound_origin, lbound_rot, _terr_bound.HalfSizes);

        SBoundBox3D laabb = lbox.GetBoundBox();

        DrawBound(laabb.GetCornersForLineRenderer(), false);
    }

    private void DrawBound(Vector3[] inPositions, bool inWorldSpace)
    {
        _lrenderer.positionCount = inPositions.Length;
        _lrenderer.SetPositions(inPositions);
        _lrenderer.useWorldSpace = inWorldSpace;
    }

    private bool ReadUserParams()
    {
        bool res = false;
        string fn = Path.Combine(Application.dataPath, UserParamFileName);
        if (File.Exists(fn))
        {
            using (StreamReader reader = File.OpenText(fn))
            {
                string txt = reader.ReadToEnd();
                if (!string.IsNullOrEmpty(txt))
                {
                    try
                    {
                        _user_params = JsonUtility.FromJson<SUserParams>(txt);
                        res = true;
                    }
                    catch (Exception ex)
                    {
                        LogContext.WriteToLog(ELogLevel.EXCEPTION, ex.Message);
                    }
                }
            }
        }
        return res;
    }

    private void WriteUserParams()
    {
        string txt = string.Empty;
        try
        {
            txt = JsonUtility.ToJson(_user_params, true);
            string fn = Path.Combine(Application.dataPath, UserParamFileName);
            using (StreamWriter writer = File.CreateText(fn))
            {
                writer.Write(txt);
            }
        }
        catch (Exception ex)
        {
            LogContext.WriteToLog(ELogLevel.EXCEPTION, ex.Message);
        }
    }

}
