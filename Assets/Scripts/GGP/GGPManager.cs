﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GGP
{
    public interface IGGPManagerOwner
    {
        void AddDelegate(Action<object, UpdateEventArgs> inDelegate);
        void RemoveDelegate(Action<object, UpdateEventArgs> inDelegate);
    }

    public interface IGGPManagerHost
    {
        CGGPManager GetGGPManager();
    }

    public partial class CGGPManager : IGGPReaderOwner, IGGPProviderOwner
    {
        public CGGPManager(string inDebugName, IGGPManagerOwner inOwner)
        {
            _name = inDebugName;
            _owner = inOwner;
        }

        #region Create Release Reader

        public SGGPReader GetReader(string inReaderName, params NamedId[] inKeys)
        {
            return GetReader(inReaderName, new SGGPKey(inKeys));
        }

        public SGGPReader GetReader(string inReaderName, in SGGPKey InParamKey, DOnChangeGGPHandler inChangeHandler = null)
        {
            int index = GetIndexOrCreate(InParamKey);

            ref SParamValueSupport value = ref _values_support[index];

            int reader_index = value.Readers.Add();

            ref SReader reader = ref value.Readers[reader_index];
            reader.Name = inReaderName;
            reader.ChangeHandler = inChangeHandler;

            SyncUpdating(index);

            return new SGGPReader(this, index, reader_index, InParamKey, inReaderName);
        }

        public void AddChangeHandlerForReader(in SGGPCore inCore, DOnChangeGGPHandler inChangeHandler)
        {
            if (!CheckCore(inCore))
                return;

            if (inChangeHandler == null)
            {
                LogContext.WriteToLog(ELogLevel.ERROR, $"ChangeHandler is null! Core: {inCore}");
                return;
            }

            ref SParamValueSupport value_support = ref _values_support[inCore.ValueIndex];
            ref SReader reader = ref value_support.Readers[inCore.RegisterIndex];

            reader.ChangeHandler = inChangeHandler;

            SyncUpdating(inCore.ValueIndex);
        }

        public void GetReport(List<KeyValuePair<SGGPKey, Variant>> outAllValues)
        {
            for(int i = 0; i < _values_support.Count; i++)
            {
                if(!_values_support.IsRemoved(i))
                {
                    RefreshValue(i);
                    outAllValues.Add(new KeyValuePair<SGGPKey, Variant>(_values_support[i].Key, _values[i].Value));
                }
            }
        }

        public void ReleaseReader(in SGGPCore inCore)
        {
            ReleaseCore(inCore, true);
        }

        #endregion //Create Release Reader

        #region Create Release Provider

        public SGGPProvider GetProvider(string inProviderName, params NamedId[] inKeys)
        {
            return GetProvider(inProviderName, new SGGPKey(inKeys));
        }

        public SGGPProvider GetProvider(string inProviderName, in SGGPKey InKey)
        {
            int index = GetIndexOrCreate(InKey);

            ref SParamValueSupport value_support = ref _values_support[index];

            int provider_index = value_support.Providers.Add();

            ref SProvider provider = ref value_support.Providers[provider_index];
            provider.Name = inProviderName;

            ref SParamValue value = ref _values[index];
            value.SetNotFresh();

            //RefreshValue(index); - нет смысла, LazySetter еще не добавлен, просто Set сделать еще не могли - ничего не изменится
            SyncUpdating(index);

            return new SGGPProvider(this, index, provider_index, InKey, inProviderName);
        }

        public void ReleaseProvider(in SGGPCore inCore)
        {
            ReleaseCore(inCore, false);
        }
        #endregion //Create Release Provider

        public ref Variant Get(in SGGPCore inCore)
        {
            if (!CheckCore(inCore))
                return ref Variant.Undefined;

            ref SParamValue value = ref _values[inCore.ValueIndex];
            if (value.OldValue)
            {
                RefreshValue(inCore.ValueIndex);
            }

            if (value.OldValue)
            {
			    ref SParamValueSupport value_support = ref _values_support[inCore.ValueIndex];
			    LogContext.WriteToLog(ELogLevel.ERROR, $"Value {value_support} is old after Refresh!");

                return ref Variant.Undefined;
            }

            return ref value.Value;
        }

        public void Set(in SGGPCore inCore, int inValue)
        {
            Set(inCore, new VariantSmall(inValue));
        }

        public void Set(in SGGPCore inCore, float inValue)
        {
            Set(inCore, new VariantSmall(inValue));
        }

        public void Set(in SGGPCore inCore, bool inValue)
        {
            Set(inCore, new VariantSmall(inValue));
        }

        public void Set(in SGGPCore inCore, NamedId inValue)
        {
            Set(inCore, new VariantSmall(inValue));
        }

        public void Set(in SGGPCore inCore, Vector2 inValue)
        {
            Set(inCore, new VariantSmall(inValue));
        }

        public void Set(in SGGPCore inCore, Vector3 inValue)
        {
            Set(inCore, new VariantSmall(inValue));
        }

        public void Set(in SGGPCore inCore, VariantSmall inValue)
        {
            ref SProvider p = ref GetProviderForSet(inCore, out bool outValidRes, out bool outHasListener);
            if (outValidRes)
            {
                p.Value.Set(inValue);

                if (outHasListener)
                    RefreshValue(inCore.ValueIndex);
            }
        }

        public void Set(in SGGPCore inCore, string inValue)
        {
            ref SProvider p = ref GetProviderForSet(inCore, out bool outValidRes, out bool outHasListener);
            if (outValidRes)
            {
                p.Value.Set(inValue);

                if (outHasListener)
                    RefreshValue(inCore.ValueIndex);
            }
        }

        public void AddSetter(in SGGPCore inCore, DGGPSetter inSetter)
        {
            if (inSetter == null)
            {
			    LogContext.WriteToLog(ELogLevel.ERROR, $"Setter is null! Core: {inCore}");
                return;
            }

            ref SProvider p = ref GetProviderForSet(inCore, out bool outValidRes, out bool outHasListener);
            if (outValidRes)
            {
                p.LazySetter = inSetter;

                ref SParamValue value = ref _values[inCore.ValueIndex];
                value.SetNotFresh();

                SyncUpdating(inCore.ValueIndex);
            }
        }

        public bool IsSetterPresent(in SGGPCore inCore)
        {
            if (!CheckCore(inCore))
                return false;

            ref SParamValueSupport value_support = ref _values_support[inCore.ValueIndex];
            ref SProvider provider = ref value_support.Providers[inCore.RegisterIndex];
            return provider.LazySetter != null;
        }

        public bool IsReadersPresent(in SGGPCore inCore)
        {
            if (!CheckCore(inCore))
                return false;

            ref SParamValueSupport value_support = ref _values_support[inCore.ValueIndex];
            return value_support.Readers.Count > 0;
        }

        #region GameEditor
        public string GetParamsText()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < _values_support.Count; i++)
            {
                sb.Append(_values_support[i].Key.ToString());
            }

            return sb.ToString();
        }

        #endregion


        /// ------------------- Private ---------------

        private int GetIndexOrCreate(in SGGPKey InParamKey)
        {
            if (!_dicKeyToStoreIndex.TryGetValue(InParamKey, out int index))
            {
                index = _values_support.Add();
                ref SParamValueSupport value_support = ref _values_support[index];

                if (value_support.Readers == null)
                    value_support.Readers = new CStructStore<SReader>(5);

                if (value_support.Providers == null)
                    value_support.Providers = new CStructStore<SProvider>(2);

                value_support.Key = InParamKey;
                _dicKeyToStoreIndex.Add(InParamKey, index);

                int val_index = _values.Add();

                if (val_index != index)
                {
				    LogContext.WriteToLog(ELogLevel.ERROR, "Value is Undefined!");
                    return -1;
                }

                ref SParamValue value = ref _values[index];
                value.Value = Variant.Undefined;
            }
            return index;
        }

        private bool CheckCore(in SGGPCore inCore)
        {
            if (!LogContext.Assert(inCore.IsValid(), "Core not valid!"))
                return false;

            ref SParamValueSupport value_support = ref _values_support[inCore.ValueIndex];
            bool eq = value_support.Key == inCore.Key;
#if DEBUG
            LogContext.Assert(eq, $"Core key {inCore.Key} not equal Value key {value_support.Key}!");
#endif
            return eq;
        }

        private void ReleaseCore(in SGGPCore inCore, bool inReader)
        {
            if (!CheckCore(inCore))
                return;

            ref SParamValueSupport value_support = ref _values_support[inCore.ValueIndex];

            string internal_name;
            if (inReader)
            {
                ref SReader r = ref value_support.Readers[inCore.RegisterIndex];
                internal_name = r.Name;
            }
            else
            {
                ref SProvider r = ref value_support.Providers[inCore.RegisterIndex];
                internal_name = r.Name;
            }

            if (internal_name != inCore.Name)
            {
#if DEBUG
			    LogContext.WriteToLog(ELogLevel.ERROR, $"Core name {inCore.Name} not equal Value name {internal_name}!");
#endif
                return;
            }

            if (inReader)
            {
                value_support.Readers.Remove(inCore.RegisterIndex);
            }
            else
            {
                value_support.Providers.Remove(inCore.RegisterIndex);

                ref SParamValue value = ref _values[inCore.ValueIndex];
                value.SetNotFresh();
            }

            SyncUpdating(inCore.ValueIndex);
        }

        private void RefreshValue(int inValueIndex)
        {
            ref SParamValue value = ref _values[inValueIndex];
            ref SParamValueSupport value_support = ref _values_support[inValueIndex];
            Variant prev = value.Value;
            value_support.FreshValue(ref value.Value);
            bool need_update = value_support.IsNeedUpdated();
            if (need_update)
                value.SetFrameFresh();
            else
                value.SetPersistanceFresh();

            //значение должно быть свежим
            if (!value.OldValue)
            {
                //разница должна быть больше, чем самая маленькая из всех слушателей
                if (!Variant.Approximately(prev, value.Value))
                {
                    using (CAdvancedList<DOnChangeGGPHandler> readers_cache = ListPool<DOnChangeGGPHandler>.Get())
                    {
                        for (int i = 0; i < value_support.Readers.Count; i++)
                        {
                            if (!value_support.Readers.IsRemoved(i) //не удален
                                && value_support.Readers[i].ChangeHandler != null) //есать слушатель
                            {
                                readers_cache.Add(value_support.Readers[i].ChangeHandler);
                            }
                        }

                        for (int i = 0; i < readers_cache.Count; i++)
                        {
                            readers_cache[i].Invoke(value_support.Key, ref prev, ref value.Value);
                        }
                        readers_cache.Clear();
                    }
                }
            }
        }

        private ref SProvider GetProviderForSet(in SGGPCore inCore, out bool outValidRes, out bool outHasListener)
        {
            outValidRes = false;
            outHasListener = false;

            if (!CheckCore(inCore))
                return ref SProvider.Undefined;


            ref SParamValueSupport value_support = ref _values_support[inCore.ValueIndex];

            outHasListener = value_support.IsHasListener();

            ref SProvider p = ref value_support.Providers[inCore.RegisterIndex];

            if (p.LazySetter != null)
            {
#if DEBUG
                LogContext.WriteToLog(ELogLevel.ERROR, $"Provider {inCore} hasn't Setter!");
#endif
                return ref SProvider.Undefined;
            }

            outValidRes = true;

            ref SParamValue value = ref _values[inCore.ValueIndex];
            value.SetNotFresh();

            return ref p;
        }

        private void SyncUpdating(int inIndex)
        {
            ref SParamValueSupport value_support = ref _values_support[inIndex];

            bool has_listener = value_support.IsHasListener();
            bool has_lazy_setter = value_support.IsSomeProvidersHasLazySetters();
            if (has_listener && has_lazy_setter)
            {
                //если добавился (т.е. новый) и в списке стал первым (т.е. до него никого не было)
                if (_listener_indices.Add(inIndex) && _listener_indices.Count == 1)
                {
                    _owner.AddDelegate(Update);
#if DEBUG
                    LogContext.WriteToLog(ELogLevel.DEBUG, $"GGPManager {_name} added to update!");
#endif
                }
            }
            else
            {
                //если удалился и никого не стало
                if (_listener_indices.Remove(inIndex) && _listener_indices.Count == 0)
                {
                    _owner.RemoveDelegate(Update);
#if DEBUG
                    LogContext.WriteToLog(ELogLevel.DEBUG, $"GGPManager {_name} removed from update!");
#endif
                }
            }
        }

        private void Update(object sender, UpdateEventArgs e)
        {
            for (int i = 0; i < _listener_indices.Count; i++)
            {
                int index = _listener_indices.GetKeyByIndex(i);
                RefreshValue(index);
            }
        }

        private string _name;
        private IGGPManagerOwner _owner;

        private const int StartCapacity = 2; //test - 200
        private Dictionary<SGGPKey, int> _dicKeyToStoreIndex = new Dictionary<SGGPKey, int>(SGGPKey.Comparer);
        private CStructStore<SParamValueSupport> _values_support = new CStructStore<SParamValueSupport>(StartCapacity);
        private CStructStore<SParamValue> _values = new CStructStore<SParamValue>(StartCapacity);
        private CHashList<int> _listener_indices = new CHashList<int>();
    }
}
