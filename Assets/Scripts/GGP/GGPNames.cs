﻿
using System.Collections;
using System.Collections.Generic;

namespace GGP
{
    public static class GGPNamesCommon
    {
        public static NamedId TerrainDivision = NamedId.GetNamedId(nameof(TerrainDivision));
        public static NamedId Test1 = NamedId.GetNamedId($"{nameof(Test1)}");
        public static NamedId Test2 = NamedId.GetNamedId($"{nameof(Test2)}");

    }
}