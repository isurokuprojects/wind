﻿
namespace GGP
{
    public readonly struct SGGPCore
    {
        private readonly bool _inited;
        private readonly SGGPKey _key;
        public SGGPKey Key { get { return _key; } }

        private readonly int _value_index;
        public int ValueIndex { get { return _value_index; } }

        private readonly int _register_index;
        public int RegisterIndex { get { return _register_index; } }

        private readonly string _name;
        public string Name { get { return _name; } }


        public SGGPCore(int inValueIndex, int inReaderIndex, in SGGPKey inKey, string inName)
        {
            _value_index = inValueIndex;
            _register_index = inReaderIndex;
            _key = inKey;
            _name = inName;
            _inited = true;
        }

        public override string ToString()
        {
            if (!_inited)
                return "Invalid";
            return $"{_name}({ValueIndex}:{RegisterIndex})[Key:{Key}]";
        }

        public bool IsValid()
        {
            return _inited;
        }
    }
}
