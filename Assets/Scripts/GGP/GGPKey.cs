﻿using System.Collections.Generic;

namespace GGP
{
    public readonly struct SGGPKey
    {
        private readonly uint _hash;
        private readonly string _route;

        public SGGPKey(params NamedId[] inSubKeys)
        {
            _hash = 0;
            _route = string.Empty;

            if (!LogContext.Assert(inSubKeys.Length > 0, "inSubKeys Length invalid!", "Length", new Variant(inSubKeys.Length)))
                return;

            _hash = inSubKeys[0].id;
            for (int i = 1; i < inSubKeys.Length; i++)
            {
                _hash = HashFunctions.HashCombine(_hash, inSubKeys[i].id);
            }

            _route = GGPKeyRegistry.Registry(_hash, inSubKeys);
        }

        public override int GetHashCode()
        {
            return (int)_hash;
        }

        public bool Equals(in SGGPKey other)
        {
            return _hash == other._hash;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is SGGPKey && Equals((SGGPKey)obj);
        }

        public static bool operator ==(in SGGPKey left, in SGGPKey right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(in SGGPKey left, in SGGPKey right)
        {
            return !left.Equals(right);
        }

        public override string ToString()
        {
            return _route;
        }

        public class CGGKeyComparer : IEqualityComparer<SGGPKey>
        {
            public bool Equals(SGGPKey x, SGGPKey y) { return x._hash == y._hash; }
            public int GetHashCode(SGGPKey obj) { return obj.GetHashCode(); }
        }

        public static CGGKeyComparer Comparer = new CGGKeyComparer();
    }
}