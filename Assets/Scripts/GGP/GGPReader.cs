﻿
using UnityEngine;

namespace GGP
{
    public delegate void DOnChangeGGPHandler(in SGGPKey inKey, ref Variant inPrevValue, ref Variant inCurrValue);

    public interface IGGPReaderOwner
    {
        SGGPReader GetReader(string inReaderName, params NamedId[] inKeys);
        SGGPReader GetReader(string inReaderName, in SGGPKey InKey, DOnChangeGGPHandler inChangeHandler = null);

        ref Variant Get(in SGGPCore inCore);

        void ReleaseReader(in SGGPCore inCore);

        void AddChangeHandlerForReader(in SGGPCore inCore, DOnChangeGGPHandler inChangeHandler);

        bool IsReadersPresent(in SGGPCore inCore);
    }

    public readonly struct SGGPReader
    {
        private readonly SGGPCore _core;
        public SGGPCore Core { get { return _core; } }

        private readonly IGGPReaderOwner _manager;

        public SGGPReader(IGGPReaderOwner inManager, int inValueIndex, int inReaderIndex, in SGGPKey inParamKey, string inReaderName)
        {
            _manager = inManager;
            _core = new SGGPCore(inValueIndex, inReaderIndex, inParamKey, inReaderName);
        }

        public SGGPReader Dispose()
        {
            if (_core.IsValid())
            {
                _manager.ReleaseReader(_core);
                //Core.Dispose();
            }
            return new SGGPReader();
        }

        public bool IsValid()
        {
            return Core.IsValid() && _manager != null;
        }

        public void AddChangeHandlerForReader(DOnChangeGGPHandler inChangeHandler)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return;
            }

            _manager.AddChangeHandlerForReader(Core, inChangeHandler);
        }


        public float ToInt(int inDefault = 0)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return inDefault;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return inDefault;
            }

            float value = val.ToInt(inDefault);
            return value;
        }

        public float ToFloat(float inDefault = 0)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return inDefault;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return inDefault;
            }

            float value = val.ToFloat(inDefault);
            return value;
        }

        public bool ToBool(bool inDefault = false)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return inDefault;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return inDefault;
            }

            bool value = val.ToBool();
            return value;
        }

        public NamedId ToNamedId()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return NamedId.Empty;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return NamedId.Empty;
            }

            NamedId value = val.ToNamedId();
            return value;
        }

        public Vector3 ToVector3()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return Vector3.zero;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return Vector3.zero;
            }

            Vector3 value = val.ToVector3();
            return value;
        }

        public Vector2 ToVector2()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return Vector2.zero;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return Vector2.zero;
            }

            Vector2 value = val.ToVector2();
            return value;
        }

        public Quaternion ToQuaternion()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return Quaternion.identity;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return Quaternion.identity;
            }

            Quaternion value = val.ToQuaternion();
            return value;
        }

        public RVector ToRVector()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return RVector.zero;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return RVector.zero;
            }

            RVector value = val.ToRVector();
            return value;
        }

        public VariantSmall ToVariantSmall()
        {
            if (!IsValid())
            {
                return VariantSmall.Undefined;
            }

            ref Variant val = ref _manager.Get(Core);
            VariantSmall value = val.ToVariantSmall();
            return value;
        }
    }
}
