﻿
using System;
using System.Text;
using UnityEngine;

namespace GGP
{
    public partial class CGGPManager
    {
        private struct SReader
        {
            public string Name;
            public DOnChangeGGPHandler ChangeHandler;

            public static SReader Undefined = new SReader();

            public override string ToString()
            {
                if (ChangeHandler == null)
                    return Name;
                return $"{Name}; with change handler";
            }

            public void GetDetails(StringBuilder sb)
            {
                if (string.IsNullOrEmpty(Name))
                    sb.Append(Name);
                else
                    sb.Append("Undefined");

                if (ChangeHandler != null)
                    sb.Append($", listener {ChangeHandler.Method.Name} in {ChangeHandler.Target}");
            }
        }

        private struct SProvider
        {
            public string Name;
            public Variant Value;
            public DGGPSetter LazySetter;

            public static SProvider Undefined = new SProvider();

            public override string ToString()
            {
                string s = LazySetter != null ? "; has setter" : string.Empty;
                return $"P:{Name}; Value:{Value}{s}";
            }
        }

        private struct SParamValue
        {
            public Variant Value;
            private int _frame;

            public void SetFrameFresh()
            {
                if (_frame >= 0)
                    _frame = Time.frameCount;
            }

            public void SetPersistanceFresh() { _frame = -1; }
            public void SetNotFresh() { _frame = 0; }

            public bool OldValue { get { return _frame >= 0 && _frame != Time.frameCount; } }

            public override string ToString()
            {
                string s = OldValue ? "[old]" : string.Empty;
                return $"{Value}{s}";
            }
        }

        struct SParamValueSupport
        {
            public SGGPKey Key;

            public CStructStore<SReader> Readers;
            public CStructStore<SProvider> Providers;

            public override string ToString()
            {
                return $"{Key}[RCount: {Readers.Count}; PCount: {Providers.Count}]";
            }

            public bool IsNeedUpdated()
            {
                if (Providers.Count == 0)
                    return false;

                bool need_update = false;
                for (int i = 0; i < Providers.Count && !need_update; i++)
                {
                    if (Providers.IsRemoved(i))
                        continue;

                    ref SProvider p = ref Providers[i];
                    need_update = p.LazySetter != null;
                }
                return need_update;
            }

            public bool IsHasListener()
            {
                if (Readers.Count == 0)
                    return false;

                bool listener_found = false;
                for (int i = 0; i < Readers.Count && !listener_found; i++)
                {
                    if (Readers.IsRemoved(i))
                        continue;

                    ref SReader p = ref Readers[i];
                    listener_found = p.ChangeHandler != null;
                }
                return listener_found;
            }

            //какой-то провайдер ставит не сам, а нужно вызвать его LazySetter
            //т.е. он сам не обновит слушателей
            //а значит надо апдейтить листенеров
            public bool IsSomeProvidersHasLazySetters()
            {
                if (Providers.Count == 0)
                    return false;

                bool func_found = false;
                for (int i = 0; i < Providers.Count && !func_found; i++)
                {
                    if (Providers.IsRemoved(i))
                        continue;

                    ref SProvider p = ref Providers[i];
                    func_found = p.LazySetter != null;
                }
                return func_found;
            }

            public bool FreshValue(ref Variant Sum)
            {
                Sum = Variant.Undefined;

                if (Providers.Count == 0)
                {
                    return false;
                }

                int first_index = -1;
                for (int i = 0; i < Providers.Count && Sum.IsUndefined; i++)
                {
                    if (Providers.IsRemoved(i))
                        continue;

                    ref SProvider pr = ref Providers[i];
                    if (pr.LazySetter != null)
                    {
                        pr.LazySetter(Key, ref pr.Value);
                    }

                    Sum = pr.Value;
                    if (!Sum.IsUndefined)
                        first_index = i;
                }

                if (first_index == -1)
                    return true;

                for (int i = first_index + 1; i < Providers.Count; i++)
                {
                    if (Providers.IsRemoved(i))
                        continue;

                    ref SProvider pr = ref Providers[i];
                    if (pr.LazySetter != null)
                    {
                        pr.LazySetter(Key, ref pr.Value);
                    }

                    VariantSmall _temp_sum = new VariantSmall();
                    VariantSmall.ESumResult SumResult = VariantSmall.GetSum(Sum.ToVariantSmall(), pr.Value.ToVariantSmall(), ref _temp_sum);
                    Sum.Set(_temp_sum);

#if DEBUG
                    if (SumResult != VariantSmall.ESumResult.Success)
				    {
					    string ssum = Sum.ToString();
					    string sval = pr.ToString();
                        string skey = Key.ToString();
					    LogContext.WriteToLog(ELogLevel.ERROR, $"GetSum is FAIL with {SumResult}. Sum: {ssum}; Provider {sval}! Key {skey}");
				    }
#endif
                }
                return true;
            }
        }
    }
}
