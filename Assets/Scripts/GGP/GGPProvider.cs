﻿using UnityEngine;

namespace GGP
{
    public delegate void DGGPSetter(in SGGPKey inKey, ref Variant outValue);

    public interface IGGPProviderFactory
    {
        SGGPProvider GetProvider(string inProviderName, params NamedId[] inKeys);
        SGGPProvider GetProvider(string inProviderName, in SGGPKey InKey);
        void ReleaseProvider(in SGGPCore inCore);

        bool IsReadersPresent(in SGGPCore inCore);
        bool IsSetterPresent(in SGGPCore inCore);
    }

    public interface IGGPProviderOwner : IGGPProviderFactory
    {
        ref Variant Get(in SGGPCore inCore);

        void Set(in SGGPCore inCore, int inValue);
        void Set(in SGGPCore inCore, float inValue);
        void Set(in SGGPCore inCore, bool inValue);
        void Set(in SGGPCore inCore, NamedId inValue);
        void Set(in SGGPCore inCore, Vector2 inValue);
        void Set(in SGGPCore inCore, Vector3 inValue);
        void Set(in SGGPCore inCore, VariantSmall inValue);
        void Set(in SGGPCore inCore, string inValue);

        void AddSetter(in SGGPCore inCore, DGGPSetter inSetter);
    }

    public readonly struct SGGPProvider
    {
        private readonly SGGPCore _core;
        public SGGPCore Core { get { return _core; } }

        private readonly IGGPProviderOwner _manager;

        public SGGPProvider(IGGPProviderOwner inOwner, int inValueIndex, int inProviderIndex, in SGGPKey inParamKey, string inProviderName)
        {
            _manager = inOwner;
            _core = new SGGPCore(inValueIndex, inProviderIndex, inParamKey, inProviderName);
        }

        public SGGPProvider Dispose()
        {
            if (_core.IsValid())
            {
                _manager.ReleaseProvider(Core);
            }
            return new SGGPProvider();
        }

        public override string ToString()
        {
            ref Variant value = ref _manager.Get(_core);
            string sval = value.ToString();
            return $"{_core.Key}: {sval}";
        }

        public bool IsValid()
        {
            return Core.IsValid() && _manager != null;
        }

        #region Reading
        public float ToInt(int inDefault = 0)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return inDefault;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return inDefault;
            }

            float value = val.ToInt(inDefault);
            return value;
        }

        public float ToFloat(float inDefault = 0)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return inDefault;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return inDefault;
            }

            float value = val.ToFloat(inDefault);
            return value;
        }

        public bool ToBool(bool inDefault = false)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return inDefault;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return inDefault;
            }

            bool value = val.ToBool();
            return value;
        }

        public NamedId ToNamedId()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return NamedId.Empty;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return NamedId.Empty;
            }

            NamedId value = val.ToNamedId();
            return value;
        }

        public Vector3 ToVector3()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return Vector3.zero;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return Vector3.zero;
            }

            Vector3 value = val.ToVector3();
            return value;
        }

        public Vector2 ToVector2()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return Vector2.zero;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return Vector2.zero;
            }

            Vector2 value = val.ToVector2();
            return value;
        }

        public Quaternion ToQuaternion()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return Quaternion.identity;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return Quaternion.identity;
            }

            Quaternion value = val.ToQuaternion();
            return value;
        }

        public RVector ToRVector()
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Reader not valid!");
                return RVector.zero;
            }

            ref Variant val = ref _manager.Get(Core);
            if (val.IsUndefined)
            {
                return RVector.zero;
            }

            RVector value = val.ToRVector();
            return value;
        }

        public VariantSmall ToVariantSmall()
        {
            if (!IsValid())
            {
                return VariantSmall.Undefined;
            }

            ref Variant val = ref _manager.Get(Core);
            VariantSmall value = val.ToVariantSmall();
            return value;
        }
        #endregion Reading

        public void Set(int inValue)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Provider not valid!");
                return;
            }

            _manager.Set(Core, inValue);
        }

        public void Set(float inValue)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Provider not valid!");
                return;
            }

            _manager.Set(Core, inValue);
        }

        public void Set(bool inValue)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Provider not valid!");
                return;
            }

            _manager.Set(Core, inValue);
        }

        public void Set(NamedId inValue)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Provider not valid!");
                return;
            }

            _manager.Set(Core, inValue);
        }

        public void Set(Vector2 inValue)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Provider not valid!");
                return;
            }

            _manager.Set(Core, inValue);
        }

        public void Set(Vector3 inValue)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Provider not valid!");
                return;
            }

            _manager.Set(Core, inValue);
        }

        public void Set(string inValue)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Provider not valid!");
                return;
            }

            _manager.Set(Core, inValue);
        }

        public int Add(int inValue)
        {
            return Add(new VariantSmall(inValue)).ToInt();
        }

        public float Add(float inValue)
        {
            return Add(new VariantSmall(inValue)).ToFloat();
        }

        public VariantSmall Add(VariantSmall inValue)
        {
            if (!IsValid())
            {
                LogContext.WriteToLog(ELogLevel.ERROR, "Provider not valid!");
                return new VariantSmall();
            }

            ref Variant val = ref _manager.Get(Core);

            VariantSmall _tmp_sum = new VariantSmall();
            VariantSmall.ESumResult result = VariantSmall.GetSum(val.ToVariantSmall(), inValue, ref _tmp_sum);

            if (result != VariantSmall.ESumResult.Success)
            {
                LogContext.WriteToLog(ELogLevel.ERROR, $"Failed to sum {val} + {inValue} (error {result})");
                return new VariantSmall();
            }

            _manager.Set(Core, _tmp_sum);

            return _tmp_sum;
        }

        public void AddParamSetter(DGGPSetter inSetter)
        {
            _manager.AddSetter(Core, inSetter);
        }

        public bool IsReadersPresent()
        {
            return _manager.IsReadersPresent(Core);
        }

        public bool IsSetterPresent()
        {
            return _manager.IsSetterPresent(Core);
        }
    }
}
