﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace GGP
{
    public readonly struct SGGKeyRoute
    {
        private readonly NamedId[] _subkeys;
        private readonly string _route;

        public SGGKeyRoute(params NamedId[] inSubKeys)
        {
            LogContext.Assert(inSubKeys.Length > 0, "inSubKeys Length invalid!", "Length", new Variant(inSubKeys.Length));

            _subkeys = new NamedId[inSubKeys.Length];
            Array.Copy(inSubKeys, _subkeys, inSubKeys.Length);

            if (_subkeys.Length > 1)
            {
                var sb = new StringBuilder();
                foreach (var n in _subkeys)
                {
                    bool f = sb.Length == 0;
                    if (f)
                        sb.Append("/");
                    sb.Append(n.name);
                }
                _route = sb.ToString();
            }
            else
                _route = _subkeys[0].name;
        }

        public override string ToString()
        {
            return _route;
        }
    }

    public static class GGPKeyRegistry
    {
        private static Dictionary<uint, SGGKeyRoute> _routes = new Dictionary<uint, SGGKeyRoute>();

        public static string Registry(uint inHash, params NamedId[] inSubKeys)
        {
            if (!_routes.TryGetValue(inHash, out SGGKeyRoute r))
            {
                r = new SGGKeyRoute(inSubKeys);
                _routes.Add(inHash, r);
            }
            return r.ToString();
        }
    }
}
