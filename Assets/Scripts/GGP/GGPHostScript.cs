﻿using GGP;
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Text;

#if UNITY_EDITOR
using UnityEditor;
#endif //UNITY_EDITOR

public class GGPHostScript : MonoBehaviour, IGGPManagerOwner
{
    [SerializeField]
    private GGPHostScript ParentHost;

    CUpdateList _update_list;
    CGGPManager _ggp_manager;

    public CGGPManager GetGGPManager()
    {
        if (ParentHost != null)
            return ParentHost.GetGGPManager();

        CheckGGPManager();
        return _ggp_manager;
    }

    private void CheckGGPManager()
    {
        if (_ggp_manager == null)
            _ggp_manager = new CGGPManager($"GameObject_{name}_Script", this);
    }

    private void CheckUpdateList()
    {
        if (_update_list == null)
            _update_list = new CUpdateList(this);
    }

    void Update()
    {
        if (ParentHost != null)
            return;

        CheckUpdateList();
        _update_list.Update(Time.deltaTime);
    }

    #region IGGPManagerOwner
    public void AddDelegate(Action<object, UpdateEventArgs> inDelegate)
    {
        CheckUpdateList();
        _update_list.AddDelegate(inDelegate);
    }

    public void RemoveDelegate(Action<object, UpdateEventArgs> inDelegate)
    {
        CheckUpdateList();
        _update_list.RemoveDelegate(inDelegate);
    }
    #endregion IGGPManagerOwner

    public string GetReport()
    {
        if (ParentHost != null)
            return ParentHost.GetReport();

        if (_ggp_manager == null)
            return "GGP undefined!";

        var sb = new StringBuilder();
        using (CAdvancedList<KeyValuePair<SGGPKey, Variant>> lst = ListPool<KeyValuePair<SGGPKey, Variant>>.Get())
        {
            _ggp_manager.GetReport(lst);
            for(int i = 0; i < lst.Count; i++)
                sb.AppendLine($"{lst[i].Key}: {lst[i].Value}");
        }
        return sb.ToString();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(GGPHostScript))]
public class EntityDebugInfoInspector : Editor
{
    private SerializedObject _serialized_owner;
    private GGPHostScript _owner;

    void Awake()
    {
        _owner = target as GGPHostScript;
        _serialized_owner = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        if (_owner == null)
        {
            EditorGUILayout.LabelField("!!!NEED RELOAD !!!");
            return;
        }

        DrawDefaultInspector();
        GUILayout.Label("Parent Host - задавать только когда нужен другой источник GGP.");

        GUILayout.Space(5f);

        GUILayout.Label("All GGP:", GUILayout.Width(50));
        GUILayout.TextArea(_owner.GetReport());
        if (GUILayout.Button("Copy to Clipboard"))
            EditorGUIUtility.systemCopyBuffer = _owner.GetReport();
    }
}
#endif //UNITY_EDITOR