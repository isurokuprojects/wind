﻿using System.Reflection;
using UnityEngine;

namespace Primitives3D
{
    public struct SSphere3D
    {
        private Vector3 _origin;
        private float _radius;
        public Vector3 Origin { get { return _origin; } }
        public float Radius { get { return _radius; } }

        public SSphere3D(Vector3 inOrigin, float inRadius)
        {
            _origin = inOrigin;
            _radius = inRadius;
        }

        public static SSphere3D Create(SphereCollider inSphere)
        {
            Vector3 center = inSphere.transform.TransformPoint(inSphere.center);
            float scale = inSphere.transform.lossyScale.GetMaxValue();
            float radius = scale * inSphere.radius;

            return new SSphere3D(center, radius);
        }

        public override string ToString()
        {
            return string.Format("{0}, Radius: {1}", _origin, Radius);
        }

        public void SetOrigin(Vector3 inOrigin)
        {
            _origin = inOrigin;
        }

        public void SetRadius(float inRadius)
        {
#if ASSERTS
            if (inRadius < 0)
                inLogContext.WriteToLog(ELogLevel.ERROR, () => "SSphere3D", () => MethodBase.GetCurrentMethod().Name, () => "Sphere has negative radius!");
#endif //ASSERTS

            _radius = inRadius;
        }

        public SBoundBox3D GetBoundBox()
        {
            return new SBoundBox3D(Origin, Radius);
        }

        public Vector3 GetMaximumProjectionPoint(Vector3 inDir)
        {
            return Origin + inDir.normalized * Radius;
        }

        public bool IsPointInside(Vector3 inPoint)
        {
            return (inPoint - Origin).sqrMagnitude <= Radius * Radius;
        }

        public static bool IsPointInside(Vector3 inPoint, Vector3 inOrigin, float inRadius)
        {
            return (inPoint - inOrigin).sqrMagnitude <= inRadius * inRadius;
        }

        public (float, Vector3) GetSqrDistance(Vector3 inPoint)
        {
            var res = GetDistance(inPoint);
            return (res.Item1 * res.Item1, res.Item2);
        }

        public (float, Vector3) GetDistance(Vector3 inPoint)
        {
            Vector3 link = inPoint - Origin;
            float dist = link.magnitude;

            float t = Radius / dist;
            Vector3 p = t * link;

            return (dist - Radius, p + Origin);
        }

        public float GetVolume() { return Mathf.PI * Radius * Radius * Radius * 4 / 3; }


    }
}
