﻿using System;
using UnityEngine;

namespace Primitives3D
{
    public struct SBox3D
    {
        private Vector3 _origin;
        private Vector3 _hs;
        private Quaternion _rotation;

        public Vector3 Origin { get { return _origin; } }
        public Vector3 HalfSizes { get { return _hs; } }
        public Quaternion Rotation { get { return _rotation; } }

        public Vector3 Right { get { return Rotation * Vector3.right; } }
        public Vector3 Up { get { return Rotation * Vector3.up; } }
        public Vector3 Forward { get { return Rotation * Vector3.forward; } }

        public SBox3D(Vector3 inOrigin, Quaternion inRotation, Vector3 inHalfSizes)
        {
            _origin = inOrigin;
            _rotation = inRotation;
            _hs = inHalfSizes;
        }

        public SBox3D(Vector3 inOrigin, Quaternion inRotation, float inHalfSizes)
        {
            _origin = inOrigin;
            _rotation = inRotation;
            _hs = new Vector3(inHalfSizes, inHalfSizes, inHalfSizes);
        }

        public static SBox3D Create(BoxCollider inBox)
        {
            Vector3 center = inBox.transform.TransformPoint(inBox.center);
            Vector3 scale = inBox.transform.lossyScale;
            Vector3 half_sizes = inBox.size * 0.5f;
            half_sizes = Vector3.Scale(half_sizes, scale);

            return new SBox3D(center, inBox.transform.rotation, half_sizes);
        }

        public static SBox3D one = new SBox3D()
        {
            _origin = Vector3.zero,
            _hs = Vector3.one,
            _rotation = Quaternion.identity
        };

        public override string ToString()
        {
            return string.Format("Origin: {0}, HalfSizes: {1}, [right:{2}, up:{3}, forw:{4}]", _origin, HalfSizes, Right, Up, Forward);
        }

        public bool IsEmpty()
        {
            return _origin.IsEmpty() && _hs.IsEmpty() && _rotation.IsZero();
        }

        public void SetOrigin(Vector3 inOrigin)
        {
            _origin = inOrigin;
        }

        public void SetRotation(Quaternion inRotation)
        {
            _rotation = inRotation;
        }

        public void SetPosition(Vector3 inOrigin, Quaternion inRotation)
        {
            _origin = inOrigin;
            _rotation = inRotation;
        }

        public void SetHalfSizes(Vector3 inHalfSizes)
        {
            _hs = inHalfSizes;
#if ASSERTS
            CLogContext.Current.Assert(HalfSizes.x >= 0, () => "SBox3D", () => MethodBase.GetCurrentMethod().Name, () => "HalfSizes.x < 0!");
            CLogContext.Current.Assert(HalfSizes.y >= 0, () => "SBox3D", () => MethodBase.GetCurrentMethod().Name, () => "HalfSizes.y < 0!");
            CLogContext.Current.Assert(HalfSizes.z >= 0, () => "SBox3D", () => MethodBase.GetCurrentMethod().Name, () => "HalfSizes.z < 0!");
#endif //ASSERTS
        }

        public void SetHalfSizes(float inHalfSizes)
        {
            SetHalfSizes(new Vector3(inHalfSizes, inHalfSizes, inHalfSizes));
        }

        static Vector3 GetAABBHalfSizes(Quaternion inRotation, Vector3 inHalfSize)
        {
            Vector3 right = inRotation * Vector3.right;
            Vector3 up = inRotation * Vector3.up;
            Vector3 forw = inRotation * Vector3.forward;

            Vector3 aabb_half_sizes = new Vector3(
                Mathf.Abs(right.x * inHalfSize.x) +
                Mathf.Abs(up.x * inHalfSize.y) +
                Mathf.Abs(forw.x * inHalfSize.z),
                Mathf.Abs(right.y * inHalfSize.x) +
                Mathf.Abs(up.y * inHalfSize.y) +
                Mathf.Abs(forw.y * inHalfSize.z),
                Mathf.Abs(right.z * inHalfSize.x) +
                Mathf.Abs(up.z * inHalfSize.y) +
                Mathf.Abs(forw.z * inHalfSize.z));
            return aabb_half_sizes;
        }

        public SBoundBox3D GetBoundBox()
        {
            var aabb_half_sizes = GetAABBHalfSizes(_rotation, _hs);
            return new SBoundBox3D(Origin, aabb_half_sizes);
        }

        public Vector3[] GetCorners()
        {
            Vector3[] c = new Vector3[8];

            Vector3[] axis = new[]
            {
                HalfSizes.x * Right,
                HalfSizes.y * Up,
                HalfSizes.z * Forward
            };

            c[0] = Origin - axis[0] - axis[1] - axis[2];
            c[1] = Origin + axis[0] - axis[1] - axis[2];
            c[2] = Origin + axis[0] + axis[1] - axis[2];
            c[3] = Origin - axis[0] + axis[1] - axis[2];
            c[4] = Origin - axis[0] - axis[1] + axis[2];
            c[5] = Origin + axis[0] - axis[1] + axis[2];
            c[6] = Origin + axis[0] + axis[1] + axis[2];
            c[7] = Origin - axis[0] + axis[1] + axis[2];

            return c;
        }

        public Vector3[] GetCornersForLineRenderer()
        {
            Vector3[] c = new Vector3[16];
            GetCornersForLineRenderer(c);
            return c;
        }

        public void GetCornersForLineRenderer(Vector3[] c)
        {
            Vector3[] axis = new[]
            {
                HalfSizes.x * Right,
                HalfSizes.y * Up,
                HalfSizes.z * Forward
            };

            c[0] = Origin - axis[0] - axis[1] - axis[2];
            c[1] = Origin + axis[0] - axis[1] - axis[2];
            c[2] = Origin + axis[0] + axis[1] - axis[2];
            c[3] = Origin - axis[0] + axis[1] - axis[2];
            c[4] = c[0];
            c[5] = Origin - axis[0] - axis[1] + axis[2];
            c[6] = Origin + axis[0] - axis[1] + axis[2];
            c[7] = Origin + axis[0] + axis[1] + axis[2];
            c[8] = Origin - axis[0] + axis[1] + axis[2];

            c[9] = c[5];
            c[10] = c[8];
            c[11] = c[3];
            c[12] = c[2];
            c[13] = c[7];
            c[14] = c[6];
            c[15] = c[1];
        }

        public static float GetProjectionLength(Vector3 inAxe, Vector3 inHalfSize, Quaternion inRotation)
        {
            Vector3 local_axe = Quaternion.Inverse(inRotation) * inAxe;
            return Math.Abs(Vector3.Dot(Vector3.right * inHalfSize.x, local_axe)) +
                   Math.Abs(Vector3.Dot(Vector3.up * inHalfSize.y, local_axe)) +
                   Math.Abs(Vector3.Dot(Vector3.forward * inHalfSize.z, local_axe));
        }

        public Vector3[] GetCorners2D()
        {
            Vector3[] c = new Vector3[4];

            Vector3[] axis = new[]
            {
                HalfSizes.x * Right,
                HalfSizes.z * Forward
            };

            c[0] = Origin - axis[0] - axis[1];
            c[1] = Origin + axis[0] - axis[1];
            c[2] = Origin + axis[0] + axis[1];
            c[3] = Origin - axis[0] + axis[1];

            return c;
        }

        public SSphere3D GetSphere()
        {
            return new SSphere3D(Origin, HalfSizes.magnitude);
        }

        public bool IsPointInside(Vector3 inPoint)
        {
            Vector3 local = Quaternion.Inverse(Rotation) * (inPoint - Origin);

            return Mathf.Abs(local.x) <= HalfSizes.x &&
                Mathf.Abs(local.y) <= HalfSizes.y &&
                Mathf.Abs(local.z) <= HalfSizes.z;
        }

        public (float, Vector3) GetSqrDistance(Vector3 inPoint)
        {
            // compute coordinates of point in box coordinate system
            Vector3 local = inPoint - Origin;
            Vector3 closest = new Vector3(Vector3.Dot(local, Right), Vector3.Dot(local, Up), Vector3.Dot(local, Forward));

            // project test point onto box
            float sqr_distance = 0;
            float delta;

            if (closest.x < -HalfSizes.x)
            {
                delta = closest.x + HalfSizes.x;
                sqr_distance += delta * delta;
                closest.x = -HalfSizes.x;
            }
            else if (closest.x > HalfSizes.x)
            {
                delta = closest.x - HalfSizes.x;
                sqr_distance += delta * delta;
                closest.x = HalfSizes.x;
            }

            if (closest.y < -HalfSizes.y)
            {
                delta = closest.y + HalfSizes.y;
                sqr_distance += delta * delta;
                closest.y = -HalfSizes.y;
            }
            else if (closest.y > HalfSizes.y)
            {
                delta = closest.y - HalfSizes.y;
                sqr_distance += delta * delta;
                closest.y = HalfSizes.y;
            }

            if (closest.z < -HalfSizes.z)
            {
                delta = closest.z + HalfSizes.z;
                sqr_distance += delta * delta;
                closest.z = -HalfSizes.z;
            }
            else if (closest.z > HalfSizes.z)
            {
                delta = closest.z - HalfSizes.z;
                sqr_distance += delta * delta;
                closest.z = HalfSizes.z;
            }

            Vector3 p = Origin +
                closest.x * Right +
                closest.y * Up +
                closest.z * Forward;

            return (sqr_distance, p);
        }
    }
}
