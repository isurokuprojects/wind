﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Primitives3D
{
    public struct SBoundBox3D
    {
        private Vector3 _origin;
        private Vector3 _hs;

        public static SBoundBox3D one = new SBoundBox3D()
        {
            _origin = Vector3.zero,
            _hs = Vector3.one
        };

        public Vector3 Origin { get { return _origin; } }
        public Vector3 HalfSizes { get { return _hs; } }
        public Vector3 Size { get { return 2 * _hs; } }
        public Vector3 Min { get { return Origin - HalfSizes; } }
        public Vector3 Max { get { return Origin + HalfSizes; } }

        public SBoundBox3D(Vector3 inOrigin, Vector3 inHalfSizes)
        {
            _origin = inOrigin;
            _hs = inHalfSizes;
        }

        public SBoundBox3D(Vector3 inOrigin, float inHalfSizes)
        {
            _origin = inOrigin;
            _hs = new Vector3(inHalfSizes, inHalfSizes, inHalfSizes);
        }

        public SBoundBox3D(Vector3 inOrigin, Quaternion Rotation, Vector3 inHalfSizes)
        {
            _origin = inOrigin;

            Vector3 right = Rotation * Vector3.right;
            Vector3 up = Rotation * Vector3.up;
            Vector3 forw = Rotation * Vector3.forward;

            _hs = new Vector3(
                Mathf.Abs(right.x * inHalfSizes.x) +
                Mathf.Abs(up.x * inHalfSizes.y) +
                Mathf.Abs(forw.x * inHalfSizes.z),
                Mathf.Abs(right.y * inHalfSizes.x) +
                Mathf.Abs(up.y * inHalfSizes.y) +
                Mathf.Abs(forw.y * inHalfSizes.z),
                Mathf.Abs(right.z * inHalfSizes.x) +
                Mathf.Abs(up.z * inHalfSizes.y) +
                Mathf.Abs(forw.z * inHalfSizes.z));
        }

        public SBoundBox3D(Bounds bounds) : this()
        {
            _origin = bounds.center;
            _hs = bounds.extents;
        }

        public override string ToString()
        {
            return string.Format("{0}, HalfSizes: {1}, [{2} - {3}]", Origin, HalfSizes.ToStringExt(), Min.ToStringExt(), Max.ToStringExt());
        }

        public void Reset()
        {
            _origin = Vector3.zero;
            _hs = Vector3.zero;
        }

        public static bool CheckHalfSizes(Vector3 inHalfSizes)
        {
            return inHalfSizes.x >= 0 && inHalfSizes.y >= 0 && inHalfSizes.z >= 0;
        }

        public static SBoundBox3D Create(Vector3 inPoint1, Vector3 inPoint2)
        {
            Vector3 min = Vector3Ext.GetMin(inPoint1, inPoint2);
            Vector3 max = Vector3Ext.GetMax(inPoint1, inPoint2);

            Vector3 half_sizes = new Vector3((max.x - min.x) / 2, (max.y - min.y) / 2, (max.z - min.z) / 2);
            Vector3 origin = min + half_sizes;

            return new SBoundBox3D(origin, half_sizes);
        }

        public void SetOrigin(Vector3 inOrigin)
        {
            _origin = inOrigin;
        }

        public void SetHalfSizes(Vector3 inHalfSizes)
        {
            _hs = inHalfSizes;
        }

        public void SetHalfSize(float inValue, int inDimIndex)
        {
            _hs[inDimIndex] = inValue;
        }

        public bool IsValid() { return (HalfSizes.x >= 0) && (HalfSizes.y >= 0) && (HalfSizes.z >= 0); }
        public bool IsHasVolume() { return (HalfSizes.x > 0) && (HalfSizes.y > 0) && (HalfSizes.z > 0); }

        public void Update(Vector3 inPointToInclude)
        {
            Vector3 link = inPointToInclude - Origin;

            float x = Math.Max(HalfSizes.x, Math.Abs(link.x));
            float y = Math.Max(HalfSizes.y, Math.Abs(link.y));
            float z = Math.Max(HalfSizes.z, Math.Abs(link.z));

            _hs = new Vector3(x, y, z);
        }

        public void Update(float pos, int dim_index)
        {
            float link = pos - Origin[dim_index];

            float v = Math.Max(_hs[dim_index], Math.Abs(link));

            _hs[dim_index] = v;
        }

        public void Update(Vector3 inPoint, float inRadius)
        {
            Vector3 rad = new Vector3(inRadius, inRadius, inRadius);
            Vector3 min = inPoint - rad;
            Vector3 max = inPoint + rad;
            Update(min, max);
        }

        public void Update(Vector3 inMin, Vector3 inMax)
        {
            Update(inMin);
            Update(inMax);
        }

        public void Update(IEnumerable<Vector3> inPoints)
        {
            foreach (var p in inPoints)
                Update(p);
        }

        public void Inflate(Vector3 inValue)
        {
            _hs += inValue;
        }

        public SSphere3D GetSphere()
        {
            return new SSphere3D(Origin, HalfSizes.magnitude);
        }

        public float GetHorizontalRadius(bool inMaxValue = true)
        {
            if (inMaxValue)
                return Mathf.Max(HalfSizes.x, HalfSizes.z);
            return (HalfSizes.x + HalfSizes.z) / 2;
        }

        public bool IsPointInside(Vector3 inPoint)
        {
            Vector3 local = inPoint - (Origin - _hs);

            return local.x > 0 && local.x <= 2 * _hs.x &&
                   local.y > 0 && local.y <= 2 * _hs.y &&
                   local.z > 0 && local.z <= 2 * _hs.z;
        }

        public bool IsIntersectingSphere(Vector3 inPoint, float inRadius)
        {
            Vector3 min = Origin - _hs;
            Vector3 max = Origin + _hs;

            double ex = Mathf.Max(min.x - inPoint.x, 0) + Mathf.Max(inPoint.x - max.x, 0);
            double ey = Mathf.Max(min.y - inPoint.y, 0) + Mathf.Max(inPoint.y - max.y, 0);
            double ez = Mathf.Max(min.z - inPoint.z, 0) + Mathf.Max(inPoint.z - max.z, 0);

            return (ex < inRadius) && (ey < inRadius) && (ez < inRadius) && (ex * ex + ey * ey + ez * ez < inRadius * inRadius);
        }

        public bool IsPointInEllipsoid(Vector3 inPoint)
        {
            Vector3 v_sqr_radii = new Vector3(HalfSizes.x * HalfSizes.x, HalfSizes.y * HalfSizes.y, HalfSizes.z * HalfSizes.z);
            Vector3 point = inPoint - Origin;

            return point.x * point.x * v_sqr_radii.y * v_sqr_radii.z
                 + point.y * point.y * v_sqr_radii.x * v_sqr_radii.z
                 + point.z * point.z * v_sqr_radii.x * v_sqr_radii.y <= v_sqr_radii.x * v_sqr_radii.y * v_sqr_radii.z;
        }

        public Vector3[] GetCorners()
        {
            Vector3[] c = new Vector3[8];

            c[0] = Origin + new Vector3(-HalfSizes.x, -HalfSizes.y, -HalfSizes.z);
            c[1] = Origin + new Vector3(+HalfSizes.x, -HalfSizes.y, -HalfSizes.z);
            c[2] = Origin + new Vector3(+HalfSizes.x, +HalfSizes.y, -HalfSizes.z);
            c[3] = Origin + new Vector3(-HalfSizes.x, +HalfSizes.y, -HalfSizes.z);
            c[4] = Origin + new Vector3(-HalfSizes.x, -HalfSizes.y, +HalfSizes.z);
            c[5] = Origin + new Vector3(+HalfSizes.x, -HalfSizes.y, +HalfSizes.z);
            c[6] = Origin + new Vector3(+HalfSizes.x, +HalfSizes.y, +HalfSizes.z);
            c[7] = Origin + new Vector3(-HalfSizes.x, +HalfSizes.y, +HalfSizes.z);

            return c;
        }

        public void GetCorners(Vector3[] c)
        {
            c[0] = Origin + new Vector3(-HalfSizes.x, -HalfSizes.y, -HalfSizes.z);
            c[1] = Origin + new Vector3(+HalfSizes.x, -HalfSizes.y, -HalfSizes.z);
            c[2] = Origin + new Vector3(+HalfSizes.x, +HalfSizes.y, -HalfSizes.z);
            c[3] = Origin + new Vector3(-HalfSizes.x, +HalfSizes.y, -HalfSizes.z);
            c[4] = Origin + new Vector3(-HalfSizes.x, -HalfSizes.y, +HalfSizes.z);
            c[5] = Origin + new Vector3(+HalfSizes.x, -HalfSizes.y, +HalfSizes.z);
            c[6] = Origin + new Vector3(+HalfSizes.x, +HalfSizes.y, +HalfSizes.z);
            c[7] = Origin + new Vector3(-HalfSizes.x, +HalfSizes.y, +HalfSizes.z);
        }
         
        public Vector3[] GetCornersForLineRenderer()
        {
            Vector3[] c = new Vector3[16];
            GetCornersForLineRenderer(c);
            return c;
        }

        public void GetCornersForLineRenderer(Vector3[] c)
        {
            c[0] = Origin + new Vector3(-HalfSizes.x, -HalfSizes.y, -HalfSizes.z);
            c[1] = Origin + new Vector3(+HalfSizes.x, -HalfSizes.y, -HalfSizes.z);
            c[2] = Origin + new Vector3(+HalfSizes.x, +HalfSizes.y, -HalfSizes.z);
            c[3] = Origin + new Vector3(-HalfSizes.x, +HalfSizes.y, -HalfSizes.z);
            c[4] = c[0];
            c[5] = Origin + new Vector3(-HalfSizes.x, -HalfSizes.y, +HalfSizes.z);
            c[6] = Origin + new Vector3(+HalfSizes.x, -HalfSizes.y, +HalfSizes.z);
            c[7] = Origin + new Vector3(+HalfSizes.x, +HalfSizes.y, +HalfSizes.z);
            c[8] = Origin + new Vector3(-HalfSizes.x, +HalfSizes.y, +HalfSizes.z);
            c[9] = c[5];
            c[10] = c[8];
            c[11] = c[3];
            c[12] = c[2];
            c[13] = c[7];
            c[14] = c[6];
            c[15] = c[1];
        }

        public float GetVolume()
        {
            if (!IsHasVolume())
                return 0;
            return HalfSizes.x * HalfSizes.y * HalfSizes.z;
        }

        public (float, Vector3) GetSqrDistance(Vector3 inPoint)
        {
            // compute coordinates of point in box coordinate system
            Vector3 local = inPoint - Origin;
            Vector3 kClosest = new Vector3(Vector3.Dot(local, Vector3.right), Vector3.Dot(local, Vector3.up), Vector3.Dot(local, Vector3.forward));

            // project test point onto box
            float fSqrDistance = 0;
            float fDelta;

            if (kClosest.x < -HalfSizes.x)
            {
                fDelta = kClosest.x + HalfSizes.x;
                fSqrDistance += fDelta * fDelta;
                kClosest.x = -HalfSizes.x;
            }
            else if (kClosest.x > HalfSizes.x)
            {
                fDelta = kClosest.x - HalfSizes.x;
                fSqrDistance += fDelta * fDelta;
                kClosest.x = HalfSizes.x;
            }

            if (kClosest.y < -HalfSizes.y)
            {
                fDelta = kClosest.y + HalfSizes.y;
                fSqrDistance += fDelta * fDelta;
                kClosest.y = -HalfSizes.y;
            }
            else if (kClosest.y > HalfSizes.y)
            {
                fDelta = kClosest.y - HalfSizes.y;
                fSqrDistance += fDelta * fDelta;
                kClosest.y = HalfSizes.y;
            }

            if (kClosest.z < -HalfSizes.z)
            {
                fDelta = kClosest.z + HalfSizes.z;
                fSqrDistance += fDelta * fDelta;
                kClosest.z = -HalfSizes.z;
            }
            else if (kClosest.z > HalfSizes.z)
            {
                fDelta = kClosest.z - HalfSizes.z;
                fSqrDistance += fDelta * fDelta;
                kClosest.z = HalfSizes.z;
            }

            Vector3 p = Origin + new Vector3(kClosest.x, kClosest.y, kClosest.z);

            return (fSqrDistance, p);
        }

        public SBox3D GetBox3D()
        {
            return new SBox3D(_origin, Quaternion.identity, _hs);
        }
    }
}